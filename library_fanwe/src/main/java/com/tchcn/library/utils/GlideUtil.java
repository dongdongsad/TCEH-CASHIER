package com.tchcn.library.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.fanwe.library.R;

/**
 * Glide帮助类
 */
public class GlideUtil {


    /**
     * 默认调用方法
     *
     * @param model String, byte[], File, Integer, Uri
     * @param <T>
     * @return
     */
    public static <T> DrawableTypeRequest<T> load(Context context,T model) {
        return (DrawableTypeRequest<T>) Glide.with(context).load(model)
                .placeholder(R.drawable.ic_o2o_default_pic)
                .error(R.drawable.ic_o2o_default_pic)
                //.override(100,100) 指定图片像素
                .diskCacheStrategy(DiskCacheStrategy.SOURCE) //禁用掉Glide的缓存功能
                .dontAnimate();
    }

    public static <T> void load(Context context,T model, ImageView view) {

        Glide.with(context)
                .load(model)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .preload();

        Glide.with(context)
                .load(model)
                .placeholder(R.drawable.ic_o2o_default_pic)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .into(view);
    }

    public static <T> void loadNoPlaceHolder(Context context,T model, ImageView view) {

        Glide.with(context)
                .load(model)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .preload();

        Glide.with(context)
                .load(model)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .into(view);
    }

    public static <T> void loadWith7Transform(Context context,T model, ImageView view) {

        Glide.with(context)
                .load(model)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .preload();

        Glide.with(context)
                .load(model)
                .placeholder(R.drawable.ic_o2o_default_pic)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate()
                .into(view);
    }
    public static <T> DrawableTypeRequest<T> loadToBitmap(Context context,T model) {

        Glide.with(context)
                .load(model)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .preload();

        return (DrawableTypeRequest<T>) Glide.with(context)
                .load(model)
                .placeholder(R.drawable.ic_o2o_default_pic)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .dontAnimate();
    }
    /**
     * 默认调用方法
     *
     * @param model String, byte[], File, Integer, Uri
     * @param <T>
     * @return
     */
    public static <T> DrawableTypeRequest<T> loadWithoutPlaceHolder(Context context,T model) {
        return (DrawableTypeRequest<T>) Glide.with(context).load(model)
                //.placeholder(R.drawable.ic_o2o_default_pic)
                .error(R.drawable.ic_o2o_default_pic)
                //.override(100,100) 指定图片像素
                //.diskCacheStrategy(DiskCacheStrategy.NONE) //禁用掉Glide的缓存功能
                .dontAnimate();
    }

    /**
     * 加载用户头像方法
     *
     * @param model String, byte[], File, Integer, Uri
     * @param <T>
     * @return
     */
    public static <T> DrawableTypeRequest<T> loadHeadImage(Context context,T model) {
        return (DrawableTypeRequest<T>) load(context,model)
                .placeholder(R.drawable.ic_o2o_default_pic)
                .error(R.drawable.ic_o2o_default_pic)
                .dontAnimate();
    }

    /**
     * 清空Glide缓存
     */
    public static void clearCache(Context context) {
        Glide.get(context).clearDiskCache();
    }

}
