package com.tchcn.library.constant;


/**
 * Created by sad on 2018/6/4.
 */

public class Constant {
    private static final String TEST_URL = "https://dev.tchcn.com";
    private static final String URL = "https://mss.tchcn.com";
    public static final boolean DEBUG = true;

    public static final String SERVER_URL = TEST_URL;//项目路径


    public static final class UserLoginState
    {
        public static final int UN_LOGIN = 0;  //未登录
        public static final int LOGIN = 1;
        public static final int TEMP_LOGIN = 2;
    }
}
