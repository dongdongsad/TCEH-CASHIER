package com.tchcn.library.common;

import com.tchcn.library.http.AppHttpUtil;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.http.AppRequestParams;
import com.tchcn.library.model.BaseActModel;
import com.tchcn.library.model.CityListActModel;
import com.tchcn.library.model.CommercialAreaModel;
import com.tchcn.library.model.IndustryActModel;
import com.tchcn.library.model.LoginActModel;
import com.tchcn.library.model.TableActModel;
import com.tchcn.library.model.TypeActModel;

/**
 * Created by sad on 2018/6/4.
 */

public class CommonInterface {

    /**
     * 发送验证码
     * @param listener
     */
    public static void getValidCode(String number,AppRequestCallback<BaseActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/valid/getValidCode");
        params.put("number",number);
        AppHttpUtil.getInstance().get(params, listener);
    }

    /**
     * 登录
     * @param listener
     */
    public static void login(String username,String password,String ip,double longitude,double latitude,AppRequestCallback<LoginActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/users/login");
        params.put("username",username);
        params.put("password",password);
        params.put("ip",ip);
        params.put("fromport","terminal");
        params.put("longitude",longitude);
        params.put("latitude",latitude);
        params.put("fromport","terminal");
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 注册
     * @param username
     * @param password
     * @param mobile
     * @param captcha
     * @param listener
     */
    public static void register(String username,String password,String mobile,String captcha,AppRequestCallback<BaseActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/users/insert");
        params.put("username",username);
        params.put("password",password);
        params.put("mobile",mobile);
        params.put("captcha",captcha);
        AppHttpUtil.getInstance().post(params, listener);
    }


    /**
     * 获取所有省份
     * @param listener
     */
    public static void getAllProvince(AppRequestCallback<CityListActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/city/getProvinces");
        AppHttpUtil.getInstance().get(params, listener);
    }


    /**
     * 根据别名获取城市和区域
     * @param pyName 城市别名
     * @param listener
     */
    public static void getCityByName(String pyName,AppRequestCallback<CityListActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/city/getCitysByProvince");
        params.put("pyName",pyName);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 所属行业 业态
     * @param type 	0.行业分类，1.所属液态
     * @param pid 	行业id（查询所属液态的时候）
     * @param listener
     */
    public static void getAllIndusty(int type,String pid,AppRequestCallback<IndustryActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/industrial/select");
        params.put("type",type);
        if (pid!=null)
        params.put("pid",pid);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 获取商圈
     * @param cityId
     * @param listener
     */
    public static void getCommercialArea(String cityId,AppRequestCallback<CommercialAreaModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/commercialArea/queryCommercialArea");
        params.put("search_city",cityId);
        params.put("currentPage",1);
        params.put("pageSize",300);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 注册完善商户资料
     * @param supplier_name 商户名称
     * @param user_id 用户id
     * @param qu_id	 区id
     * @param address 地址
     * @param cate_id 所属液态
     * @param deal_cate_id 所属行业
     * @param quan_id 	商圈id
     * @param city_id 城市id
     * @param listener
     */
    public static void addSupplier(String supplier_name,String user_id,String qu_id,
                                   String address,String cate_id,String deal_cate_id,
                                   String quan_id,String city_id,AppRequestCallback<BaseActModel> listener) {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/supplier/supplier_insert");
        params.put("supplier_name",supplier_name);
        params.put("user_id",user_id);
        params.put("qu_id",qu_id);
        params.put("address",address);
        params.put("cate_id",cate_id);
        params.put("deal_cate_id",deal_cate_id);
        params.put("quan_id",quan_id);
        params.put("city_id",city_id);
        AppHttpUtil.getInstance().post(params, listener);
    }


    /**
     * 获取所有桌台
     * @param currentPage
     * @param userid
     * @param listener
     */
    public static void getAllTables(String sendType,String status,int currentPage,int userid,AppRequestCallback<TableActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/Table/getAllTables");
        params.put("sendType",sendType);
        params.put("status",status);
        params.put("currentPage",currentPage);
        params.put("userid",11);
        AppHttpUtil.getInstance().post(params, listener);
    }


    public static void getAllGoods(String sendType,String status,int currentPage,int userid,AppRequestCallback<TableActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/dealcate/cateDetail");
        params.put("sendType",sendType);
        params.put("status",status);
        params.put("currentPage",currentPage);
        params.put("userid",11);
        AppHttpUtil.getInstance().post(params, listener);
    }


    /**
     * 获取所有类型
     * @param supplier_id
     * @param location_id
     * @param page
     * @param listener
     */
    public static void getGoodsType(String supplier_id,String location_id,int page,AppRequestCallback<TypeActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/deal/deal_all");
        params.put("supplier_id",supplier_id);
        params.put("location_id",location_id);
        params.put("page",page);
        AppHttpUtil.getInstance().post(params, listener);
    }



}
