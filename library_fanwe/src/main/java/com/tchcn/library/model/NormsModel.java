package com.tchcn.library.model;

import java.io.Serializable;
import java.util.List;

/**
 * 规格
 * Created by sad on 2018/5/15.
 */

public class NormsModel implements Serializable{
    private int id;//大id
    private String name;//大名称
    private List<NormsModel> norms;//子类
    private double [] price;
    private int num = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double[] getPrice() {
        return price;
    }

    public void setPrice(double[] price) {
        this.price = price;
    }

    public List<NormsModel> getNorms() {
        return norms;
    }

    public void setNorms(List<NormsModel> norms) {
        this.norms = norms;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String  getAllNormString (){
        String allNorm = "";
        for (int i = 0; i < norms.size(); i++) {
            allNorm += norms.get(i).getName();
        }
        return allNorm;
    }
}
