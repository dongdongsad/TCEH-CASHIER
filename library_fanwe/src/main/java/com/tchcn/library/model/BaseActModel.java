package com.tchcn.library.model;


public class BaseActModel
{
    private String code;
    private String msg;


    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public boolean isOk(){
        if("10001".equals(getCode())){
            return true;
        }
        return false;
    }

}
