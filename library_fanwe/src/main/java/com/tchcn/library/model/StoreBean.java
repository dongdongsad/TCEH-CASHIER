package com.tchcn.library.model;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Created by sad on 2018/11/9.
 */
@Table(name = "store")
public class StoreBean {
    @Column(name = "_id",isId = true)
    private int _id;
    @Column(name="id")
    private int id;
    @Column(name = "location_name")
    private String location_name;
    @Column(name = "supplier_id")
    private int supplier_id; //商户id
    @Column(name = "address")
    private String address;
    @Column(name = "user_id")
    private String user_id;
    @Column(name = "link_mobile")
    private String link_mobile;
    @Column(name = "is_default")
    private int is_default; // 1 默认 0 非默认
    @Column(name = "deal_cate_id")
    private int deal_cate_id;//门店类型

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLocation_name() {
        return location_name;
    }

    public void setLocation_name(String location_name) {
        this.location_name = location_name;
    }

    public int getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(int supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getLink_mobile() {
        return link_mobile;
    }

    public void setLink_mobile(String link_mobile) {
        this.link_mobile = link_mobile;
    }

    public int getDeal_cate_id() {
        return deal_cate_id;
    }

    public void setDeal_cate_id(int deal_cate_id) {
        this.deal_cate_id = deal_cate_id;
    }

    public int getIs_default() {
        return is_default;
    }

    public void setIs_default(int is_default) {
        this.is_default = is_default;
    }
}
