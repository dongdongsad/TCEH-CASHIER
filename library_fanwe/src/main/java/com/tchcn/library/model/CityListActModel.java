package com.tchcn.library.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sad on 2018/6/6.
 */

public class CityListActModel extends BaseActModel implements Serializable{
    CityData data;

    public CityData getData() {
        return data;
    }

    public void setData(CityData data) {
        this.data = data;
    }

    public class CityData implements Serializable{
        List<RegionModel> cityList;

        public List<RegionModel> getCityList() {
            return cityList;
        }

        public void setCityList(List<RegionModel> cityList) {
            this.cityList = cityList;
        }
    }
}
