package com.tchcn.library.model;

import java.util.List;

/**
 * Created by sad on 2018/10/22.
 */

public class LoginActModel extends BaseActModel{
    LoginData data;

    public LoginData getData() {
        return data;
    }

    public void setData(LoginData data) {
        this.data = data;
    }

    public class  LoginData{
        List<StoreBean> locationList;

        List<MenuModel> ownMenuList;
        UserInfoModel user;

        SupplierData supplier;



        public SupplierData getSupplier() {
            return supplier;
        }

        public void setSupplier(SupplierData supplier) {
            this.supplier = supplier;
        }

        public List<StoreBean> getLocationList() {
            return locationList;
        }

        public void setLocationList(List<StoreBean> locationList) {
            this.locationList = locationList;
        }


        public List<MenuModel> getOwnMenuList() {
            return ownMenuList;
        }

        public void setOwnMenuList(List<MenuModel> ownMenuList) {
            this.ownMenuList = ownMenuList;
        }

        public class SupplierData{
            private int id;
            private String supplier_name;
            private String address;
            private int deal_cate_id; // 1 美食 2 便利店

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getSupplier_name() {
                return supplier_name;
            }

            public void setSupplier_name(String supplier_name) {
                this.supplier_name = supplier_name;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public int getDeal_cate_id() {
                return deal_cate_id;
            }

            public void setDeal_cate_id(int deal_cate_id) {
                this.deal_cate_id = deal_cate_id;
            }
        }

        public UserInfoModel getUser() {
            return user;
        }

        public void setUser(UserInfoModel user) {
            this.user = user;
        }
    }
}
