package com.tchcn.library.model;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Created by sad on 2018/11/9.
 */
@Table(name = "menu_model")
public class MenuModel {
    @Column(name = "_id",isId = true)
    private int _id;
    @Column(name = "id")
    private int id;
    @Column(name = "menu_name")
    private String menu_name;
    @Column(name = "image")
    private String image;
    @Column(name = "is_image")
    private String is_image;
    @Column(name = "menu_id")
    private int menu_id;
    @Column(name = "projectId")
    private String projectId;

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getIs_image() {
        return is_image;
    }

    public void setIs_image(String is_image) {
        this.is_image = is_image;
    }

    public int getMenu_id() {
        return menu_id;
    }

    public void setMenu_id(int menu_id) {
        this.menu_id = menu_id;
    }

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }
}
