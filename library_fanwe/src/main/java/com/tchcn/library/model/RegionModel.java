package com.tchcn.library.model;

import java.io.Serializable;

/**
 * Created by sad on 2018/6/5.
 */
public class RegionModel implements Serializable
{
    private static final long serialVersionUID = 0L;

    private String id;
    private String pname;
    private String pname_url;
    private String cityname;
    private String cityname_url;
    private String citycode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPname() {
        return pname;
    }

    public void setPname(String pname) {
        this.pname = pname;
    }

    public String getPname_url() {
        return pname_url;
    }

    public void setPname_url(String pname_url) {
        this.pname_url = pname_url;
    }

    public String getCitycode() {
        return citycode;
    }

    public void setCitycode(String citycode) {
        this.citycode = citycode;
    }

    public String getCityname() {
        return cityname;
    }

    public void setCityname(String cityname) {
        this.cityname = cityname;
    }

    public String getCityname_url() {
        return cityname_url;
    }

    public void setCityname_url(String cityname_url) {
        this.cityname_url = cityname_url;
    }
}
