package com.tchcn.library.model;

import com.tchcn.library.dao.LocalUserModelDao;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

/**
 * Created by sad on 2018/10/25.
 */
@Table(name = "user")
public class UserInfoModel {
    @Column(name = "_id",isId =true)
    private int _id;
    @Column(name = "id")
    private int id;
    @Column(name = "mobile")
    private String mobile;
    @Column(name = "img")
    private String img;
    @Column(name = "real_name")
    private String real_name;
    @Column(name = "firstLogin")
    private int firstLogin;
    @Column(name = "name")
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getReal_name() {
        return real_name;
    }

    public void setReal_name(String real_name) {
        this.real_name = real_name;
    }

    public int getFirstLogin() {
        return firstLogin;
    }

    public void setFirstLogin(int firstLogin) {
        this.firstLogin = firstLogin;
    }

    public static String getUser_id(){
        if(LocalUserModelDao.queryModel() != null && !(LocalUserModelDao.queryModel().getId()+"").isEmpty()){
            return LocalUserModelDao.queryModel().getId()+"";
        }
        return "";
    }

}
