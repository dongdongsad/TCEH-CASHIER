package com.tchcn.library.model;

import java.io.Serializable;

/**
 * Created by sad on 2018/5/3.
 */

public class TableModel implements Serializable{
    private String tableid;
    private int id;
    private String name;//餐桌名
    private String day;//日期
    private String time;//时间
    private int num;//就餐人数
    private int status;//状态 	1.桌台，2.包厢
    private String statusjs;//状态
    private int type;//类型 1.待下单，2.待结账，3.预定，4.停用
    private String typejs;//类型

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getTableid() {
        return tableid;
    }

    public void setTableid(String tableid) {
        this.tableid = tableid;
    }

    public String getStatusjs() {
        return statusjs;
    }

    public void setStatusjs(String statusjs) {
        this.statusjs = statusjs;
    }

    public String getTypejs() {
        return typejs;
    }

    public void setTypejs(String typejs) {
        this.typejs = typejs;
    }
}
