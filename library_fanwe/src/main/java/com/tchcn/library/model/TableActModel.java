package com.tchcn.library.model;

import java.util.List;

/**
 * Created by sad on 2018/10/26.
 */

public class TableActModel {
    List<TableModel> list;

    public List<TableModel> getList() {
        return list;
    }

    public void setList(List<TableModel> list) {
        this.list = list;
    }
}
