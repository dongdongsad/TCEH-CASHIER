package com.tchcn.library.model;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sad on 2018/5/3.
 */
@Table(name = "good_model")
public class GoodActModel implements Serializable{
    @Column(name ="_id",isId = true)
    private int _id;
    @Column(name = "id")
    private int id; //接口需要
    @Column(name = "orderId")
    private int orderId; //保存时的orderId
    @Column(name = "name")
    private String name;//菜品名称
    @Column(name = "price")
    private double price;
    @Column(name = "image")
    private String image;
    @Column(name = "typeId")
    private int typeId;//菜品类型
    @Column(name = "typeName")
    private String typeName;//类型名称
    @Column(name = "carNum")
    private int carNum;//购物车数量  接口需要
    private List<NormsModel> selectNorms;//已选规格
    @Column(name = "normsId")
    private String normsId; //规格 拼接id  ,连接  接口需要
    @Column(name = "hasNorm")
    private int hasNorm = 0;
    private List<NormsModel> normsModel;
    @Column(name = "status")
    private int status; // 1 已下单
    @Column(name = "discount")
    private double discount = 0;  //折扣 如0.9 九折 接口需要
    @Column(name = "free")
    private int free = 0;  //是否免费 接口需要

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getCarNum() {
        return carNum;
    }

    public void setCarNum(int carNum) {
        this.carNum = carNum;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public List<NormsModel> getSelectNorms() {
        return selectNorms;
    }

    public void setSelectNorms(List<NormsModel> selectNorms) {
        this.selectNorms = selectNorms;
    }

    public int getHasNorm() {
        return hasNorm;
    }

    public void setHasNorm(int hasNorm) {
        this.hasNorm = hasNorm;
    }

    public List<NormsModel> getNormsModel() {
        return normsModel;
    }

    public void setNormsModel(List<NormsModel> normsModel) {
        this.normsModel = normsModel;
    }

    public String getNormsId() {
        return normsId;
    }

    public void setNormsId(String normsId) {
        this.normsId = normsId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getDiscount() {
        return discount;
    }

    public void setDiscount(double discount) {
        this.discount = discount;
    }

    public int getFree() {
        return free;
    }

    public void setFree(int free) {
        this.free = free;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    @Override
    public String toString() {
        return id+","+orderId+","+status;
    }
}
