package com.tchcn.library.model;

import org.xutils.db.annotation.Column;
import org.xutils.db.annotation.Table;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sad on 2018/5/17.
 */

@Table(name = "good_order")
public class OrderModel implements Serializable{
    @Column(name = "id",isId = true,autoGen = false)
    private int id;
    @Column(name = "tableName")
    private String tableName;
    @Column(name = "name")
    private String name;
    private List<GoodActModel> goodList;
    @Column(name = "remarks")
    private String remarks;
    @Column(name = "remarksId")
    private String remarksId;
    @Column(name = "code")
    private String code;//订单编号
    @Column(name = "amountReceivable")
    private double amountReceivable;//应收金额
    private double amount;//订单金额
    @Column(name = "placeOrderTime")
    private String placeOrderTime;//下单时间
    @Column(name = "status")
    private int status;// 1 已下单 2 已结账 3 挂单

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GoodActModel> getGoodList() {
        return goodList;
    }

    public void setGoodList(List<GoodActModel> goodList) {
        this.goodList = goodList;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }


    public String getRemarksId() {
        return remarksId;
    }

    public void setRemarksId(String remarksId) {
        this.remarksId = remarksId;
    }

    public String getRemarks() {
        return remarks;
    }

    public double getAmountReceivable() {
        return amountReceivable;
    }

    public void setAmountReceivable(double amountReceivable) {
        this.amountReceivable = amountReceivable;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getPlaceOrderTime() {
        return placeOrderTime;
    }

    public void setPlaceOrderTime(String placeOrderTime) {
        this.placeOrderTime = placeOrderTime;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }
}
