package com.tchcn.library.model;

import java.util.List;

/**
 * 菜品类型
 * Created by sad on 2018/10/27.
 */

public class TypeActModel extends BaseActModel{
    TypeData data;

    public TypeData getData() {
        return data;
    }

    public void setData(TypeData data) {
        this.data = data;
    }

    public class TypeData{
        List<TypeModel> dealList;

        public List<TypeModel> getDealList() {
            return dealList;
        }

        public void setDealList(List<TypeModel> dealList) {
            this.dealList = dealList;
        }
    }

    public class TypeModel{
        private int id;
        private String deal_name;
        private int num;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDeal_name() {
            return deal_name;
        }

        public void setDeal_name(String deal_name) {
            this.deal_name = deal_name;
        }

        public int getNum() {
            return num;
        }

        public void setNum(int num) {
            this.num = num;
        }
    }

}
