package com.tchcn.library.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sad on 2018/6/6.
 */

public class IndustryActModel extends BaseActModel implements Serializable{
    IndustryData data;

    public IndustryData getData() {
        return data;
    }

    public void setData(IndustryData data) {
        this.data = data;
    }

    public class IndustryData implements Serializable{
        List<Industry> industrialList;

        public List<Industry> getIndustrialList() {
            return industrialList;
        }

        public void setIndustrialList(List<Industry> industrialList) {
            this.industrialList = industrialList;
        }

        public class Industry implements Serializable{
            private String id;
            private String name;
            private String type;

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getType() {
                return type;
            }

            public void setType(String type) {
                this.type = type;
            }
        }
    }


}
