package com.tchcn.library.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by sad on 2018/6/6.
 */

public class CommercialAreaModel extends BaseActModel implements Serializable{
    AllData data;

    public AllData getData() {
        return data;
    }

    public void setData(AllData data) {
        this.data = data;
    }

    public class AllData implements Serializable{

        Resultmap resultmap;

        public Resultmap getResultmap() {
            return resultmap;
        }

        public void setResultmap(Resultmap resultmap) {
            this.resultmap = resultmap;
        }

        public class Resultmap{
        List<CommercialArea> list;

            public List<CommercialArea> getList() {
                return list;
            }

            public void setList(List<CommercialArea> list) {
                this.list = list;
            }

            public class CommercialArea implements Serializable{
                private String id;
                private String pname_url;
                private String area_name;
                private String city_proper_code;
                private String city_proper;
                private String city_id;
                private String cityname;

                public String getId() {
                    return id;
                }

                public void setId(String id) {
                    this.id = id;
                }

                public String getPname_url() {
                    return pname_url;
                }

                public void setPname_url(String pname_url) {
                    this.pname_url = pname_url;
                }

                public String getArea_name() {
                    return area_name;
                }

                public void setArea_name(String area_name) {
                    this.area_name = area_name;
                }

                public String getCity_proper_code() {
                    return city_proper_code;
                }

                public void setCity_proper_code(String city_proper_code) {
                    this.city_proper_code = city_proper_code;
                }

                public String getCity_proper() {
                    return city_proper;
                }

                public void setCity_proper(String city_proper) {
                    this.city_proper = city_proper;
                }

                public String getCity_id() {
                    return city_id;
                }

                public void setCity_id(String city_id) {
                    this.city_id = city_id;
                }

                public String getCityname() {
                    return cityname;
                }

                public void setCityname(String cityname) {
                    this.cityname = cityname;
                }
            }
        }



    }


}
