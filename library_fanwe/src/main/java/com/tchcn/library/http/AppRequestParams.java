package com.tchcn.library.http;

import android.text.TextUtils;

import com.fanwe.library.adapter.http.model.SDRequestParams;
import com.tchcn.library.constant.Constant;


/**
 * Created by sad on 2016/12/13.
 */
public class AppRequestParams extends SDRequestParams {

    private boolean isNeedShowActInfo = true;
    private boolean isNeedShowErrorTip = true;

    public static final class RequestDataType {
        public static final int BASE64 = 0;
        public static final int AES = 4;
    }

    public static final class ResponseDataType {
        public static final int BASE64 = 0;
        public static final int JSON = 1;
        public static final int AES = 4;
    }

    public AppRequestParams() {
        super();
    }

    public void setModuleAct(String url) {
        setUrl(Constant.SERVER_URL+url);
    }

    public void putCheckEmpty(String key, String value){
        if(!TextUtils.isEmpty(value)){
            super.put(key,value);
        }
    }

    public void putUserInfo() {
     /*   LocalUserModel user = LocalUserModelDao.queryModel();
        if (user != null) {
            put("user_key", user.getUser_name());
            put("user_pwd", user.getUser_pwd());
        }*/
    }


    /*public void putCityId()
    {
        put("city_id", AppRuntimeWorker.getCity_id());
    }

    public void putLocation()
    {
        if (BaiduMapManager.getInstance().hasLocationSuccess())
        {
            put("m_latitude", BaiduMapManager.getInstance().getLatitude());
            put("m_longitude", BaiduMapManager.getInstance().getLongitude());
        }
    }*/

    public boolean isNeedShowActInfo()
    {
        return isNeedShowActInfo;
    }

    public void setNeedShowActInfo(boolean isNeedShowActInfo)
    {
        this.isNeedShowActInfo = isNeedShowActInfo;
    }

    public boolean isNeedShowErrorTip()
    {
        return isNeedShowErrorTip;
    }

    public void setIsNeedShowErrorTip(boolean isNeedShowErrorTip)
    {
        this.isNeedShowErrorTip = isNeedShowErrorTip;
    }

}
