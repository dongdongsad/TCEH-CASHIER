package com.tchcn.library.http;


import com.fanwe.library.adapter.http.callback.SDModelRequestCallback;
import com.fanwe.library.adapter.http.model.SDRequestParams;
import com.fanwe.library.adapter.http.model.SDResponse;
import com.tchcn.library.constant.Constant;
import com.tchcn.library.model.BaseActModel;
import com.tchcn.library.utils.LogUtil;
import com.tchcn.library.utils.SDJsonUtil;
import com.tchcn.library.utils.SDToast;

public abstract class AppRequestCallback<D> extends SDModelRequestCallback<D> {

    protected AppRequestParams requestParams;
    protected BaseActModel baseActModel;
    protected boolean isCache;
    protected String sessionId;

    @Override
    public void setRequestParams(SDRequestParams requestParams) {
        super.setRequestParams(requestParams);
        if (requestParams instanceof AppRequestParams)
        {
            this.requestParams = (AppRequestParams) requestParams;
        }
    }

    @Override
    protected void onStartAfter() {

    }

    @Override
    protected void onSuccessBefore(SDResponse resp) {
        LogUtil.e( requestParams.getUrl() , resp.getResult());
     /*   if (requestParams != null) {
            LogUtil.i("onSuccessBefore:" + requestParams.getCtl() + "," + requestParams.getAct() + "：" + resp.getResult());

            String result = resp.getResult();
            if (!TextUtils.isEmpty(result)) {
                if (Constant.DEBUG) {
                    if (result.contains("false")) {
                        SDToast.showToast(requestParams.getCtl() + "," + requestParams.getAct() + " false");
                    }
                }

                switch (requestParams.getResponseDataType()) {
                    case AppRequestParams.ResponseDataType.BASE64:
                        result = SDBase64.decodeToString(result);
                        break;
                    case AppRequestParams.ResponseDataType.JSON:

                        break;

                    default:
                        break;
                }

                // 解密后的result赋值回去
                resp.setResult(result);
                requestParams.setResponseDataType(AppRequestParams.ResponseDataType.JSON);
            }
        }
        // 调用父类方法转实体*/
        try {
            super.onSuccessBefore(resp);

            if (actModel != null) {
                if (actModel instanceof BaseActModel) {
                    baseActModel = (BaseActModel) actModel;

                   /* if (baseActModel != null) {
                        if(baseActModel.getCode() == 200){
                            if(!TextUtils.isEmpty(baseActModel.getMsg()))
                                showToast();
                        }
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

 /*   protected void startLoginActivity()
    {
        Activity lastActivity = SDActivityManager.getInstance().getLastActivity();
        if (lastActivity != null)
        {
            Intent intent = new Intent(lastActivity, LoginActivity.class);
            lastActivity.startActivity(intent);
//            if(lastActivity.getClass()!= MainActivity.class&&lastActivity.getClass()!=InitAdvsMultiActivity.class){
//                SDActivityManager.getInstance().onDestroy(lastActivity);
//                lastActivity.finish();
//            }
        }
    }
*/
    @Override
    protected void onError(SDResponse resp) {
        if (requestParams != null) {
            LogUtil.e("onError:" , requestParams.getCtl() + "," + requestParams.getAct() + "：" + resp.getThrowable());

            if (Constant.DEBUG) {
                SDToast.showToast(requestParams.getCtl() + "," + requestParams.getAct() + " " + String.valueOf(resp.getThrowable()));
            }
        }
    }

    @Override
    protected void onCancel(SDResponse resp) {

    }

    @Override
    protected void onFinish(SDResponse resp) {

    }

    @Override
    protected <T> T parseActModel(String result, Class<T> clazz) {
        return SDJsonUtil.json2Object(result, clazz);
    }


    public void showToast()
    {
        if (actModel != null) {
            if (requestParams != null) {
                if (requestParams.isNeedShowActInfo()) {
                    if (actModel instanceof BaseActModel) {
                        BaseActModel baseActModel = (BaseActModel) actModel;
                        SDToast.showToast(baseActModel.getMsg());
                    }
                }
            }
        }
    }
}
