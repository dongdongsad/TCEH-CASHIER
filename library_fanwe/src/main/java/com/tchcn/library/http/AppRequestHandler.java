package com.tchcn.library.http;


import com.fanwe.library.adapter.http.handler.SDRequestHandler;

import org.xutils.common.Callback;

public class AppRequestHandler implements SDRequestHandler
{

	private Callback.Cancelable cancelable;

	public AppRequestHandler(Callback.Cancelable cancelable)
	{
		super();
		this.cancelable = cancelable;
	}

	@Override
	public void cancel()
	{
		if (cancelable != null)
		{
			cancelable.cancel();
		}
	}

}
