package com.tchcn.library.dao;

import com.tchcn.library.db.DbManagerX;
import com.tchcn.library.model.MenuModel;
import com.tchcn.library.model.UserInfoModel;

import org.xutils.ex.DbException;

import java.util.List;

public class MenuModelDao
{

	public static void insertModel(List<MenuModel> model)
	{
		try {
			deleteAllModel();
			DbManagerX.getDb().save(model);
		} catch (DbException e) {
			e.printStackTrace();
		}
	}

	public static List<MenuModel> queryModel()
	{
		try {
			List<MenuModel> list = DbManagerX.getDb().findAll(MenuModel.class);
			if (list != null)
				return list;
		} catch (DbException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void deleteAllModel()
	{
		try {
			DbManagerX.getDb().delete(MenuModel.class);
		} catch (DbException e) {
			e.printStackTrace();
		}

	}

}
