package com.tchcn.library.dao;

import android.widget.LinearLayout;

import com.tchcn.library.db.DbManagerX;
import com.tchcn.library.model.UserInfoModel;

import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.ex.DbException;

import java.util.List;

public class LocalUserModelDao
{

	public static void insertModel(UserInfoModel model)
	{
		try {
			deleteAllModel();
			DbManagerX.getDb().save(model);
		} catch (DbException e) {
			e.printStackTrace();
		}
	}

	public static UserInfoModel queryModel()
	{
		try {
			List<UserInfoModel> list = DbManagerX.getDb().findAll(UserInfoModel.class);
			if (list != null && list.size() > 0)
				return list.get(0);
		} catch (DbException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void deleteAllModel()
	{
		try {
			DbManagerX.getDb().delete(UserInfoModel.class);
		} catch (DbException e) {
			e.printStackTrace();
		}

	}

}
