package com.tchcn.library.dao;

import com.tchcn.library.db.DbManagerX;
import com.tchcn.library.model.MenuModel;
import com.tchcn.library.model.StoreBean;

import org.xutils.ex.DbException;

import java.util.List;

/**
 * Created by sad on 2018/11/9.
 */

public class StoreBeanDao {

    public static void insertModel(List<StoreBean> model)
    {
        try {
            deleteAllModel();
            DbManagerX.getDb().save(model);
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static List<StoreBean> queryModel()
    {
        try {
            List<StoreBean> list = DbManagerX.getDb().findAll(StoreBean.class);
            if (list != null)
                return list;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static StoreBean queryDefaultModel()
    {
        try {
            List<StoreBean> list = DbManagerX.getDb().selector(StoreBean.class).where("is_default","=",1).findAll();
            if (list != null && list.size()>0)
                return list.get(0);
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static void deleteAllModel()
    {
        try {
            DbManagerX.getDb().delete(StoreBean.class);
        } catch (DbException e) {
            e.printStackTrace();
        }

    }

}
