package com.tchcn.library.dao;

import android.util.Log;

import com.tchcn.library.db.DbManagerX;
import com.tchcn.library.model.GoodActModel;
import com.tchcn.library.model.OrderModel;

import org.xutils.db.sqlite.WhereBuilder;
import org.xutils.ex.DbException;

import java.util.List;

/**
 * Created by sad on 2018/11/3.
 */

public class OrderModelDao {
    public static void insertModel(OrderModel model)
    {
        try {
            DbManagerX.getDb().save(model);
            for (int i = 0; i < model.getGoodList().size(); i++) {
                model.getGoodList().get(i).setOrderId(model.getId());
            }
            DbManagerX.getDb().save(model.getGoodList());
        } catch (DbException e) {
            e.printStackTrace();
        }
    }

    public static List<OrderModel> queryAllHangingOrder()
    {
        try {
            List<OrderModel> list = DbManagerX.getDb().findAll(OrderModel.class);
            for (int i = 0; i < list.size(); i++) {
                List<GoodActModel> goodActModels = DbManagerX.getDb().selector(GoodActModel.class).where("orderId","=",list.get(i).getId()).findAll();
                list.get(i).setGoodList(goodActModels);
            }
            if (list != null)
                return list;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }


    public static List<OrderModel> queryAllOrder()
    {
        try {
            List<OrderModel> list = DbManagerX.getDb().findAll(OrderModel.class);
            for (int i = 0; i < list.size(); i++) {
                List<GoodActModel> goodActModels = DbManagerX.getDb().selector(GoodActModel.class).where("orderId","=",list.get(i).getId()).findAll();
                list.get(i).setGoodList(goodActModels);
            }
            if (list != null)
                return list;
        } catch (DbException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void deleteAllModel()
    {
        try {
            DbManagerX.getDb().delete(OrderModel.class);
            DbManagerX.getDb().delete(GoodActModel.class);
        } catch (DbException e) {
            e.printStackTrace();
        }

    }

    public static void deleteAllHangingModel()
    {
        try {
            DbManagerX.getDb().delete(OrderModel.class, WhereBuilder.b("status","=",3));
        } catch (DbException e) {
            e.printStackTrace();
        }

    }

    public static void deleteModel(OrderModel orderModel)
    {
        try {
            DbManagerX.getDb().delete(orderModel);
            DbManagerX.getDb().delete(GoodActModel.class, WhereBuilder.b("orderId","=",orderModel.getId()));
        } catch (DbException e) {
            e.printStackTrace();
        }

    }
}
