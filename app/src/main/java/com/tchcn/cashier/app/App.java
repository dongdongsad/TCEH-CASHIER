package com.tchcn.cashier.app;

import android.app.Application;
import android.content.Context;
import android.webkit.CookieManager;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipelineConfig;
import com.tchcn.library.SDLibrary;

import org.xutils.x;


/**
 * Created by sad on 2018/4/22.
 */
public class App extends Application
{

    private static App sInstance;
    private static long lastJump2LoginTime = 0L;

    @Override
    public void onCreate()
    {
        super.onCreate();
        sInstance = this;
        init();
    }

    public static App getApplication()
    {
        return sInstance;
    }

    private void init()
    {
        //initBugly();
        x.Ext.init(sInstance);
        SDLibrary.getInstance().init(this);
        initFresco();
        x.Ext.init(sInstance);
    }

    private void initFresco() {
        ImagePipelineConfig frescoConfig = ImagePipelineConfig.newBuilder(this).setDownsampleEnabled(true).build();
        Fresco.initialize(this, frescoConfig);
    }

   /* private void initBugly()
    {
        CrashReport.initCrashReport(getApplicationContext(), SDResourcesUtil.getString(R.string.bugly_appid), BuildConfig.Enable_Bugly);
    }
*/



    public void exitApp(boolean isBackground)
    {
        /*SDActivityManager.getInstance().finishAllActivity();
        EExitApp event = new EExitApp();
        SDEventManager.post(event);*/
        if (!isBackground)
        {
            System.exit(0);
        }
    }

    public void clearAppsLocalUserModel()
    {
        CookieManager.getInstance().removeAllCookie();
    }

    public static long getLastJump2LoginTime()
    {
        return lastJump2LoginTime;
    }

    public static void setLastJump2LoginTime(long lastJump2LoginTime)
    {
        App.lastJump2LoginTime = lastJump2LoginTime;
    }

    @Override
    protected void attachBaseContext(Context base)
    {
        super.attachBaseContext(base);
    }
}
