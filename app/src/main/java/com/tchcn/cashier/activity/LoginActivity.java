package com.tchcn.cashier.activity;

import android.content.Intent;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.fanwe.library.adapter.http.model.SDResponse;
import com.tchcn.cashier.R;
import com.tchcn.cashier.order.activity.MainActivity;
import com.tchcn.library.common.CommonInterface;
import com.tchcn.library.dao.LocalUserModelDao;
import com.tchcn.library.dao.MenuModelDao;
import com.tchcn.library.dao.StoreBeanDao;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.model.LoginActModel;
import com.tchcn.library.model.MenuModel;
import com.tchcn.library.model.UserInfoModel;
import com.tchcn.library.utils.LogUtil;
import com.tchcn.library.utils.SDToast;
import com.tchcn.supermarket.ui.activity.CashierHomeActivity;
import com.zhy.autolayout.AutoLayoutActivity;

import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class LoginActivity extends AutoLayoutActivity {

    @BindView(R.id.et_account)
    EditText etAccount;
    @BindView(R.id.et_password)
    EditText etPassword;
    @BindView(R.id.activity_main)
    LinearLayout activityMain;

    Location location;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initView();
        hideBottomUIMenu();
    }

    private void initView() {

    }

    @OnClick({R.id.tv_login, R.id.tv_register})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_login:
                if("".equals(etAccount.getText().toString())||"".equals(etPassword.getText().toString())){
                    SDToast.showToast("请填写账号和密码");
                    return;
                }

                CommonInterface.login(etAccount.getText().toString(), etPassword.getText().toString()
                        ,getLocalIpAddress(), 0, 0
                        ,new AppRequestCallback<LoginActModel>() {
                            @Override
                            protected void onSuccess(SDResponse sdResponse) {
                                LogUtil.e("login",sdResponse.getResult());
                                if("200".equals(actModel.getCode())) {
                                    LocalUserModelDao.insertModel(actModel.getData().getUser());
                                    MenuModelDao.insertModel(actModel.getData().getOwnMenuList());
                                    StoreBeanDao.insertModel(actModel.getData().getLocationList());
                                    if(actModel.getData().getSupplier().getDeal_cate_id() == 1)
                                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                    else {
                                        startActivity(new Intent(LoginActivity.this, CashierHomeActivity.class));
                                    }

                                }else {
                                    SDToast.showToast(actModel.getMsg());
                                }
                            }
                        });

                break;
            case R.id.tv_register:
                startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                break;
        }
    }

    /**
     * 获取本机ip
     * @return
     */
    public String getLocalIpAddress() {
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        return inetAddress.getHostAddress().toString();
                    }
                }
            }
        } catch (SocketException ex) {
            LogUtil.e("===ip====",ex.getMessage());
        }
        return null;
    }

    /**
     * 隐藏虚拟按键，并且全屏
     */
    protected void hideBottomUIMenu() {
        //隐藏虚拟按键，并且全屏
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }


}
