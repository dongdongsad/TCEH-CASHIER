package com.tchcn.cashier.dialog;

import android.view.View;
import android.widget.TextView;

import com.bigkoo.pickerview.adapter.ArrayWheelAdapter;
import com.contrarywind.listener.OnItemSelectedListener;
import com.contrarywind.view.WheelView;
import com.fanwe.library.adapter.http.model.SDResponse;
import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.R;
import com.tchcn.cashier.order.dialog.ChooseNumDialog;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.utils.LogUtil;

import java.util.ArrayList;
import java.util.List;

import com.tchcn.library.common.CommonInterface;
import com.tchcn.library.model.CityListActModel;
import com.tchcn.library.model.RegionModel;

/**
 * Created by sad on 2018/6/6.
 */

public class ChooseCityDialog extends BaseNiceDialog{

    WheelView wvProvince;//省
    WheelView wvCity;//市
    WheelView wvArea;//区

    private List<RegionModel> mListProvince;//省份集合
    private List<RegionModel> mListCity;//城市集合
    private List<RegionModel> mListCounty;//区集合

    List<String> provinceList = new ArrayList<>();
    List<String> cityList = new ArrayList<>();
    List<String> countyList = new ArrayList<>();

    private String province="";
    private String city="";
    private String county="";
    private String cityId;
    private String qu_id;

    ChooseNumDialog.OnDialogClickListener onDialogClickListener = null;

    public static ChooseCityDialog newInstance() {
        ChooseCityDialog dialog = new ChooseCityDialog();
        return dialog;
    }

    public ChooseCityDialog setConvertListener(ChooseNumDialog.OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_choose_city;
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        wvProvince = (WheelView) holder.getConvertView().findViewById(R.id.options1);
        wvCity = (WheelView) holder.getConvertView().findViewById(R.id.options2);
        wvArea = (WheelView) holder.getConvertView().findViewById(R.id.options3);
        setWheelView(wvProvince);
        setWheelView(wvCity);
        setWheelView(wvArea);
        wvProvince.setCyclic(false);
        wvCity.setCyclic(false);
        wvArea.setCyclic(false);
        CommonInterface.getAllProvince(new AppRequestCallback<CityListActModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                LogUtil.e("data",sdResponse.toString());
                mListProvince = actModel.getData().getCityList();
                for (int i = 0; i < mListProvince.size(); i++) {
                    provinceList.add(mListProvince.get(i).getPname());
                }
                wvProvince.setAdapter(new ArrayWheelAdapter(provinceList));
                wvProvince.requestLayout();
                province = mListProvince.get(0).getPname();
                CommonInterface.getCityByName(mListProvince.get(0).getPname_url(), new AppRequestCallback<CityListActModel>() {
                    @Override
                    protected void onSuccess(SDResponse sdResponse) {
                        mListCity = actModel.getData().getCityList();
                        for (int i = 0; i < mListCity.size(); i++) {
                            cityList.add(mListCity.get(i).getCityname());
                        }
                        wvCity.setAdapter(new ArrayWheelAdapter(cityList));
                        wvCity.requestLayout();
                        if(mListCity!=null) {
                            city = mListCity.get(0).getCityname();
                            CommonInterface.getCityByName(mListCity.get(0).getCityname_url(), new AppRequestCallback<CityListActModel>() {
                                @Override
                                protected void onSuccess(SDResponse sdResponse) {
                                    mListCounty = actModel.getData().getCityList();
                                    for (int i = 0; i < mListCounty.size(); i++) {
                                        countyList.add(mListCounty.get(i).getCityname());
                                    }
                                    wvArea.setAdapter(new ArrayWheelAdapter(countyList));
                                    wvArea.requestLayout();
                                    if(mListCounty!=null)
                                    county = mListCounty.get(0).getCityname();
                                }
                            });
                        }
                    }
                });
                wvProvince.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(int index) {
                        province = provinceList.get(index);
                        CommonInterface.getCityByName(mListProvince.get(index).getPname_url(), new AppRequestCallback<CityListActModel>() {
                            @Override
                            protected void onSuccess(SDResponse sdResponse) {
                                cityList.clear();
                                mListCity = actModel.getData().getCityList();
                                for (int i = 0; i < mListCity.size(); i++) {
                                    cityList.add(mListCity.get(i).getCityname());
                                }
                                wvCity.setAdapter(new ArrayWheelAdapter(cityList));
                                wvCity.requestLayout();
                                if(mListCity!=null)
                                    city = cityList.get(0);
                                CommonInterface.getCityByName(mListCity.get(0).getCityname_url(), new AppRequestCallback<CityListActModel>() {
                                    @Override
                                    protected void onSuccess(SDResponse sdResponse) {
                                        countyList.clear();
                                        mListCounty = actModel.getData().getCityList();
                                        for (int i = 0; i < mListCounty.size(); i++) {
                                            countyList.add(mListCounty.get(i).getCityname());
                                        }
                                        wvArea.setAdapter(new ArrayWheelAdapter(countyList));
                                        wvArea.requestLayout();
                                        if(mListCounty!=null)
                                        county = mListCounty.get(0).getCityname();
                                    }
                                });
                            }
                        });
                    }
                });

                wvCity.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(int index) {
                        city = cityList.get(index);
                        cityId = mListCity.get(index).getId();
                        CommonInterface.getCityByName(mListCity.get(index).getCityname_url(), new AppRequestCallback<CityListActModel>() {
                            @Override
                            protected void onSuccess(SDResponse sdResponse) {
                                countyList.clear();
                                mListCounty = actModel.getData().getCityList();
                                for (int i = 0; i < mListCounty.size(); i++) {
                                    countyList.add(mListCounty.get(i).getCityname());
                                }
                                wvArea.setAdapter(new ArrayWheelAdapter(countyList));
                                wvArea.requestLayout();
                                if(mListCounty!=null)
                                    county = mListCounty.get(0).getCityname();
                            }
                        });
                    }
                });

                wvArea.setOnItemSelectedListener(new OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(int index) {
                        county = countyList.get(index);
                        qu_id = mListCounty.get(index).getId();
                    }
                });
            }
        });
        TextView tvCommit = (TextView) holder.getConvertView().findViewById(R.id.tv_commit);
        tvCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDialogClickListener.onItemClick(view,dialog);
            }
        });
    }


    private void setWheelView(WheelView view){
        view.setLineSpacingMultiplier(1.6f);//设置两横线之间的间隔倍数
        view.setTextSize(32);
    }

    public String getCityInfo(){
        return province+"-"+city+"-"+county;
    }

    public String getCityId(){
        return cityId;
    }

    public String getQuId(){
        return qu_id;
    }
}
