package com.tchcn.cashier.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fanwe.library.adapter.http.model.SDResponse;
import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.R;
import com.tchcn.library.common.CommonInterface;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.model.CommercialAreaModel;
import com.tchcn.cashier.order.dialog.ChooseNumDialog;

import java.util.List;

/**
 * 选择商圈弹框
 * Created by sad on 2018/6/11.
 */

public class ChooseCircleDialog extends BaseNiceDialog{

    private String cityId;
    RecyclerView rv;
    List<CommercialAreaModel.AllData.Resultmap.CommercialArea> commercialAreas;
    CommercialAreaAdapter areaAdapter;
    ChooseNumDialog.OnDialogClickListener onDialogClickListener = null;

    CommercialAreaModel.AllData.Resultmap.CommercialArea commercialArea;

    public static ChooseCircleDialog newInstance(String cityId) {
        ChooseCircleDialog dialog = new ChooseCircleDialog();
        Bundle bundle = new Bundle();
        bundle.putString("cityId",cityId);
        dialog.setArguments(bundle);
        return dialog;
    }

    public ChooseCircleDialog setConvertListener(ChooseNumDialog.OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_choose_industy;
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        cityId = getArguments().getString("cityId");
        rv = (RecyclerView) holder.getConvertView().findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        CommonInterface.getCommercialArea(cityId, new AppRequestCallback<CommercialAreaModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                commercialAreas = actModel.getData().getResultmap().getList();
                areaAdapter = new CommercialAreaAdapter(commercialAreas);
                areaAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        commercialArea = commercialAreas.get(position);
                        onDialogClickListener.onItemClick(view,dialog);
                    }
                });
                rv.setAdapter(areaAdapter);
            }
        });
    }

    class CommercialAreaAdapter extends BaseQuickAdapter<CommercialAreaModel.AllData.Resultmap.CommercialArea, BaseViewHolder> {

        public CommercialAreaAdapter(@Nullable List<CommercialAreaModel.AllData.Resultmap.CommercialArea> data) {
            super(R.layout.item_industry, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, CommercialAreaModel.AllData.Resultmap.CommercialArea item) {
            helper.setText(R.id.tv_name,item.getArea_name());
        }
    }

    public CommercialAreaModel.AllData.Resultmap.CommercialArea getCommercialArea(){
        return commercialArea;
    }
}
