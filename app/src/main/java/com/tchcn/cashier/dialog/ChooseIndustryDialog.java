package com.tchcn.cashier.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.fanwe.library.adapter.http.model.SDResponse;
import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.R;

import com.tchcn.cashier.order.dialog.ChooseNumDialog;
import com.tchcn.library.common.CommonInterface;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.model.IndustryActModel;

import java.util.List;

/**
 * Created by sad on 2018/6/6.
 */

public class ChooseIndustryDialog extends BaseNiceDialog{

    private int type;
    private String pid;
    RecyclerView rv;
    List<IndustryActModel.IndustryData.Industry> industries;
    IndustryAdapter industryAdapter;
    ChooseNumDialog.OnDialogClickListener onDialogClickListener = null;

    IndustryActModel.IndustryData.Industry industry;

    public static ChooseIndustryDialog newInstance(int type,String pid) {
        ChooseIndustryDialog dialog = new ChooseIndustryDialog();
        Bundle bundle = new Bundle();
        bundle.putInt("type",type);
        bundle.putString("pid",pid);
        dialog.setArguments(bundle);
        return dialog;
    }

    public ChooseIndustryDialog setConvertListener(ChooseNumDialog.OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_choose_industy;
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        type = getArguments().getInt("type");
        pid = getArguments().getString("pid");
        rv = (RecyclerView) holder.getConvertView().findViewById(R.id.rv);
        rv.setLayoutManager(new LinearLayoutManager(getActivity()));
        CommonInterface.getAllIndusty(type,pid, new AppRequestCallback<IndustryActModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                industries = actModel.getData().getIndustrialList();
                industryAdapter = new IndustryAdapter(industries);
                industryAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
                    @Override
                    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                        industry = industries.get(position);
                        onDialogClickListener.onItemClick(view,dialog);
                    }
                });
                rv.setAdapter(industryAdapter);
            }
        });
    }

    class IndustryAdapter extends BaseQuickAdapter<IndustryActModel.IndustryData.Industry, BaseViewHolder> {

        public IndustryAdapter(@Nullable List<IndustryActModel.IndustryData.Industry> data) {
            super(R.layout.item_industry, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, IndustryActModel.IndustryData.Industry item) {
            helper.setText(R.id.tv_name,item.getName());
        }
    }

    public IndustryActModel.IndustryData.Industry getSelectIndustry(){
        return industry;
    }
}
