package com.tchcn.cashier.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.tchcn.cashier.activity.LoginActivity;


/**
 * Created by sad on 2018/10/23.
 */

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action.equals("android.intent.action.BOOT_COMPLETED")) {
            //启动app
            starUp(context);
        }
    }

    private void starUp(Context context) {
//        Intent mMainActivityIntent = context.getPackageManager(). 用包名启动
//                getLaunchIntentForPackage(context.getPackageName());
        Intent mMainActivityIntent = new Intent(context, LoginActivity.class);//跳转首页启动
        mMainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(mMainActivityIntent);

    }
}
