package com.tchcn.cashier.utils;

import android.content.Context;

import com.bumptech.glide.DrawableTypeRequest;
import com.bumptech.glide.Glide;
import com.tchcn.cashier.R;
import com.tchcn.cashier.app.App;

/**
 * Glide帮助类
 */
public class GlideUtil
{

    @Deprecated
    public static <T> DrawableTypeRequest<T> load(Context context, T model)
    {
        return load(model);
    }

    /**
     * 默认调用方法
     *
     * @param model String, byte[], File, Integer, Uri
     * @param <T>
     * @return
     */
    public static <T> DrawableTypeRequest<T> load(T model)
    {
        return (DrawableTypeRequest<T>) Glide.with(App.getApplication()).load(model)
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                //.override(100,100) 指定图片像素
                //.diskCacheStrategy(DiskCacheStrategy.NONE) 禁用掉Glide的缓存功能
                .dontAnimate();
    }

    //---------以下为扩展方法------------

    @Deprecated
    public static <T> DrawableTypeRequest<T> loadHeadImage(Context context, T model)
    {
        return loadHeadImage(model);
    }

    /**
     * 加载用户头像方法
     *
     * @param model String, byte[], File, Integer, Uri
     * @param <T>
     * @return
     */
    public static <T> DrawableTypeRequest<T> loadHeadImage(T model)
    {
        return (DrawableTypeRequest<T>) load(model)
                .placeholder(R.drawable.ic_launcher)
                .error(R.drawable.ic_launcher)
                .dontAnimate();
    }
}
