package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

import java.util.List;

/**
 * Created by Administrator on 2018/12/5.
 */

public class MenuListActModel extends BaseActModel {

    private MenuListData data;

    public MenuListData getData() {
        return data;
    }

    public void setData(MenuListData data) {
        this.data = data;
    }

    public static class MenuListData{

        private List<MenuOperateModel> res;

        public List<MenuOperateModel> getRes() {
            return res;
        }

        public void setRes(List<MenuOperateModel> res) {
            this.res = res;
        }

        public static class MenuOperateModel{

            private String menu_name;
            private String id;
            private String is_auth; //1已授权

            public String getMenu_name() {
                return menu_name;
            }

            public void setMenu_name(String menu_name) {
                this.menu_name = menu_name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getIs_auth() {
                return is_auth;
            }

            public void setIs_auth(String is_auth) {
                this.is_auth = is_auth;
            }
        }

    }

}
