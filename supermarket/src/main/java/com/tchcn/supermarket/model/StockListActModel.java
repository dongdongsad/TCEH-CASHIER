package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

import java.util.List;

/**
 * Created by Administrator on 2018/11/3.
 */

public class StockListActModel extends BaseActModel {

    private StockListData data;

    public StockListData getData() {
        return data;
    }

    public void setData(StockListData data) {
        this.data = data;
    }

    public static class StockListData{

        private List<StockModel> res;

        public List<StockModel> getRes() {
            return res;
        }

        public void setRes(List<StockModel> res) {
            this.res = res;
        }

        public static class StockModel{

            private String id; //商品id
            private String goods_id; //商品统一id
            private String bar_code; //	条码
            private String image; //商品图片
            private String cate_name; //商品名
            private String unit; //单位
            private String stock; //库存
            private String stock_warning; //库存预警
            private String retail_price; //	零售价
            private String user_price; //会员价
            private String deal_id; //商品分类id
            private String deal_name; //商品分类名
            private int mode;

            public int getMode() {
                return mode;
            }

            public void setMode(int mode) {
                this.mode = mode;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getBar_code() {
                return bar_code;
            }

            public void setBar_code(String bar_code) {
                this.bar_code = bar_code;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getCate_name() {
                return cate_name;
            }

            public void setCate_name(String cate_name) {
                this.cate_name = cate_name;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getStock() {
                return stock;
            }

            public void setStock(String stock) {
                this.stock = stock;
            }

            public String getStock_warning() {
                return stock_warning;
            }

            public void setStock_warning(String stock_warning) {
                this.stock_warning = stock_warning;
            }

            public String getRetail_price() {
                return retail_price;
            }

            public void setRetail_price(String retail_price) {
                this.retail_price = retail_price;
            }

            public String getUser_price() {
                return user_price;
            }

            public void setUser_price(String user_price) {
                this.user_price = user_price;
            }

            public String getDeal_id() {
                return deal_id;
            }

            public void setDeal_id(String deal_id) {
                this.deal_id = deal_id;
            }

            public String getDeal_name() {
                return deal_name;
            }

            public void setDeal_name(String deal_name) {
                this.deal_name = deal_name;
            }
        }

    }

}
