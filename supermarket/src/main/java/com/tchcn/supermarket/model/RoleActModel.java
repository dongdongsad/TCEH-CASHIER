package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

/**
 * Created by Administrator on 2018/11/23.
 */

public class RoleActModel extends BaseActModel {

    private RoleData data;

    public RoleData getData() {
        return data;
    }

    public void setData(RoleData data) {
        this.data = data;
    }

    public static class RoleData{

        private String[] list;

        public String[] getList() {
            return list;
        }

        public void setList(String[] list) {
            this.list = list;
        }
    }

}
