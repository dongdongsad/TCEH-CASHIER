package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

/**
 * Created by Administrator on 2018/11/3.
 */

public class StockNumActModel extends BaseActModel {

    private StockNumData data;

    public StockNumData getData() {
        return data;
    }

    public void setData(StockNumData data) {
        this.data = data;
    }

    public static class StockNumData{

        private String[] res;

        public String[] getRes() {
            return res;
        }

        public void setRes(String[] res) {
            this.res = res;
        }
    }

}
