package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

import java.util.List;

/**
 * Created by Administrator on 2018/11/19.
 */

public class UserActModel extends BaseActModel {

    private UserData data;

    public UserData getData() {
        return data;
    }

    public void setData(UserData data) {
        this.data = data;
    }

    public static class UserData{

        private List<UserModel> res;
        private PageModel pagination;

        public PageModel getPagination() {
            return pagination;
        }

        public void setPagination(PageModel pagination) {
            this.pagination = pagination;
        }

        public List<UserModel> getRes() {
            return res;
        }

        public void setRes(List<UserModel> res) {
            this.res = res;
        }

        public static class UserModel{

            private String role_name; //角色名
            private String user_image; //用户头像
            private String role_id; //角色id
            private String user_name; //用户名
            private String disabled; //是否停用 1=停用
            private String id; //用户id
            private String supplier_name; //商户名
            private String approle_name; //身份
            private String location_name;
            private String user_pwd; //密码
            private String location_id; //门店id
            private boolean checked;

            public String getUser_pwd() {
                return user_pwd;
            }

            public void setUser_pwd(String user_pwd) {
                this.user_pwd = user_pwd;
            }

            public String getLocation_id() {
                return location_id;
            }

            public void setLocation_id(String location_id) {
                this.location_id = location_id;
            }

            public String getLocation_name() {
                return location_name;
            }

            public void setLocation_name(String location_name) {
                this.location_name = location_name;
            }

            public String getApprole_name() {
                return approle_name;
            }

            public void setApprole_name(String approle_name) {
                this.approle_name = approle_name;
            }

            public boolean isChecked() {
                return checked;
            }

            public void setChecked(boolean checked) {
                this.checked = checked;
            }

            public String getRole_name() {
                return role_name;
            }

            public void setRole_name(String role_name) {
                this.role_name = role_name;
            }

            public String getUser_image() {
                return user_image;
            }

            public void setUser_image(String user_image) {
                this.user_image = user_image;
            }

            public String getRole_id() {
                return role_id;
            }

            public void setRole_id(String role_id) {
                this.role_id = role_id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getDisabled() {
                return disabled;
            }

            public void setDisabled(String disabled) {
                this.disabled = disabled;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getSupplier_name() {
                return supplier_name;
            }

            public void setSupplier_name(String supplier_name) {
                this.supplier_name = supplier_name;
            }
        }

    }

}
