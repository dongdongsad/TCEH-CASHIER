package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

import java.util.List;

/**
 * Created by Administrator on 2018/11/21.
 */

public class PtActModel extends BaseActModel {

    private PtData data;

    public PtData getData() {
        return data;
    }

    public void setData(PtData data) {
        this.data = data;
    }

    public static class PtData{

        private List<PtModel> res;

        public List<PtModel> getRes() {
            return res;
        }

        public void setRes(List<PtModel> res) {
            this.res = res;
        }

        public static class PtModel{

            private String name;
            private String id;
            private boolean checked;

            public boolean isChecked() {
                return checked;
            }

            public void setChecked(boolean checked) {
                this.checked = checked;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }
        }

    }

}
