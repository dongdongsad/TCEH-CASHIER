package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

import java.util.List;

/**
 * Created by Administrator on 2018/11/5.
 */

public class StockOperateActModel extends BaseActModel {

    private StockOperateData data;

    public StockOperateData getData() {
        return data;
    }

    public void setData(StockOperateData data) {
        this.data = data;
    }

    public static class StockOperateData{

        private List<StockOperateModel> res;

        public List<StockOperateModel> getRes() {
            return res;
        }

        public void setRes(List<StockOperateModel> res) {
            this.res = res;
        }

        public static class StockOperateModel{

            private String id; //出入库id
            private String dealcate_id; //商品id
            private String dealcate_name; //商品名
            private String goods_id; //商品统一id
            private String bar_code; //条码
            private String image; //商品图片
            private String unit; //单位
            private String num; //数量
            private String price; //单价
            private String deal_id; //商品分类id
            private String deal_name; //商品分类名
            private String user_id; //操作人id
            private String user_name; //操作人用户名
            private String user_image; //操作人头像
            private String create_time; //操作时间
            private String is_store; //1入库记录 2出库记录

            public String getIs_store() {
                return is_store;
            }

            public void setIs_store(String is_store) {
                this.is_store = is_store;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getDealcate_id() {
                return dealcate_id;
            }

            public void setDealcate_id(String dealcate_id) {
                this.dealcate_id = dealcate_id;
            }

            public String getDealcate_name() {
                return dealcate_name;
            }

            public void setDealcate_name(String dealcate_name) {
                this.dealcate_name = dealcate_name;
            }

            public String getGoods_id() {
                return goods_id;
            }

            public void setGoods_id(String goods_id) {
                this.goods_id = goods_id;
            }

            public String getBar_code() {
                return bar_code;
            }

            public void setBar_code(String bar_code) {
                this.bar_code = bar_code;
            }

            public String getImage() {
                return image;
            }

            public void setImage(String image) {
                this.image = image;
            }

            public String getUnit() {
                return unit;
            }

            public void setUnit(String unit) {
                this.unit = unit;
            }

            public String getNum() {
                return num;
            }

            public void setNum(String num) {
                this.num = num;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getDeal_id() {
                return deal_id;
            }

            public void setDeal_id(String deal_id) {
                this.deal_id = deal_id;
            }

            public String getDeal_name() {
                return deal_name;
            }

            public void setDeal_name(String deal_name) {
                this.deal_name = deal_name;
            }

            public String getUser_id() {
                return user_id;
            }

            public void setUser_id(String user_id) {
                this.user_id = user_id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }

            public String getUser_image() {
                return user_image;
            }

            public void setUser_image(String user_image) {
                this.user_image = user_image;
            }

            public String getCreate_time() {
                return create_time;
            }

            public void setCreate_time(String create_time) {
                this.create_time = create_time;
            }
        }

    }

}
