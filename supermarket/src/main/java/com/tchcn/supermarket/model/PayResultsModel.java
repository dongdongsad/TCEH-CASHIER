package com.tchcn.supermarket.model;

/**
 * Created by Administrator on 2018/12/10.
 */

public class PayResultsModel {

    private int payIndex;
    private float payMoney;
    private String payType;

    public String getPayType() {
        switch (getPayIndex()){
            case 0:
                payType = "现金支付";
                break;
            case 1:
                payType = "微信支付";
                break;
            case 2:
                payType = "支付宝支付";
                break;
            case 3:
                payType = "余额支付";
                break;
            case 4:
                payType = "银行卡支付";
                break;
        }
        return payType;
    }

    public int getPayIndex() {
        return payIndex;
    }

    public void setPayIndex(int payIndex) {
        this.payIndex = payIndex;
    }

    public float getPayMoney() {
        return payMoney;
    }

    public void setPayMoney(float payMoney) {
        this.payMoney = payMoney;
    }
}
