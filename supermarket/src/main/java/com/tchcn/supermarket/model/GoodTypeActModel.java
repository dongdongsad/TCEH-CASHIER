package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

import java.util.List;

/**
 * Created by Administrator on 2018/11/3.
 */

public class GoodTypeActModel extends BaseActModel {

    private GoodTypeData data;

    public GoodTypeData getData() {
        return data;
    }

    public void setData(GoodTypeData data) {
        this.data = data;
    }

    public static class GoodTypeData{

        private List<GoodType> res;

        public List<GoodType> getRes() {
            return res;
        }

        public void setRes(List<GoodType> res) {
            this.res = res;
        }

        public static class GoodType{

            private String id;
            private String name;
            private boolean checked = false;

            public boolean isChecked() {
                return checked;
            }

            public void setChecked(boolean checked) {
                this.checked = checked;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }
        }

    }

}
