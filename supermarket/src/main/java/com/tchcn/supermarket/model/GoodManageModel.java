package com.tchcn.supermarket.model;

/**
 * Created by Administrator on 2018/5/7.
 */

public class GoodManageModel {

    private String good_name;
    private int mode; //0入库记录，1出库记录

    public int getMode() {
        return mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public String getGood_name() {
        return good_name;
    }

    public void setGood_name(String good_name) {
        this.good_name = good_name;
    }
}
