package com.tchcn.supermarket.model;

import com.tchcn.library.model.BaseActModel;

import java.util.List;

/**
 * Created by Administrator on 2018/11/3.
 */

public class LocationUserListActModel extends BaseActModel {

    private LocationUserListData data;

    public LocationUserListData getData() {
        return data;
    }

    public void setData(LocationUserListData data) {
        this.data = data;
    }

    public static class LocationUserListData{

        private List<LocationUser> res;

        public List<LocationUser> getRes() {
            return res;
        }

        public void setRes(List<LocationUser> res) {
            this.res = res;
        }

        public static class LocationUser{

            private String id;
            private String user_name;
            private boolean checked;

            public boolean isChecked() {
                return checked;
            }

            public void setChecked(boolean checked) {
                this.checked = checked;
            }

            public String getId() {
                return id;
            }

            public void setId(String id) {
                this.id = id;
            }

            public String getUser_name() {
                return user_name;
            }

            public void setUser_name(String user_name) {
                this.user_name = user_name;
            }
        }

    }

}
