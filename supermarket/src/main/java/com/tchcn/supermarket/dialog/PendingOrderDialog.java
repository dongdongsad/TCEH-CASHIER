package com.tchcn.supermarket.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.adapter.GoodsInPendingAdapter;
import com.tchcn.supermarket.adapter.PendingListAdapter;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.dao.PendingOrdersModelDao;
import com.tchcn.supermarket.dbmodel.PendingOrderModel;
import com.tchcn.supermarket.ui.fragment.CashierHomeFragment;
import com.tchcn.supermarket.utils.DisplayUtils;
import com.tchcn.supermarket.utils.SDToast;

import java.util.List;

/**
 *  挂单列表
 * Created by humu on 2018/5/5.
 */

public class PendingOrderDialog extends BaseNiceDialog {

    private RecyclerView rv_pending;
    private RecyclerView rv_pending_goods;
    private PendingListAdapter listAdapter;
    private GoodsInPendingAdapter goodsInPendingAdapter;
    private int last_order_position = 0;
    private PendingOrderModel pendingOrderModel;

    public static PendingOrderDialog newInstance(){
        PendingOrderDialog dialog = new PendingOrderDialog();
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setWidth(DisplayUtils.px2dp(getContext(),990));

        listAdapter = new PendingListAdapter(new PendingListAdapter.OnPendingOrderDelListener() {
            @Override
            public void onPendDelete(int position, PendingOrderModel.PendingOrder order) {
                pendingOrderModel = PendingOrdersModelDao.queryModel();
                if(pendingOrderModel != null){
                    List<PendingOrderModel.PendingOrder> pendingOrderList = pendingOrderModel.getOrder_list();
                    if(pendingOrderList != null){
                        pendingOrderList.remove(position);
                        pendingOrderModel.setOrder_list(pendingOrderList);
                        PendingOrdersModelDao.insertModel(pendingOrderModel);
                        if(order.isIs_checked()){
                            last_order_position = -1;
                            goodsInPendingAdapter.clearDatas();
                            rv_pending_goods.setAdapter(goodsInPendingAdapter);
                        }
                        if(pendingOrderList.size() == 0){
                            dismiss();
                        }
                        CashierHomeFragment.getInstance().refreshPendingOrders();
                    }
                }
            }
        });
        pendingOrderModel = PendingOrdersModelDao.queryModel();
        List<PendingOrderModel.PendingOrder> pendingOrderList = pendingOrderModel.getOrder_list();
        if(pendingOrderModel != null){
            if(pendingOrderList.size() > 0){
                pendingOrderList.get(0).setIs_checked(true);
            }
            listAdapter.setDatas(pendingOrderModel.getOrder_list());
        }

        goodsInPendingAdapter = new GoodsInPendingAdapter();
        if(pendingOrderList.size() > 0){
            goodsInPendingAdapter.setDatas(pendingOrderList.get(0).getGood_list());
        }

    }

    @Override
    public int intLayoutId() {
        return R.layout.sdialog_pending_orders;
    }

    @Override
    public void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
        Button btn_cancel = viewHolder.getView(R.id.btn_cancel);
        Button btn_chose_order = viewHolder.getView(R.id.btn_chose_order);
        rv_pending = viewHolder.getView(R.id.rv_pending);
        rv_pending_goods = viewHolder.getView(R.id.rv_pending_goods);
        rv_pending.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_pending_goods.setLayoutManager(new LinearLayoutManager(getContext()));
        rv_pending.setAdapter(listAdapter);
        rv_pending_goods.setAdapter(goodsInPendingAdapter);
        //左侧挂单列表
        listAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<PendingOrderModel.PendingOrder>() {
            @Override
            public void onItemClick(View covertView, int position, PendingOrderModel.PendingOrder data) {
                if(position != last_order_position){
                    if(last_order_position >= 0){
                        listAdapter.getItem(last_order_position).setIs_checked(false);
                        listAdapter.notifyItemChanged(last_order_position);
                    }
                    data.setIs_checked(true);
                    last_order_position = position;
                    listAdapter.notifyItemChanged(last_order_position);
                    goodsInPendingAdapter.setDatas(listAdapter.getItem(last_order_position).getGood_list());
                    rv_pending_goods.setAdapter(goodsInPendingAdapter);
                }
            }
        });
        //取消
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseNiceDialog.dismiss();
            }
        });
        //确定
        btn_chose_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(last_order_position >= 0){
                    baseNiceDialog.dismiss();
                    CashierHomeFragment.getInstance().getPendingOrderRefresh(pendingOrderModel.getOrder_list().get(last_order_position));
                    pendingOrderModel.getOrder_list().remove(last_order_position);
                    pendingOrderModel.setOrder_list(pendingOrderModel.getOrder_list());
                    PendingOrdersModelDao.insertModel(pendingOrderModel);
                    CashierHomeFragment.getInstance().refreshPendingOrders();
                }else{
                    SDToast.showToast(getContext(),"请选择订单");
                }
            }
        });

    }
}
