package com.tchcn.supermarket.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.utils.DisplayUtils;

/**
 *  取消付款
 * Created by humu on 2018/5/5.
 */

public class CancelPaymentDialog extends BaseNiceDialog {

    private static SetOnConfirmListener onConfirmListener;

    public static CancelPaymentDialog newInstance(SetOnConfirmListener listener){
        CancelPaymentDialog dialog = new CancelPaymentDialog();
        onConfirmListener = listener;
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setWidth(300);
/*        setWidth(DisplayUtils.px2dp(getContext(),600));
        setHeight(DisplayUtils.px2dp(getContext(),371));*/
    }

    public void addOnConfirmListener(SetOnConfirmListener listener){
        onConfirmListener = listener;
    }

    @Override
    public int intLayoutId() {
        return R.layout.sdialog_cancel_payment;
    }

    @Override
    public void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
        TextView tv_confirm = viewHolder.getView(R.id.tv_confirm);
        TextView tv_cancel = viewHolder.getView(R.id.tv_cancel);
        tv_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseNiceDialog.dismiss();
            }
        });
        tv_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseNiceDialog.dismiss();
                onConfirmListener.onConfirm();
            }
        });
    }

    public interface SetOnConfirmListener{

        void onConfirm();

    }

}
