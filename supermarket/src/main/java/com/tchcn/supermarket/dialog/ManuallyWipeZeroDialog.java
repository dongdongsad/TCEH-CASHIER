package com.tchcn.supermarket.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.utils.DisplayUtils;
import com.tchcn.supermarket.utils.FormatUtil;
import com.tchcn.supermarket.utils.SDToast;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 *  手动抹零
 * Created by humu on 2018/5/5.
 */

public class ManuallyWipeZeroDialog extends BaseNiceDialog implements View.OnClickListener{

    private static SetOnConfirmListener onConfirmListener;
    private TextView tv_wipe_amount;

    public static ManuallyWipeZeroDialog newInstance(SetOnConfirmListener listener){
        ManuallyWipeZeroDialog dialog = new ManuallyWipeZeroDialog();
        onConfirmListener = listener;
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setWidth(DisplayUtils.px2dp(getContext(),500));
        //setHeight(DisplayUtils.px2dp(getContext(),700));
    }

    @Override
    public int intLayoutId() {
        return R.layout.sdialog_manually_wipe_zero;
    }

    @Override
    public void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
        tv_wipe_amount = viewHolder.getView(R.id.tv_wipe_amount);
        Button btn_confirm_wipe = viewHolder.getView(R.id.btn_confirm_wipe);
        Button btn_reset = viewHolder.getView(R.id.btn_reset);
        Button btn_0 = viewHolder.getView(R.id.btn_0);
        Button btn_1 = viewHolder.getView(R.id.btn_1);
        Button btn_2 = viewHolder.getView(R.id.btn_2);
        Button btn_3 = viewHolder.getView(R.id.btn_3);
        Button btn_4 = viewHolder.getView(R.id.btn_4);
        Button btn_5 = viewHolder.getView(R.id.btn_5);
        Button btn_6 = viewHolder.getView(R.id.btn_6);
        Button btn_7 = viewHolder.getView(R.id.btn_7);
        Button btn_8 = viewHolder.getView(R.id.btn_8);
        Button btn_9 = viewHolder.getView(R.id.btn_9);
        Button btn_00 = viewHolder.getView(R.id.btn_00);
        Button btn_10 = viewHolder.getView(R.id.btn_10);
        Button btn_20 = viewHolder.getView(R.id.btn_20);
        Button btn_30 = viewHolder.getView(R.id.btn_30);
        Button btn_point = viewHolder.getView(R.id.btn_point);
        RelativeLayout rl_del = viewHolder.getView(R.id.rl_del);
        ImageView iv_clear = viewHolder.getView(R.id.iv_clear);
        btn_0.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);
        btn_00.setOnClickListener(this);
        btn_10.setOnClickListener(this);
        btn_20.setOnClickListener(this);
        btn_30.setOnClickListener(this);
        btn_point.setOnClickListener(this);
        rl_del.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_confirm_wipe.setOnClickListener(this);
        iv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_wipe_amount.setText("");
            }
        });
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        StringBuilder builder = new StringBuilder(tv_wipe_amount.getText().toString());
        if(id == R.id.btn_reset){
            if(onConfirmListener != null){
                onConfirmListener.onReset();
            }
            dismiss();
        }else if(id == R.id.btn_confirm_wipe){
            if(FormatUtil.isFloat(tv_wipe_amount.getText().toString())){
                onConfirmListener.onConfirm(Float.parseFloat(tv_wipe_amount.getText().toString()));
                dismiss();
            }else{
                SDToast.showToast(getContext(),"格式错误");
            }
        }else if(id == R.id.btn_0){
            builder.append("0");
        }else if(id == R.id.btn_1){
            builder.append("1");
        }else if(id == R.id.btn_2){
            builder.append("2");
        }else if(id == R.id.btn_3){
            builder.append("3");
        }else if(id == R.id.btn_4){
            builder.append("4");
        }else if(id == R.id.btn_5){
            builder.append("5");
        }else if(id == R.id.btn_6){
            builder.append("6");
        }else if(id == R.id.btn_7){
            builder.append("7");
        }else if(id == R.id.btn_8){
            builder.append("8");
        }else if(id == R.id.btn_9){
            builder.append("9");
        }else if(id == R.id.btn_00){
            builder.append("00");
        }else if(id == R.id.btn_point){
            builder.append(".");
        }else if(id == R.id.btn_10){
            onConfirmListener.onConfirm(10f);
            dismiss();
        }else if(id == R.id.btn_20){
            onConfirmListener.onConfirm(20f);
            dismiss();
        }else if(id == R.id.btn_30){
            onConfirmListener.onConfirm(30f);
            dismiss();
        }else if(id == R.id.rl_del){
            if(tv_wipe_amount.getText().toString().length() > 0){
                builder.deleteCharAt(tv_wipe_amount.getText().toString().length() - 1);
            }
        }
        ViewBinder.setTextView(tv_wipe_amount,builder.toString());
    }

    public interface SetOnConfirmListener{

        void onConfirm(Float wipeMoney);

        void onReset();

    }

}
