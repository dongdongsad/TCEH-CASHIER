package com.tchcn.supermarket.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.utils.DisplayUtils;
import com.tchcn.supermarket.utils.FormatUtil;
import com.tchcn.supermarket.utils.SDToast;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 *  商品数量/入库数量
 * Created by humu on 2018/5/5.
 */

public class GoodNumInputDialog extends BaseNiceDialog implements View.OnClickListener{

    private static SetOnConfirmListener onConfirmListener;
    private TextView tv_good_num_result;

    public static final String STORAGE = "storage"; //商品入库
    public static final String CHANGE_NUM = "change_num"; //改变购物车数量
    private String type = CHANGE_NUM;

    public static GoodNumInputDialog newInstance(String type,String currentNum,SetOnConfirmListener listener){
        GoodNumInputDialog dialog = new GoodNumInputDialog();
        onConfirmListener = listener;
        Bundle bundle = new Bundle();
        bundle.putString("type",type);
        bundle.putString("currentNum",currentNum);
        dialog.setArguments(bundle);
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setWidth(DisplayUtils.px2dp(getContext(),500));
        //setHeight(DisplayUtils.px2dp(getContext(),584));
    }

    @Override
    public int intLayoutId() {
        return R.layout.sdialog_good_num_input;
    }

    @Override
    public void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
        tv_good_num_result = viewHolder.getView(R.id.tv_good_num_result);
        TextView tv_title = viewHolder.getView(R.id.tv_title);
        type = getArguments().getString("type");
        if(type == CHANGE_NUM){
            tv_good_num_result.setHint("输入数量（直接确认默认数量为1）");
            ViewBinder.setTextView(tv_title,"商品数量");
        }else{
            tv_good_num_result.setHint("输入入库数量或重量");
            ViewBinder.setTextView(tv_title,"入库数量");
        }
        ViewBinder.setTextView(tv_good_num_result,getArguments().getString("currentNum"));
        Button btn_confirm_num = viewHolder.getView(R.id.btn_confirm_num);
        Button btn_cancel = viewHolder.getView(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                baseNiceDialog.dismiss();
            }
        });
        btn_confirm_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String result = tv_good_num_result.getText().toString();
                if(!TextUtils.isEmpty(result)){
                    if(FormatUtil.isFloat(result)){
                        baseNiceDialog.dismiss();
                        onConfirmListener.onConfirm(Float.parseFloat(result)+"");
                    }else{
                        SDToast.showToast(getContext(),"格式错误");
                    }
                }else{
                    if(type == CHANGE_NUM){
                        baseNiceDialog.dismiss();
                        onConfirmListener.onConfirm("1");
                    }else{
                        SDToast.showToast(getContext(),"请输入入库数量");
                    }
                }
            }
        });

        Button btn_0 = viewHolder.getView(R.id.btn_0);
        Button btn_1 = viewHolder.getView(R.id.btn_1);
        Button btn_2 = viewHolder.getView(R.id.btn_2);
        Button btn_3 = viewHolder.getView(R.id.btn_3);
        Button btn_4 = viewHolder.getView(R.id.btn_4);
        Button btn_5 = viewHolder.getView(R.id.btn_5);
        Button btn_6 = viewHolder.getView(R.id.btn_6);
        Button btn_7 = viewHolder.getView(R.id.btn_7);
        Button btn_8 = viewHolder.getView(R.id.btn_8);
        Button btn_9 = viewHolder.getView(R.id.btn_9);
        RelativeLayout rl_del = viewHolder.getView(R.id.rl_del);
        Button btn_point = viewHolder.getView(R.id.btn_point);
        ImageView iv_clear = viewHolder.getView(R.id.iv_clear);
        btn_0.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);
        rl_del.setOnClickListener(this);
        btn_point.setOnClickListener(this);
        iv_clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tv_good_num_result.setText("");
            }
        });

    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        StringBuilder builder = new StringBuilder(tv_good_num_result.getText().toString());
        if(id == R.id.btn_0){
            builder.append(0);
        }else if(id == R.id.btn_1){
            builder.append(1);
        }else if(id == R.id.btn_2){
            builder.append(2);
        }else if(id == R.id.btn_3){
            builder.append(3);
        }else if(id == R.id.btn_4){
            builder.append(4);
        }else if(id == R.id.btn_5){
            builder.append(5);
        }else if(id == R.id.btn_6){
            builder.append(6);
        }else if(id == R.id.btn_7){
            builder.append(7);
        }else if(id == R.id.btn_8){
            builder.append(8);
        }else if(id == R.id.btn_9){
            builder.append(9);
        }else if(id == R.id.rl_del){
            if(tv_good_num_result.getText().toString().length() > 0){
                builder.deleteCharAt(tv_good_num_result.getText().toString().length() - 1);
            }
        }else if(id == R.id.btn_point){
            builder.append(".");
        }

        ViewBinder.setTextView(tv_good_num_result,builder.toString());

    }

    public interface SetOnConfirmListener{

        void onConfirm(String discount_result);

    }

}
