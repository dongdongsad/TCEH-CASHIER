package com.tchcn.supermarket.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.utils.DisplayUtils;
import com.tchcn.supermarket.utils.FormatUtil;
import com.tchcn.supermarket.utils.SDToast;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 *  单品折扣/整单折扣
 * Created by humu on 2018/5/5.
 */

public class DiscountInputDialog extends BaseNiceDialog implements View.OnClickListener{

    private static SetOnConfirmListener onConfirmListener;
    private static String title;
    private static float totalMoney = 0.0f;
    private TextView tv_discount_result;

    public static DiscountInputDialog newInstance(String type,float money,SetOnConfirmListener listener){
        DiscountInputDialog dialog = new DiscountInputDialog();
        title = type;
        onConfirmListener = listener;
        totalMoney = money;
        return dialog;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setWidth(DisplayUtils.px2dp(getContext(),500));
/*        setWidth(DisplayUtils.px2dp(getContext(),500));
        setHeight(DisplayUtils.px2dp(getContext(),700));*/
    }

    @Override
    public int intLayoutId() {
        return R.layout.sdialog_discount_input;
    }

    @Override
    public void convertView(ViewHolder viewHolder, final BaseNiceDialog baseNiceDialog) {
        TextView tv_discount_type = viewHolder.getView(R.id.tv_discount_type);
        tv_discount_result = viewHolder.getView(R.id.tv_discount_result);
        Button btn_confirm_discount = viewHolder.getView(R.id.btn_confirm_discount);
        Button btn_reset = viewHolder.getView(R.id.btn_reset);
        Button btn_0 = viewHolder.getView(R.id.btn_0);
        Button btn_00 = viewHolder.getView(R.id.btn_00);
        Button btn_point = viewHolder.getView(R.id.btn_point);
        Button btn_1 = viewHolder.getView(R.id.btn_1);
        Button btn_2 = viewHolder.getView(R.id.btn_2);
        Button btn_3 = viewHolder.getView(R.id.btn_3);
        Button btn_4 = viewHolder.getView(R.id.btn_4);
        Button btn_5 = viewHolder.getView(R.id.btn_5);
        Button btn_6 = viewHolder.getView(R.id.btn_6);
        Button btn_7 = viewHolder.getView(R.id.btn_7);
        Button btn_8 = viewHolder.getView(R.id.btn_8);
        Button btn_9 = viewHolder.getView(R.id.btn_9);
        RelativeLayout rl_del = viewHolder.getView(R.id.rl_del);
        Button btn_75 = viewHolder.getView(R.id.btn_75);
        Button btn_85 = viewHolder.getView(R.id.btn_85);
        Button btn_95 = viewHolder.getView(R.id.btn_95);
        ImageView iv_clear = viewHolder.getView(R.id.iv_clear);

        tv_discount_type.setText(title);
        if("整单折扣".equals(title)){
            tv_discount_result.setHint("输入整单折扣");
        }else{
            tv_discount_result.setHint("输入单品的折扣");
        }
        btn_confirm_discount.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_0.setOnClickListener(this);
        btn_00.setOnClickListener(this);
        btn_point.setOnClickListener(this);
        btn_1.setOnClickListener(this);
        btn_2.setOnClickListener(this);
        btn_3.setOnClickListener(this);
        btn_4.setOnClickListener(this);
        btn_5.setOnClickListener(this);
        btn_6.setOnClickListener(this);
        btn_7.setOnClickListener(this);
        btn_8.setOnClickListener(this);
        btn_9.setOnClickListener(this);
        rl_del.setOnClickListener(this);
        btn_75.setOnClickListener(this);
        btn_85.setOnClickListener(this);
        btn_95.setOnClickListener(this);
        iv_clear.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        String result = tv_discount_result.getText().toString();
        StringBuilder builder = new StringBuilder(result);
        if(id == R.id.btn_confirm_discount){
            if(FormatUtil.isFloat(result)){
                float currentAccount = Float.parseFloat(result);
                if(currentAccount < 1){
                    if(onConfirmListener != null){
                        onConfirmListener.onConfirm(currentAccount,(totalMoney * currentAccount)+"");
                    }
                }else if(currentAccount > 1){
                    if(onConfirmListener != null){
                        onConfirmListener.onConfirm(currentAccount / 100,(totalMoney * (currentAccount / 100))+"");
                    }
                }
                dismiss();
            }else{
                SDToast.showToast(getContext(),"折扣格式错误");
            }
        }else if(id == R.id.btn_reset){
            if(onConfirmListener != null){
                onConfirmListener.onReset();
            }
            dismiss();
        }else if(id == R.id.btn_0){
            builder.append("0");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_00){
            builder.append("00");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_point){
            builder.append(".");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_1){
            builder.append("1");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_2){
            builder.append("2");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_3){
            builder.append("3");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_4){
            builder.append("4");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_5){
            builder.append("5");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_6){
            builder.append("6");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_7){
            builder.append("7");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_8){
            builder.append("8");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.btn_9){
            builder.append("9");
            ViewBinder.setTextView(tv_discount_result,builder.toString());
        }else if(id == R.id.rl_del){
            if(!TextUtils.isEmpty(result)){
                builder.deleteCharAt(result.length() - 1);
                ViewBinder.setTextView(tv_discount_result,builder.toString());
            }
        }else if(id == R.id.btn_75){
            if(onConfirmListener != null){
                onConfirmListener.onConfirm(0.75f,(totalMoney * 0.75)+"");
                dismiss();
            }
        }else if(id == R.id.btn_85){
            if(onConfirmListener != null){
                onConfirmListener.onConfirm(0.85f,(totalMoney * 0.85)+"");
                dismiss();
            }
        }else if(id == R.id.btn_95){
            if(onConfirmListener != null){
                onConfirmListener.onConfirm(0.95f,(totalMoney * 0.95)+"");
                dismiss();
            }
        }else if(id == R.id.iv_clear){
            tv_discount_result.setText("");
        }
    }

    public interface SetOnConfirmListener{

        void onConfirm(float discount,String discount_result);

        void onReset();

    }

}
