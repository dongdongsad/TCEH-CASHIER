package com.tchcn.supermarket.utils;

import java.util.regex.Pattern;

/**
 * Created by Administrator on 2018/12/6.
 */

public class FormatUtil {

    public static boolean isFloat(String str){
        try {
            Float.parseFloat(str);
            return true;
        }catch (NumberFormatException e){
            return false;
        }
    }


    /*
     * 是否为浮点数？double或float类型。
     * @param str 传入的字符串。
     * @return 是浮点数返回true,否则返回false。
     */
    public static boolean isDoubleOrFloat(String str) {
        Pattern pattern = Pattern.compile("^[-\\+]?[.\\d]*$");
        return pattern.matcher(str).matches();
    }

    /**
     * 判断字符串是否为数字
     *
     * @param str
     * @return boolean
     */
    public static boolean isNumber(String str)
    {

        if (null == str || "".equals(str)) {
            return false;
        }
        Pattern pattern = Pattern.compile("^[-\\+]?[.\\d]*$");
        return pattern.matcher(str).matches();
    }

}
