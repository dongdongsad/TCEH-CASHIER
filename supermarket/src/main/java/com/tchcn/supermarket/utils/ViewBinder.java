package com.tchcn.supermarket.utils;

import android.text.TextUtils;
import android.widget.TextView;

/**
 * Created by humu on 2018/4/28.
 */

public class ViewBinder {

    public static void setTextView(TextView textView, String text) {

        if (textView == null) {
            return;
        }

        if (!TextUtils.isEmpty(text)) {
            textView.setText(text);
        } else {
            textView.setText("");
        }

    }

}
