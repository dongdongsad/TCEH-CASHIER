package com.tchcn.supermarket.utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Administrator on 2018/5/5.
 */

public class TimeUtils {

    /**
     * 获取时间戳
     * @return 获取时间戳
     */
    public static String getTimeString() {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY, 8);
        return df.format(calendar.getTime());
    }

    /**
     * 库存查询-操作记录-时间
     * @param time 时间戳
     * @return
     */
    public static String getTimeFormatInOperate(String time){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(Long.parseLong(time));
        calendar.add(Calendar.HOUR_OF_DAY, 8);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);

        String cMonth = "";
        if(month < 10){
            cMonth = "0"+month;
        }else{
            cMonth = month+"";
        }
        String cDay = "";
        if(day < 10){
            cDay = "0"+day;
        }else{
            cDay = day+"";
        }
        String cHour = "";
        if(hour < 10){
            cHour = "0"+hour;
        }else{
            cHour = hour+"";
        }
        String cMinute = "";
        if(minute < 10){
            cMinute = "0"+minute;
        }else{
            cMinute = minute+"";
        }

        return year+"-"+cMonth+"-"+cDay+" "+cHour+":"+cMinute;
    }

}
