package com.tchcn.supermarket.utils;

import java.util.Collection;

/**
 *  集合工具类
 * Created by humu on 2018/4/28.
 */

public class CollectionUtil {

    //为空或长度为0
    public static boolean isEmpty(Collection<?> collection){
        return collection == null || collection.size() == 0;
    }

    //获取列表长度
    public static int size(Collection<?> collection){
        return collection != null ? collection.size():0;
    }

}
