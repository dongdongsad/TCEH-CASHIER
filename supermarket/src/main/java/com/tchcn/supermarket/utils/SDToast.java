package com.tchcn.supermarket.utils;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.widget.Toast;

/**
 * toast显示类，可以在子线程直接调用
 * 
 */
public class SDToast
{
	private static Toast toast;

	public static Handler mHandler = new Handler(Looper.getMainLooper());

	public static void showToast(Context context,String text)
	{
		showToast(context,text, Toast.LENGTH_SHORT);
	}

	public static void showToast(final Context context, final String text, final int duration)
	{
		if (Looper.myLooper() == Looper.getMainLooper())
		{
			show(context,text, duration);
		} else
		{
			mHandler.post(new Runnable()
			{
				@Override
				public void run()
				{
					show(context,text, duration);
				}
			});
		}
	}

	private static void show(Context context,String text, int duration)
	{
		if (TextUtils.isEmpty(text))
		{
			return;
		}
		if (toast != null)
		{
			toast.cancel();
		}
		toast = Toast.makeText(context, text, duration);
		toast.show();
	}

}
