package com.tchcn.supermarket.dbmodel;

import com.tchcn.supermarket.model.Good;

import java.io.Serializable;
import java.util.List;

/** 挂单
 * Created by humu on 2018/5/4.
 */

public class PendingOrderModel implements Serializable{

    private static final long serialVersionUID = 1L;
    private List<PendingOrder> order_list;

    public List<PendingOrder> getOrder_list() {
        return order_list;
    }

    public void setOrder_list(List<PendingOrder> order_list) {
        this.order_list = order_list;
    }

    public static class PendingOrder{

        private List<Good> good_list;
        private float money;
        private int good_num;
        private String pend_time;
        private float real_money;
        private float offer_money;
        private boolean is_checked;

        public float getReal_money() {
            return real_money;
        }

        public void setReal_money(float real_money) {
            this.real_money = real_money;
        }

        public float getOffer_money() {
            return offer_money;
        }

        public void setOffer_money(float offer_money) {
            this.offer_money = offer_money;
        }

        public boolean isIs_checked() {
            return is_checked;
        }

        public void setIs_checked(boolean is_checked) {
            this.is_checked = is_checked;
        }

        public String getPend_time() {
            return pend_time;
        }

        public void setPend_time(String pend_time) {
            this.pend_time = pend_time;
        }

        public int getGood_num() {
            return good_num;
        }

        public void setGood_num(int good_num) {
            this.good_num = good_num;
        }

        public List<Good> getGood_list() {
            return good_list;
        }

        public void setGood_list(List<Good> good_list) {
            this.good_list = good_list;
        }

        public float getMoney() {
            return money;
        }

        public void setMoney(float money) {
            this.money = money;
        }

    }

}
