package com.tchcn.supermarket.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.MenuListActModel;
import com.tchcn.supermarket.utils.ViewBinder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2018/10/31.
 */

public class UserPermissionAdapter extends BaseRecyclerAdapter<MenuListActModel.MenuListData.MenuOperateModel> {

    private OnMenuChangedListener onMenuChangedListener;
    private List<MenuListActModel.MenuListData.MenuOperateModel> checkedModelList = new ArrayList<>();

    @Override
    public void setDatas(List<MenuListActModel.MenuListData.MenuOperateModel> datas) {
        checkedModelList.clear();
        super.setDatas(datas);
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_user_permission;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final MenuListActModel.MenuListData.MenuOperateModel data) {
        LinearLayout ll_main = getView(holder,R.id.ll_main);
        final ImageView iv_permission = getView(holder,R.id.iv_permission);
        TextView tv_p_name = getView(holder,R.id.tv_p_name);
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ll_main.getLayoutParams();
        if(position == 0 || position % 4 == 0){
            params.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
        }else if((position + 1) % 4 == 0){
            params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        }else{
            params.addRule(RelativeLayout.CENTER_HORIZONTAL);
        }
        ll_main.setLayoutParams(params);

        ViewBinder.setTextView(tv_p_name,data.getMenu_name());

        if("1".equals(data.getIs_auth())){
            checkedModelList.add(data);
            iv_permission.setImageResource(R.drawable.ic_has_permission);
        }else{
            iv_permission.setImageResource(R.drawable.ic_no_permission);
        }

        ll_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if("1".equals(data.getIs_auth())){
                    iv_permission.setImageResource(R.drawable.ic_no_permission);
                    data.setIs_auth("0");
                    if(checkedModelList.contains(data)){
                        checkedModelList.remove(data);
                    }
                }else{
                    iv_permission.setImageResource(R.drawable.ic_has_permission);
                    data.setIs_auth("1");
                    if(!checkedModelList.contains(data)){
                        checkedModelList.add(data);
                    }
                }
                if(onMenuChangedListener != null){
                    onMenuChangedListener.onMenuChanged(getCheckedMenus());
                }
            }
        });
    }

    private String getCheckedMenus(){
        String result = "";
        if(checkedModelList.size() > 0){
            for(int i = 0;i<checkedModelList.size();i++){
                MenuListActModel.MenuListData.MenuOperateModel model = checkedModelList.get(i);
                if(i < checkedModelList.size() - 1){
                    result = result + model.getId()+",";
                }else{
                    result = result + model.getId();
                }
            }
        }
        return result;
    }

    public void setOnMenuChangedListener(OnMenuChangedListener onMenuChangedListener){
        this.onMenuChangedListener = onMenuChangedListener;
    }

    public interface OnMenuChangedListener{
        void onMenuChanged(String ids);
    }

}
