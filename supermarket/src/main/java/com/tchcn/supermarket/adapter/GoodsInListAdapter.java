package com.tchcn.supermarket.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.NoCodeGoodActModel;
import com.tchcn.supermarket.utils.GlideUtil;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 * Created by Administrator on 2018/4/29.
 */

public class GoodsInListAdapter extends BaseRecyclerAdapter<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_good_in_list;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel data) {
        ImageView iv_good_img = getView(holder,R.id.iv_good_img);
        TextView tv_good_name = getView(holder,R.id.tv_good_name);
        TextView tv_unit_price = getView(holder,R.id.tv_unit_price);
        TextView tv_stock = getView(holder,R.id.tv_stock);
        ViewBinder.setTextView(tv_unit_price,"¥"+data.getRetail_price());
        ViewBinder.setTextView(tv_good_name,data.getCate_name());
        ViewBinder.setTextView(tv_stock,"库存："+data.getStock()+data.getUnit());
        GlideUtil.load(context,data.getImage(),iv_good_img);

    }
}
