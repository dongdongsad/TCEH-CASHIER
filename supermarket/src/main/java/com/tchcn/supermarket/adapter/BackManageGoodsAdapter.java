package com.tchcn.supermarket.adapter;

import android.widget.ImageView;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.StockListActModel;
import com.tchcn.supermarket.utils.GlideUtil;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 * Created by Administrator on 2018/4/29.
 */

public class BackManageGoodsAdapter extends BaseRecyclerAdapter<StockListActModel.StockListData.StockModel> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_backstage_good;
    }

    @Override
    public void setUpData(final CommonHolder holder, int position, int viewType, StockListActModel.StockListData.StockModel data) {
        ImageView iv_good_img = getView(holder,R.id.iv_good_img);
        TextView tv_good_name = getView(holder,R.id.tv_good_name);
        TextView tv_price = getView(holder,R.id.tv_price);
        TextView tv_warning_line = getView(holder,R.id.tv_warning_line);

        GlideUtil.load(context,data.getImage(),iv_good_img);
        ViewBinder.setTextView(tv_good_name,data.getCate_name());
        ViewBinder.setTextView(tv_price,"售价：¥"+data.getRetail_price());
        ViewBinder.setTextView(tv_warning_line,"预警"+data.getStock_warning()+data.getUnit());
    }

}
