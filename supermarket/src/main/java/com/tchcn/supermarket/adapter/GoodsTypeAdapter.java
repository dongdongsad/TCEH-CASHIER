package com.tchcn.supermarket.adapter;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.GoodTypeActModel;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 * Created by Administrator on 2018/4/29.
 */

public class GoodsTypeAdapter extends BaseRecyclerAdapter<GoodTypeActModel.GoodTypeData.GoodType> implements View.OnClickListener{

    private OnItemClickListener mOnItemClickListener = null;

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_good_type;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, GoodTypeActModel.GoodTypeData.GoodType data) {
        RelativeLayout rl_main = getView(holder,R.id.rl_main);
        rl_main.setTag(position);
        rl_main.setOnClickListener(this);
        TextView tv_type_name = getView(holder,R.id.tv_type_name);
        View view_checked = getView(holder,R.id.view_checked);

        ViewBinder.setTextView(tv_type_name,data.getName());
        if(data.isChecked()){
            view_checked.setVisibility(View.VISIBLE);
            tv_type_name.setTextColor(holder.itemView.getResources().getColor(R.color.main_blue));
        }else{
            view_checked.setVisibility(View.GONE);
            tv_type_name.setTextColor(holder.itemView.getResources().getColor(R.color.six_six));
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取position
            mOnItemClickListener.onItemClick(v,v.getId(),(int)v.getTag());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view , int id,int position);
    }

}
