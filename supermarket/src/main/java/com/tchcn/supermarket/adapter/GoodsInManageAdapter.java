package com.tchcn.supermarket.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.GoodManageModel;
import com.tchcn.supermarket.model.StockOperateActModel;
import com.tchcn.supermarket.utils.GlideUtil;
import com.tchcn.supermarket.utils.TimeUtils;
import com.tchcn.supermarket.utils.ViewBinder;

import java.util.Date;

/**
 * Created by Administrator on 2018/5/7.
 */

public class GoodsInManageAdapter extends BaseRecyclerAdapter<StockOperateActModel.StockOperateData.StockOperateModel> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_good_manage;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, StockOperateActModel.StockOperateData.StockOperateModel data) {
        SimpleDraweeView sdv = getView(holder,R.id.sdv);
        TextView tv_price = getView(holder,R.id.tv_price);
        TextView tv_operator_name = getView(holder,R.id.tv_operator_name);
        TextView tv_good_type = getView(holder,R.id.tv_good_type);
        TextView tv_operate_time = getView(holder,R.id.tv_operate_time);
        ImageView iv_good_img = getView(holder,R.id.iv_good_img);
        TextView tv_good_name = getView(holder,R.id.tv_good_name);
        TextView tv_in_num = getView(holder,R.id.tv_in_num);
        if("1".equals(data.getIs_store())){
            ViewBinder.setTextView(tv_in_num,"入库"+data.getNum()+data.getUnit());
            tv_price.setVisibility(View.GONE);
        }else if("2".equals(data.getIs_store())){
            ViewBinder.setTextView(tv_in_num,"出库"+data.getNum()+data.getUnit());
            tv_price.setVisibility(View.VISIBLE);
        }
        ViewBinder.setTextView(tv_operator_name,data.getUser_name());
        ViewBinder.setTextView(tv_good_type,data.getDeal_name());
        GlideUtil.load(context,data.getImage(),iv_good_img);
        GlideUtil.load(context,data.getUser_image(),sdv);
        ViewBinder.setTextView(tv_good_name,data.getDealcate_name());
        ViewBinder.setTextView(tv_price,"售价：¥"+data.getPrice());

        ViewBinder.setTextView(tv_operate_time,TimeUtils.getTimeFormatInOperate(data.getCreate_time()));

    }

    public void notifyModeChanged(String is_store){
        if(getDatas() != null && getDatas().size() > 0){
            for(StockOperateActModel.StockOperateData.StockOperateModel model : getDatas()){
                model.setIs_store(is_store);
            }
            notifyDataSetChanged();
        }
    }

}
