package com.tchcn.supermarket.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fanwe.library.adapter.http.model.SDResponse;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.model.BaseActModel;
import com.tchcn.library.model.UserInfoModel;
import com.tchcn.library.utils.LogUtil;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.common.SCommonInterface;
import com.tchcn.supermarket.dialog.GoodNumInputDialog;
import com.tchcn.supermarket.model.StockListActModel;
import com.tchcn.supermarket.ui.fragment.CommodityStorageFragment;
import com.tchcn.supermarket.utils.GlideUtil;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 *  商品入库的列表
 * Created by humu on 2018/5/7.
 */

public class CSGoodsAdapter extends BaseRecyclerAdapter<StockListActModel.StockListData.StockModel> {

    private final CommodityStorageFragment fragment;

    public CSGoodsAdapter(CommodityStorageFragment fragment){
        this.fragment = fragment;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_cs_goods;
    }

    @Override
    public void setUpData(final CommonHolder holder, final int position, int viewType, final StockListActModel.StockListData.StockModel data) {
        TextView tv_warning_line = getView(holder,R.id.tv_warning_line);
        TextView tv_good_type = getView(holder,R.id.tv_good_type);
        ImageView iv_good_img = getView(holder,R.id.iv_good_img);
        TextView tv_good_name = getView(holder,R.id.tv_good_name);
        TextView tv_stock_num = getView(holder,R.id.tv_stock_num);
        RelativeLayout rl_storage = getView(holder,R.id.rl_storage);
        if(data.getMode() == 0){
            tv_warning_line.setVisibility(View.GONE);
        }else if(data.getMode() == 1){
            tv_warning_line.setVisibility(View.VISIBLE);
        }

        GlideUtil.load(context,data.getImage(),iv_good_img);
        ViewBinder.setTextView(tv_good_name,data.getCate_name());
        ViewBinder.setTextView(tv_stock_num,"库存"+data.getStock()+data.getUnit());
        ViewBinder.setTextView(tv_warning_line,"预警线:"+data.getStock_warning()+data.getUnit());
        ViewBinder.setTextView(tv_good_type,data.getDeal_name());

        rl_storage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodNumInputDialog.newInstance(GoodNumInputDialog.STORAGE,"",new GoodNumInputDialog.SetOnConfirmListener() {
                    @Override
                    public void onConfirm(final String discount_result) {
                        SCommonInterface.storeGoods(Float.parseFloat(discount_result), UserInfoModel.getUser_id(), data.getId(), new AppRequestCallback<BaseActModel>() {
                                    @Override
                                    protected void onSuccess(SDResponse sdResponse) {
                                        if(actModel.isOk()){
                                            float currentStock = Float.parseFloat(data.getStock());
                                            data.setStock((currentStock+Float.parseFloat(discount_result))+"");
                                            notifyItemChanged(position);
                                        }
                                    }
                                });
                    }
                }).setOutCancel(true).show(fragment.getChildFragmentManager());

            }
        });

    }

    public void notifyModeChanged(int mode){
        for(StockListActModel.StockListData.StockModel model : getDatas()){
            model.setMode(mode);
        }
        notifyDataSetChanged();
    }

}
