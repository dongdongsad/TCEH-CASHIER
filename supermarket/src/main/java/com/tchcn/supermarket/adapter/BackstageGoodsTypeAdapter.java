package com.tchcn.supermarket.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.GoodTypeActModel;
import com.tchcn.supermarket.ui.fragment.GoodsManageFragment;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 * Created by humu on 2018/4/29.
 */

public class BackstageGoodsTypeAdapter extends BaseRecyclerAdapter<GoodTypeActModel.GoodTypeData.GoodType> implements View.OnClickListener{

    private OnItemClickListener mOnItemClickListener = null;
    //0商品分类,1切换商品分类
    private int mode = 0;

    public BackstageGoodsTypeAdapter(int mode) {
        this.mode = mode;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        if(mode == 1){
            return R.layout.sitem_switch_good_type;
        }
        return R.layout.sitem_backstage_good_type;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, final GoodTypeActModel.GoodTypeData.GoodType data) {
        if(mode != 1){
            RelativeLayout rl_bg = getView(holder,R.id.rl_bg);
            TextView tv_type_name = getView(holder,R.id.tv_type_name);
            ViewBinder.setTextView(tv_type_name,data.getName());
            ImageView iv_edit = getView(holder,R.id.iv_edit);

            if(data.isChecked()){
                tv_type_name.setTextColor(context.getResources().getColor(R.color.main_blue));
                rl_bg.setBackgroundColor(context.getResources().getColor(R.color.gray_bg));
            }else{
                tv_type_name.setTextColor(context.getResources().getColor(R.color.six_six));
                rl_bg.setBackgroundColor(context.getResources().getColor(R.color.white));
            }

            iv_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    GoodsManageFragment.getInstance().showGoodTypePopup("编辑分类",data.getId(),data.getName());
                }
            });
        }else{
            RelativeLayout rl_bg = getView(holder,R.id.rl_bg);
            TextView tv_type_name = getView(holder,R.id.tv_type_name);
            ViewBinder.setTextView(tv_type_name,data.getName());
            if(data.isChecked()){
                tv_type_name.setTextColor(context.getResources().getColor(R.color.main_blue));
                rl_bg.setBackgroundColor(context.getResources().getColor(R.color.gray_bg));
            }else{
                tv_type_name.setTextColor(context.getResources().getColor(R.color.six_six));
                rl_bg.setBackgroundColor(context.getResources().getColor(R.color.white));
            }
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取position
            mOnItemClickListener.onItemClick(v,v.getId(),(int)v.getTag());
        }
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int id, int position);
    }

}
