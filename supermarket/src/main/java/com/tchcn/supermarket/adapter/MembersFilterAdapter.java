package com.tchcn.supermarket.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.LocationUserListActModel;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 *  商品分类筛选列表
 * Created by humu on 2018/5/7.
 */

public class MembersFilterAdapter extends BaseRecyclerAdapter<LocationUserListActModel.LocationUserListData.LocationUser> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_filter_good_type;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, LocationUserListActModel.LocationUserListData.LocationUser data) {
        TextView tv_type_name = getView(holder,R.id.tv_type_name);
        ImageView iv_checked = getView(holder,R.id.iv_checked);
        ViewBinder.setTextView(tv_type_name,data.getUser_name());
        if(data.isChecked()){
            tv_type_name.setTextColor(holder.itemView.getResources().getColor(R.color.main_blue));
            iv_checked.setVisibility(View.VISIBLE);
        }else{
            iv_checked.setVisibility(View.GONE);
            tv_type_name.setTextColor(holder.itemView.getResources().getColor(R.color.six_six));
        }
    }

}
