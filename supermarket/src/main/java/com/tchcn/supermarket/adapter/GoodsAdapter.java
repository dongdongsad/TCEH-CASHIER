package com.tchcn.supermarket.adapter;

import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.dialog.GoodNumInputDialog;
import com.tchcn.supermarket.model.Good;
import com.tchcn.supermarket.ui.fragment.CashierHomeFragment;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 * Created by Administrator on 2018/4/29.
 */

public class GoodsAdapter extends BaseRecyclerAdapter<Good> {

    private Fragment fragment;

    public GoodsAdapter(Fragment fragment){
        this.fragment = fragment;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_good;
    }

    @Override
    public void setUpData(final CommonHolder holder, int position, int viewType, final Good data) {
        LinearLayout ll_main = getView(holder,R.id.ll_main);
        TextView tv_id = getView(holder,R.id.tv_id);
        TextView tv_name = getView(holder,R.id.tv_name);
        final TextView tv_num = getView(holder,R.id.tv_num);
        final Button btn_change_num = getView(holder,R.id.btn_change_num);
        TextView tv_unit = getView(holder,R.id.tv_unit);
        ViewBinder.setTextView(tv_unit,data.getUnit());
        TextView tv_unit_price = getView(holder,R.id.tv_unit_price);
        ViewBinder.setTextView(tv_unit_price,data.getMoney()+"");
        final TextView tv_discount_price = getView(holder,R.id.tv_discount_price);
        if(data.getDiscount() > 0){
            ViewBinder.setTextView(tv_discount_price,data.getNum()*(data.getMoney()*data.getDiscount())+"");
        }else{
            ViewBinder.setTextView(tv_discount_price,"");
        }

        int num = position + 1;
        if(num < 10){
            ViewBinder.setTextView(tv_id,"0"+num);
        }else{
            ViewBinder.setTextView(tv_id,num+"");
        }
        ViewBinder.setTextView(tv_name,data.getName());
        ViewBinder.setTextView(tv_num,data.getNum()+"");
        btn_change_num.setText(data.getNum()+"");
        if(data.isIs_checked()){
            ll_main.setBackgroundResource(R.drawable.sshape_checked_good_bg);
            btn_change_num.setVisibility(View.VISIBLE);
        }else{
            btn_change_num.setVisibility(View.INVISIBLE);
            if((position+1) % 2 == 0){
                ll_main.setBackgroundColor(holder.itemView.getResources().getColor(R.color.three_f5));
            }else{
                ll_main.setBackgroundColor(holder.itemView.getResources().getColor(R.color.white));
            }
        }

        btn_change_num.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                GoodNumInputDialog.newInstance(GoodNumInputDialog.CHANGE_NUM,data.getNum()+"",new GoodNumInputDialog.SetOnConfirmListener() {
                    @Override
                    public void onConfirm(String discount_result) {
                        data.setNum(Float.parseFloat(discount_result));
                        if(data.getDiscount() > 0){
                            float discountMoney = data.getNum() * (data.getMoney() * data.getDiscount());
                            ViewBinder.setTextView(tv_discount_price,discountMoney+"");
                        }
                        ViewBinder.setTextView(tv_num,data.getNum()+"");
                        ViewBinder.setTextView(btn_change_num,data.getNum()+"");
                        CashierHomeFragment.getInstance().refreshMoneyAndNum();
                    }
                }).setOutCancel(true).show(fragment.getChildFragmentManager());
            }
        });

    }

}
