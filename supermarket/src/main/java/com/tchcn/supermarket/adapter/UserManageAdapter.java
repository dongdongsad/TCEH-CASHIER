package com.tchcn.supermarket.adapter;

import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.UserActModel;
import com.tchcn.supermarket.model.UserManageModel;
import com.tchcn.supermarket.utils.GlideUtil;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 * Created by Administrator on 2018/10/30.
 */

public class UserManageAdapter extends BaseRecyclerAdapter<UserActModel.UserData.UserModel> {

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_user_manage;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, UserActModel.UserData.UserModel data) {
        LinearLayout ll_main = getView(holder,R.id.ll_main);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ll_main.getLayoutParams());
        ImageView iv_state = getView(holder,R.id.iv_state);
        SimpleDraweeView sdv = getView(holder,R.id.sdv);
        TextView tv_user_name = getView(holder,R.id.tv_user_name);
        TextView tv_user_job = getView(holder,R.id.tv_user_job);
        TextView tv_location = getView(holder,R.id.tv_location);
        if(position % 2 != 0 || position == 1){
            lp.setMargins(10,20,0,0);
        }else{
            lp.setMargins(0,20,0,0);
        }
        ll_main.setLayoutParams(lp);

        if(data.isChecked()){
            ll_main.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.drawable.sshape_user_check));
        }else{
            ll_main.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.color.white));
        }

        GlideUtil.load(context,data.getUser_image(),sdv);
        ViewBinder.setTextView(tv_user_name,data.getUser_name());
        ViewBinder.setTextView(tv_user_job,data.getApprole_name());
        ViewBinder.setTextView(tv_location,data.getLocation_name());
        if("1".equals(data.getDisabled())){
            iv_state.setImageResource(R.drawable.ic_user_stop_use);
        }else{
            iv_state.setImageResource(R.drawable.ic_user_normal);
        }

    }
}
