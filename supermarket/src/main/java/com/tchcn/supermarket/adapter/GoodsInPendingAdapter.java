package com.tchcn.supermarket.adapter;

import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.model.Good;
import com.tchcn.supermarket.utils.ViewBinder;

/**
 * Created by Administrator on 2018/5/5.
 */

public class GoodsInPendingAdapter extends BaseRecyclerAdapter<Good> {
    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_goods_in_pending;
    }

    @Override
    public void setUpData(CommonHolder holder, int position, int viewType, Good data) {
        TextView tv_num = getView(holder,R.id.tv_num);
        TextView tv_good_name = getView(holder,R.id.tv_good_name);
        TextView tv_good_num = getView(holder,R.id.tv_good_num);
        TextView tv_unit_price = getView(holder,R.id.tv_unit_price);
        TextView tv_discount_price = getView(holder,R.id.tv_discount_price);
        if((position+1) < 10){
            ViewBinder.setTextView(tv_num,"0"+(position+1));
        }else{
            ViewBinder.setTextView(tv_num,(position+1)+"");
        }

        ViewBinder.setTextView(tv_good_name,data.getName());
        ViewBinder.setTextView(tv_good_num,data.getNum()+"");
        ViewBinder.setTextView(tv_unit_price,data.getMoney()+"");
        if(data.getDiscount() > 0){
            ViewBinder.setTextView(tv_discount_price,data.getNum()*(data.getMoney()*data.getDiscount())+"");
        }else{
            ViewBinder.setTextView(tv_discount_price,"");
        }
    }
}
