package com.tchcn.supermarket.adapter;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.dbmodel.PendingOrderModel;

/**
 * Created by Administrator on 2018/5/5.
 */

public class PendingListAdapter extends BaseRecyclerAdapter<PendingOrderModel.PendingOrder> {

    private OnPendingOrderDelListener listener;

    public PendingListAdapter(OnPendingOrderDelListener delListener){
        listener = delListener;
    }

    @Override
    public int getItemLayoutId(int viewType) {
        return R.layout.sitem_pending_order;
    }

    @Override
    public void setUpData(CommonHolder holder, final int position, int viewType, final PendingOrderModel.PendingOrder data) {
        RelativeLayout rl_main = getView(holder,R.id.rl_main);
        View view_bottom = getView(holder,R.id.view_bottom);
        TextView tv_num = getView(holder,R.id.tv_num);
        TextView tv_pend_time = getView(holder,R.id.tv_pend_time);
        ImageView iv_del_pending_order = getView(holder,R.id.iv_del_pending_order);
        iv_del_pending_order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onPendDelete(position,data);
                getDatas().remove(data);
                notifyDataSetChanged();
            }
        });

        if(data.isIs_checked()){
            rl_main.setBackgroundDrawable(holder.itemView.getResources().getDrawable(R.drawable.sshape_pending_selected));
            view_bottom.setVisibility(View.GONE);
        }else{
            rl_main.setBackgroundColor(holder.itemView.getResources().getColor(R.color.white));
            view_bottom.setVisibility(View.VISIBLE);
        }
        String num = "";
        if(position+1 < 10){
            num = "00"+(position+1);
        }else if(position+1 >= 10 && position <100){
            num = "0"+(position+1);
        }else{
            num = position+"";
        }
        tv_num.setText(num);
        tv_pend_time.setText(data.getPend_time());
    }

    public interface OnPendingOrderDelListener{

        void onPendDelete(int position, PendingOrderModel.PendingOrder order);

    }

}
