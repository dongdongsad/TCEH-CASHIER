package com.tchcn.supermarket.dao;

import com.tchcn.supermarket.dbmodel.PendingOrderModel;

/** 挂单本地存储
 * Created by humu on 2018/5/4.
 */

public class PendingOrdersModelDao {

    public static boolean insertModel(PendingOrderModel model)
    {
        return JsonDbModelDaoX.getInstance().insertOrUpdate(model, true);

    }

    public static PendingOrderModel queryModel()
    {
        return JsonDbModelDaoX.getInstance().query(PendingOrderModel.class, true);
    }

    public static void deleteAllModel()
    {
        JsonDbModelDaoX.getInstance().delete(PendingOrderModel.class);
    }


}
