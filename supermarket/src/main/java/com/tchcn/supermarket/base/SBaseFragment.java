package com.tchcn.supermarket.base;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.tchcn.supermarket.ui.activity.CashierHomeActivity;

/**
 * Created by bigmu on 2018/4/29.
 */

public abstract class SBaseFragment extends Fragment implements View.OnClickListener{

    protected Context context;
    protected CashierHomeActivity activity;
    private View contentView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        baseInit();
    }

    public <V extends View> V find(int id)
    {
        View view = contentView.findViewById(id);
        return (V) view;
    }

    private void baseInit() {
        context = getContext();
        activity = (CashierHomeActivity) getActivity();
    }

    protected View onCreateContentView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        return null;
    }

    /**
     * 此方法用于被重写返回fragment布局id
     *
     * @return
     */
    protected abstract int onCreateContentView();

    protected abstract void initView();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        contentView = onCreateContentView(inflater, container, savedInstanceState);
        if (contentView == null)
        {
            int layoutId = onCreateContentView();
            if (layoutId != 0)
            {
                View layoutView = inflater.inflate(layoutId, container, false);
                contentView = layoutView;
            }
        }
        initView();
        return contentView;
    }

    protected int getContentViewResId()
    {
        return 0;
    }

    @Override
    public void onClick(View v) {

    }

}
