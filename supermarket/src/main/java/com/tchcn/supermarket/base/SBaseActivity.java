package com.tchcn.supermarket.base;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.tchcn.supermarket.utils.KeyboardUtil;
import com.zhy.autolayout.AutoLayoutActivity;

import org.xutils.x;

/**     Activity 基类
 * Created by humu on 2018/4/28.
 */

public abstract class SBaseActivity extends AutoLayoutActivity implements View.OnClickListener{

    protected Context context;
    protected Activity activity;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fresco.initialize(this);
        setContentView(getLayoutId());
        baseInit();
        initView();
    }

    //初始化操作
    protected abstract void initView();

    //公共初始化部分
    protected void baseInit() {
        context = this;
        activity = this;
    }

    public <V extends View> V find(int id)
    {
        View view = findViewById(id);
        return (V) view;
    }

    //返回布局文件
    protected abstract int getLayoutId();

    @Override
    public void onClick(View v) {

    }

}
