package com.tchcn.supermarket.common;

import android.text.TextUtils;

import com.tchcn.library.http.AppHttpUtil;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.http.AppRequestParams;
import com.tchcn.library.model.BaseActModel;
import com.tchcn.library.utils.LogUtil;
import com.tchcn.supermarket.model.GoodInfoActModel;
import com.tchcn.supermarket.model.GoodTypeActModel;
import com.tchcn.supermarket.model.LocationUserListActModel;
import com.tchcn.supermarket.model.MenuListActModel;
import com.tchcn.supermarket.model.NoCodeGoodActModel;
import com.tchcn.supermarket.model.PtActModel;
import com.tchcn.supermarket.model.RoleActModel;
import com.tchcn.supermarket.model.StockListActModel;
import com.tchcn.supermarket.model.StockNumActModel;
import com.tchcn.supermarket.model.StockOperateActModel;
import com.tchcn.supermarket.model.UserActModel;

import java.security.MessageDigest;

/**
 * Created by humu on 2018/11/1.
 */

public class SCommonInterface {

    /**
     * 商品分类列表
     * @param location_id 门店id
     * */
    public static void getAllTables(String location_id,AppRequestCallback<GoodTypeActModel> listener)
    {
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getTypeList");
        params.put("location_id",location_id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 商品详情（根据id查询）
     * @param id 商品id
     * */
    public static void getGoodInfoById(String id,AppRequestCallback<GoodInfoActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/goodsInfo");
        params.put("id",id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 商品详情（根据条码查询）
     * @param location_id 门店id
     * @param bar_code 商品条码
     * */
    public static void getGoodInfoByCode(String location_id,String bar_code,AppRequestCallback<GoodInfoActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getGoodsInfo");
        params.put("location_id",location_id);
        params.put("bar_code",bar_code);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 库存查询-库存上限/下限数字列表获取
     * @param location_id 门店id
     * */
    public static void getStockNums(String location_id, AppRequestCallback<StockNumActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getStockNumList");
        params.put("location_id",location_id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 无条码商品列表
     * @param location_id 门店id
     * @param deal_id 商品分类id
     * */
    public static void getNoCodeGoods(String location_id,String deal_id,AppRequestCallback<NoCodeGoodActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getNoCodeGoodsList");
        params.put("location_id",location_id);
        params.put("deal_id",deal_id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 库存列表
     * @param location_id 门店列表
     * @param deal_id 商品分类id
     * @param search_keyword 搜索关键字(条码或商品名); (条码精确匹配, 商品名模糊匹配)
     * @param stock_max 库存上限
     * @param stock_min 库存下限
     * @param listener
     */
    public static void getStockList(String location_id,String deal_id,String search_keyword,String stock_max,
                                    String stock_min,AppRequestCallback<StockListActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getStockList");
        params.put("location_id",location_id);
        if(!TextUtils.isEmpty(deal_id)){
            params.put("deal_id",deal_id);
        }
        if(!TextUtils.isEmpty(search_keyword)){
            params.put("search_keyword",search_keyword);
        }
        if(!TextUtils.isEmpty(stock_max)){
            params.put("stock_max",stock_max);
        }
        if(!TextUtils.isEmpty(stock_min)){
            params.put("stock_min",stock_min);
        }
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 库存预警列表
     * @param location_id 门店列表
     * @param deal_id 商品分类id
     * @param search_keyword 搜索关键字(条码或商品名); (条码精确匹配, 商品名模糊匹配)
     * @param stock_max 库存上限
     * @param stock_min 库存下限
     * @param listener
     */
    public static void getStockWarningList(String location_id,String deal_id,String search_keyword,String stock_max,
                                    String stock_min,AppRequestCallback<StockListActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getStockWarningList");
        params.put("location_id",location_id);
        if(!TextUtils.isEmpty(deal_id)){
            params.put("deal_id",deal_id);
        }
        if(!TextUtils.isEmpty(search_keyword)){
            params.put("search_keyword",search_keyword);
        }
        if(!TextUtils.isEmpty(stock_max)){
            params.put("stock_max",stock_max);
        }
        if(!TextUtils.isEmpty(stock_min)){
            params.put("stock_min",stock_min);
        }
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 门店相关账号列表
     * */
    public static void getLocationUserList(String location_id,AppRequestCallback<LocationUserListActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getLocationUserList");
        params.put("location_id",location_id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 库存查询-操作记录列表
     * @param location_id 门店id
     * @param is_store 1入库 2出库
     * @param deal_id 商品分类id
     * @param search_keyword 搜索关键字
     * @param account_id 操作人id
     * @param start_year
     * @param start_month
     * @param start_day
     * @param start_hour
     * @param end_year
     * @param end_month
     * @param end_day
     * @param end_hour
     * @param listener
     */
    public static void getStockOperateList(String location_id,String is_store,String deal_id,
                                           String search_keyword,String account_id,String start_year,
                                           String start_month,String start_day,String start_hour,
                                           String end_year,String end_month,String end_day,String end_hour,
                                           AppRequestCallback<StockOperateActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getStockInList");
        params.put("location_id",location_id);
        params.put("is_store",is_store);
        params.putCheckEmpty("deal_id",deal_id);
        if(!TextUtils.isEmpty(search_keyword)){
            params.putCheckEmpty("search_keyword",search_keyword);
        }
        params.putCheckEmpty("account_id",account_id);
        params.putCheckEmpty("start_year",start_year);
        params.putCheckEmpty("start_month",start_month);
        params.putCheckEmpty("start_day",start_day);
        params.putCheckEmpty("start_hour",start_hour);
        params.putCheckEmpty("end_year",end_year);
        params.putCheckEmpty("end_month",end_month);
        params.putCheckEmpty("end_day",end_day);
        params.putCheckEmpty("end_hour",end_hour);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 新增商品分类
     * @param user_id
     * @param location_id
     * @param name 分类名
     * @param listener
     */
    public static void addGoodType(String user_id,String location_id,String name,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/addCate");
        params.put("user_id",user_id);
        params.put("location_id",location_id);
        params.put("name",name);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 修改商品分类名
     * @param user_id 操作人id
     * @param id //分类id
     * @param name //分类新名称
     * @param listener
     */
    public static void updateGoodType(String user_id,String id,String name,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/updateCate");
        params.put("user_id",user_id);
        params.put("id",id);
        params.put("name",name);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 删除商品分类
     * @param user_id 操作人id
     * @param id 分类id
     * @param listener
     */
    public static void delGoodType(String user_id,String id,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/delCate");
        params.put("user_id",user_id);
        params.put("id",id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 修改商品资料
     * @param user_id
     * @param id 商品id
     * @param name
     * @param bar_code
     * @param deal_id 分类id
     * @param unit
     * @param buy_price 进货价
     * @param retail_price 零售价
     * @param user_price 会员价
     * @param stock_warning
     * @param listener
     */
    public static void updateGoodInfo(String user_id,String id,String name,String bar_code,
                                      String deal_id,String unit,String buy_price,String retail_price,
                                      String user_price,String stock_warning,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/updateGoods");
        params.put("user_id",user_id);
        params.put("id",id);
        params.put("name",name);
        params.put("bar_code",bar_code);
        params.put("deal_id",deal_id);
        params.put("unit",unit);
        params.put("buy_price",buy_price);
        params.put("retail_price",retail_price);
        params.put("user_price",user_price);
        params.put("stock_warning",stock_warning);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 后台管理-商品管理-新增商品
     * @param user_id 操作人id
     * @param name 商品名称
     * @param bar_code 条形码
     * @param deal_id 商品类型id
     * @param unit 单位
     * @param buy_price 进货价
     * @param retail_price 零售价
     * @param user_price 会员价
     * @param stock 库存
     * @param stock_warning 库存预警
     * @param listener
     */
    public static void addGood(String user_id,String name,String bar_code,String deal_id,String unit,
                               String buy_price,String retail_price,String user_price,String stock,
                               String stock_warning,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/addGoods");
        params.put("user_id",user_id);
        params.put("name",name);
        params.put("bar_code",bar_code);
        params.put("deal_id",deal_id);
        params.put("unit",unit);
        params.put("buy_price",buy_price);
        params.put("retail_price",retail_price);
        params.put("user_price",user_price);
        params.put("stock",stock);
        params.put("stock_warning",stock_warning);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 分页查询商户子账号列表
     * @param supplier_id 商户ID
     * @param listener
     */
    public static void getUsers(String supplier_id,AppRequestCallback<UserActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/queryUserList");
        params.put("supplier_id",supplier_id);
        params.put("page",1);
        params.put("pageSize",15);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 收银权限-权限分类
     * @param user_id
     * @param listener
     */
    public static void getCashPts(String user_id,AppRequestCallback<PtActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getRoleList");
        params.put("user_id",user_id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 收银权限-权限分类-新增
     * @param user_id 操作人id
     * @param name 权限名
     * @param listener
     */
    public static void addRole(String user_id,String name,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/addRole");
        params.put("user_id",user_id);
        params.put("name",name);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 收银权限-权限分类-修改
     * @param user_id 操作人id
     * @param id 权限id
     * @param name 权限名
     * @param listener
     */
    public static void updateRole(String user_id,String id,String name,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/updateRole");
        params.put("user_id",user_id);
        params.put("id",id);
        params.put("name",name);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 删除权限
     * @param user_id 操作人id
     * @param id 权限id
     * @param listener
     */
    public static void delRole(String user_id,String id,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/delRole");
        params.put("user_id",user_id);
        params.put("id",id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 获取角色
     */
    public static void getRoles(AppRequestCallback<RoleActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/sysAccount/queryRoleNames");
        AppHttpUtil.getInstance().get(params, listener);
    }

    /**
     * 用户管理-新增用户
     * @param user_id 当前用户id
     * @param pd 子账户密码
     * @param name 账户名
     * @param supplier_id 商户id
     * @param location_id 门店id
     * @param role_id 权限组id
     * @param role_name 身份
     * @param disable 是否有效 1停用 0正常
     * @param listener
     */
    public static void addNewAccount(String user_id,String pd,String name,String supplier_id,String location_id,
                               String role_id,String role_name,String disable,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/locationss/appInsert_role");
        params.put("user_id",user_id);
        params.put("pd",pd);
        params.put("name",name);
        params.put("supplier_id",supplier_id);
        params.put("location_id",location_id);
        params.put("role_id",role_id);
        params.put("role_name",role_name);
        params.put("disable",disable);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 修改子账户
     * @param id 子账户id
     * @param user_id 操作人id
     * @param location_id 门店id
     * @param name 账号名
     * @param pd 密码
     * @param role_id 权限组id
     * @param role_name 用户名
     * @param disable 是否有效 0有效1无效
     * @param listener
     */
    public static void updateAccount(String id,String user_id,String location_id,String name,String pd,
                                     String role_id,String role_name,String disable,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/locationss/appUpdate_user");
        params.put("id",id);
        params.put("user_id",user_id);
        params.put("location_id",location_id);
        params.put("name",name);
        params.put("pd",pd);
        params.put("role_id",role_id);
        params.put("role_name",role_name);
        params.put("disable",disable);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 便利店菜单权限列表
     * @param role_id 角色id
     * @param listener
     */
    public static void getMenuList(String role_id,AppRequestCallback<MenuListActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/getMenuList");
        params.put("role_id",role_id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 角色分配权限
     * @param role_id 角色id
     * @param checkMenuValues 逗号分隔的菜单id
     * @param listener
     */
    public static void setMenus(String role_id,String checkMenuValues,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/sysAccount/addRoleAuth");
        params.put("role_id",role_id);
        params.put("checkMenuValues",checkMenuValues);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 商品入库
     * @param num 入库数量
     * @param user_id 操作人id
     * @param id 商品id
     * @param listener
     */
    public static void storeGoods(float num,String user_id,String id,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/dealcate/StoreCate");
        params.put("num",1);
        params.put("user_id",user_id);
        params.put("id",id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 删除商品
     * @param id 商品id
     * @param listener
     */
    public static void delGood(String id,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/cvs/delGoods");
        params.put("id",id);
        AppHttpUtil.getInstance().post(params, listener);
    }

    /**
     * 删除子账户
     * @param id 用户id
     * @param listener
     */
    public static void delAccount(String id,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/locationss/update_role");
        params.put("id",id);
        AppHttpUtil.getInstance().get(params, listener);
    }

    /**
     * 下单
     * @param pay_types 支付方式集 （用逗号拼接）1.现金支付，2.微信，3.支付宝，4.会员卡余额，5、银行卡
     * @param pay_prices 对应支付方式的金额
     * @param total_price 总价格
     * @param goodsIds 商品id集（用逗号拼接）
     * @param goodNums 商品对应数量
     * @param user_id 操作人员id（可不填）
     * @param listener
     */
    public static void orderInsert(String pay_types,String pay_prices,String total_price,String goodsIds,
                                   String goodNums,String user_id,AppRequestCallback<BaseActModel> listener){
        AppRequestParams params = new AppRequestParams();
        params.setModuleAct("/dc_order/storeOrderInsert");
        params.put("pay_types",pay_types);
        params.put("pay_prices",pay_prices);
        params.put("total_price",total_price);
        params.put("goodsIds",goodsIds);
        params.put("goodNums",goodNums);
        params.put("user_id",user_id);
        AppHttpUtil.getInstance().post(params, listener);
    }

}
