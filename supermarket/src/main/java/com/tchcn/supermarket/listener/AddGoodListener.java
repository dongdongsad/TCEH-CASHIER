package com.tchcn.supermarket.listener;

import com.tchcn.supermarket.model.NoCodeGoodActModel;

/**
 * Created by Administrator on 2018/5/2.
 */

public interface AddGoodListener {

    void addGood(Float addNum,NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel model);

}
