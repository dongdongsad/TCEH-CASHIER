package com.tchcn.supermarket.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fanwe.library.adapter.http.model.SDResponse;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.adapter.GoodsInListAdapter;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.base.SBaseFragment;
import com.tchcn.supermarket.common.SCommonInterface;
import com.tchcn.supermarket.dialog.GoodNumInputDialog;
import com.tchcn.supermarket.listener.AddGoodListener;
import com.tchcn.supermarket.model.NoCodeGoodActModel;
import com.tchcn.supermarket.utils.KeyboardUtil;

import java.util.ArrayList;
import java.util.List;

/**
 *  收银台-无条码商品列表
 * Created by humu on 2018/4/29.
 */

public class GoodsListFragment extends SBaseFragment {

    private static GoodsListFragment fragment;
    private RecyclerView rv_goods_list;
    private AddGoodListener addGoodListener;
    private GoodsInListAdapter adapter;
    private String deal_id;

    public static GoodsListFragment getInstance() {
        if(fragment == null){
            fragment = new GoodsListFragment();
        }
        return fragment;
    }

    public void setAddGoodListener(AddGoodListener addGoodListener){
        this.addGoodListener = addGoodListener;
    }

    @Override
    protected int onCreateContentView() {
        return R.layout.sfragment_goods_list;
    }

    @Override
    protected void initView() {
        initUI();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if(activity != null){
            SCommonInterface.getNoCodeGoods(activity.getLocationId(), deal_id,
                    new AppRequestCallback<NoCodeGoodActModel>() {
                        @Override
                        protected void onSuccess(SDResponse sdResponse) {
                            if(actModel.isOk()){
                                NoCodeGoodActModel.NoCodeGoodData data = actModel.getData();
                                if(data != null){
                                    List<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel> modelList = data.getRes();
                                    if(modelList != null && modelList.size() > 0){
                                        adapter.setDatas(modelList);
                                    }else{
                                        adapter.setDatas(new ArrayList<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel>());
                                    }
                                }else{
                                    adapter.setDatas(new ArrayList<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel>());
                                }
                            }else{
                                adapter.setDatas(new ArrayList<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel>());
                            }
                        }
                    });
        }
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    private void initUI() {
        rv_goods_list = find(R.id.rv_goods_list);
        rv_goods_list.setLayoutManager(new GridLayoutManager(context,4));
        adapter = new GoodsInListAdapter();
        adapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel>() {
            @Override
            public void onItemClick(View covertView, int position, final NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel data) {
                GoodNumInputDialog.newInstance(GoodNumInputDialog.CHANGE_NUM,"",new GoodNumInputDialog.SetOnConfirmListener() {
                    @Override
                    public void onConfirm(String discount_result) {
                        addGoodListener.addGood(Float.parseFloat(discount_result),data);
                    }
                }).setOutCancel(true).show(getChildFragmentManager());
                KeyboardUtil.hideSoftInput(activity);
            }
        });
        rv_goods_list.setAdapter(adapter);
    }

    public void setDeal_id(String deal_id){
        this.deal_id = deal_id;
        if(activity != null){
            SCommonInterface.getNoCodeGoods(activity.getLocationId(), deal_id,
                    new AppRequestCallback<NoCodeGoodActModel>() {
                        @Override
                        protected void onSuccess(SDResponse sdResponse) {
                            if(actModel.isOk()){
                                NoCodeGoodActModel.NoCodeGoodData data = actModel.getData();
                                if(data != null){
                                    List<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel> modelList = data.getRes();
                                    if(modelList != null && modelList.size() > 0){
                                        adapter.setDatas(modelList);
                                    }else{
                                        adapter.setDatas(new ArrayList<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel>());
                                    }
                                }else{
                                    adapter.setDatas(new ArrayList<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel>());
                                }
                            }else{
                                adapter.setDatas(new ArrayList<NoCodeGoodActModel.NoCodeGoodData.NoCodeGoodModel>());
                            }
                        }
                    });
        }
    }

}
