package com.tchcn.supermarket.ui.fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.tchcn.supermarket.R;
import com.tchcn.supermarket.base.SBaseFragment;

/**
 *  后台管理-商品管理
 * Created by humu on 2018/5/26.
 */

public class BackstageGoodsManageFragment extends SBaseFragment {

    private Fragment nowFragment;
    private static BackstageGoodsManageFragment fragment;

    public static BackstageGoodsManageFragment getInstance(){
        if(fragment == null){
            fragment = new BackstageGoodsManageFragment();
        }
        return fragment;
    }

    @Override
    protected int onCreateContentView() {
        return R.layout.sfragment_backstage_goods_manage;
    }

    @Override
    protected void initView() {
        initFragment();
    }

    private void initFragment() {
        nowFragment = GoodsManageFragment.getInstance();
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.add(R.id.frame_good_manage,nowFragment).commit();
        ft.addToBackStack("");
    }

    //左侧菜单的切换
    public void changeFragment(Fragment fromFragment, Fragment toFragment) {

        if (nowFragment != null && nowFragment != toFragment) {
            nowFragment = toFragment;
        }
        FragmentManager fm = getChildFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);

        if (toFragment.isAdded() == false) {
            ft.hide(fromFragment).add(R.id.frame_good_manage, toFragment).commit();
        } else {
            ft.hide(fromFragment).show(toFragment).commit();
        }

    }

}
