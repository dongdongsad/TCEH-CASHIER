package com.tchcn.supermarket.ui.fragment;

import android.animation.ObjectAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fanwe.library.adapter.http.model.SDResponse;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.utils.LogUtil;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.adapter.CSGoodsAdapter;
import com.tchcn.supermarket.adapter.GoodTypesAdapter;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.base.SBaseFragment;
import com.tchcn.supermarket.common.SCommonInterface;
import com.tchcn.supermarket.model.GoodTypeActModel;
import com.tchcn.supermarket.model.StockListActModel;
import com.tchcn.supermarket.model.StockNumActModel;
import com.tchcn.supermarket.utils.AnimationUtil;
import com.tchcn.supermarket.view.WheelView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *  库存查询-商品入库
 * Created by humu on 2018/5/7.
 */

public class CommodityStorageFragment extends SBaseFragment {

    private static CommodityStorageFragment fragment;
    //商品列表
    private RecyclerView rv_goods;
    //商品适配
    private CSGoodsAdapter adapter;
    //商品库存
    private LinearLayout ll_sr;
    private TextView tv_sr;
    private View view_sr;
    //库存预警
    private LinearLayout ll_or;
    private TextView tv_or;
    private View view_or;
    //筛选分类
    private LinearLayout ll_chose_type;
    //筛选库存上线
    private LinearLayout ll_ir;
    //商品类型
    private RelativeLayout rl_filter_good_type;
    private RecyclerView rv_good_types;
    private View view_mask;
    private int menuHeight;

    private int menu_index_now = -1; //0分类，1库存上限，2库存下限
    //库存上限
    private LinearLayout ll_ir_switch;
    private TextView tv_ir_title;
    //库存下限
    private LinearLayout ll_ir_switch2;
    //筛选-当前商品分类
    private TextView tv_filter_type_now;
    private ImageView iv_type;
    //筛选-当前库存上限
    private TextView tv_biggest_inventory_now;
    private ImageView iv_biggest;
    //筛选-当前库存下限
    private TextView tv_smallest;
    private ImageView iv_smallest;
    //库存上限/下限数字列表
    private WheelView wv_ir;
    //筛选-库存上限/下限清空
    private TextView tv_clear_current_ir;
    //当前选中的商品类型，默认选中的是第一个
    private int currentGoodTypeIndex = 0;
    //当前选中的库存上限
    private int currentIrMaxIndex = 0;
    private int currentIrMax = 0;
    //当前选中的库存下限
    private int currentIrMinIndex = 0;
    private int currentIrMin = 0;
    private GoodTypesAdapter typeAdapter;
    private int modeNow = 0;
    private EditText et_good_code;

    public static CommodityStorageFragment getInstance(){
        if(fragment == null){
            fragment = new CommodityStorageFragment();
        }
        return fragment;
    }

    @Override
    protected int onCreateContentView() {
        return R.layout.sfragment_commodity_storage;
    }

    @Override
    protected void initView() {
        findViews();
        getStockNumList();
        getTypeList();
    }

    //筛选-商品类型列表
    private void getTypeList() {
        final List<GoodTypeActModel.GoodTypeData.GoodType> goodTypeList = new ArrayList<>();
        GoodTypeActModel.GoodTypeData.GoodType allType = new GoodTypeActModel.GoodTypeData.GoodType();
        allType.setChecked(true);
        allType.setId("");
        allType.setName("全部分类");
        goodTypeList.add(0,allType);
        SCommonInterface.getAllTables(activity.getLocationId(), new AppRequestCallback<GoodTypeActModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                if(actModel.isOk()){
                    GoodTypeActModel.GoodTypeData data = actModel.getData();
                    if(data != null){
                        List<GoodTypeActModel.GoodTypeData.GoodType> types = data.getRes();
                        if(types != null && types.size() > 0){
                            goodTypeList.addAll(types);
                        }
                    }
                }
            }

            @Override
            protected void onFinish(SDResponse resp) {
                super.onFinish(resp);
                typeAdapter.setDatas(goodTypeList);
                getGoods();
            }
        });
    }

    private void getStockList(){
        String stock_max = tv_biggest_inventory_now.getText().toString();
        if("库存上限".equals(stock_max)){
            stock_max = "";
        }
        String stock_min = tv_smallest.getText().toString();
        if("库存下限".equals(stock_min)){
            stock_min = "";
        }
        String keyword = et_good_code.getText().toString();
        if(modeNow == 0){
            SCommonInterface.getStockList(activity.getLocationId(), typeAdapter.getItem(currentGoodTypeIndex).getId(),
                    keyword, stock_max, stock_min, new AppRequestCallback<StockListActModel>() {
                        @Override
                        protected void onSuccess(SDResponse sdResponse) {
                            LogUtil.e("getStockList",sdResponse.getResult());
                            if(actModel.isOk()){
                                StockListActModel.StockListData data = actModel.getData();
                                if(data != null){
                                    List<StockListActModel.StockListData.StockModel> stockModelList = data.getRes();
                                    if(stockModelList != null && stockModelList.size() > 0){
                                        adapter.setDatas(stockModelList);
                                        adapter.notifyModeChanged(modeNow);
                                    }else{
                                        adapter.setDatas(new ArrayList<StockListActModel.StockListData.StockModel>());
                                    }
                                }else{
                                    adapter.setDatas(new ArrayList<StockListActModel.StockListData.StockModel>());
                                }
                            }else{
                                adapter.setDatas(new ArrayList<StockListActModel.StockListData.StockModel>());
                            }
                        }
                    });
        }else if(modeNow == 1){
            SCommonInterface.getStockWarningList(activity.getLocationId(), typeAdapter.getItem(currentGoodTypeIndex).getId(),
                    keyword, stock_max, stock_min, new AppRequestCallback<StockListActModel>() {
                        @Override
                        protected void onSuccess(SDResponse sdResponse) {
                            if(actModel.isOk()){
                                StockListActModel.StockListData data = actModel.getData();
                                if(data != null){
                                    List<StockListActModel.StockListData.StockModel> stockModelList = data.getRes();
                                    if(stockModelList != null && stockModelList.size() > 0){
                                        adapter.setDatas(stockModelList);
                                        adapter.notifyModeChanged(modeNow);
                                    }else{
                                        adapter.setDatas(new ArrayList<StockListActModel.StockListData.StockModel>());
                                    }
                                }else{
                                    adapter.setDatas(new ArrayList<StockListActModel.StockListData.StockModel>());
                                }
                            }else{
                                adapter.setDatas(new ArrayList<StockListActModel.StockListData.StockModel>());
                            }
                        }
                    });
        }

    }


    //商品列表
    private void getGoods() {
        getStockList();
    }

    //获取库存上限/下限数量
    private void getStockNumList() {
        SCommonInterface.getStockNums(activity.getLocationId(), new AppRequestCallback<StockNumActModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                if(actModel.isOk()){
                    StockNumActModel.StockNumData data = actModel.getData();
                    if(data != null){
                        String[] res = data.getRes();
                        if(res != null && res.length > 0){
                            wv_ir.setItems(Arrays.asList(res),0);
                        }
                    }
                }
            }
        });
    }

    private void findViews() {
        ll_sr = find(R.id.ll_sr);
        ll_sr.setOnClickListener(this);
        tv_sr = find(R.id.tv_sr);
        view_sr = find(R.id.view_sr);
        ll_or = find(R.id.ll_or);
        ll_or.setOnClickListener(this);
        tv_or = find(R.id.tv_or);
        view_or = find(R.id.view_or);
        rv_goods = find(R.id.rv_goods);
        ll_chose_type = find(R.id.ll_chose_type);
        ll_chose_type.setOnClickListener(this);
        rl_filter_good_type = find(R.id.rl_filter_good_type);
        rv_good_types = find(R.id.rv_good_types);
        rv_good_types.setLayoutManager(new LinearLayoutManager(context));
        view_mask = find(R.id.view_mask);
        view_mask.setOnClickListener(this);
        ll_ir = find(R.id.ll_ir);
        ll_ir_switch = find(R.id.ll_ir_switch);
        ll_ir_switch.setOnClickListener(this);
        wv_ir = find(R.id.wv_ir);
        tv_clear_current_ir = find(R.id.tv_clear_current_ir);
        TextView tv_confirm_ir = find(R.id.tv_confirm_ir);
        tv_confirm_ir.setOnClickListener(this);
        tv_clear_current_ir.setOnClickListener(this);
        tv_ir_title = find(R.id.tv_ir_title);
        ll_ir_switch2 = find(R.id.ll_ir_switch2);
        ll_ir_switch2.setOnClickListener(this);
        TextView tv_cancel_filter_inventory = find(R.id.tv_cancel_filter_inventory);
        tv_cancel_filter_inventory.setOnClickListener(this);
        tv_filter_type_now = find(R.id.tv_filter_type_now);
        iv_type = find(R.id.iv_type);
        tv_biggest_inventory_now = find(R.id.tv_biggest_inventory_now);
        iv_biggest = find(R.id.iv_biggest);
        tv_smallest = find(R.id.tv_smallest);
        iv_smallest = find(R.id.iv_smallest);
        et_good_code = find(R.id.et_good_code);
        et_good_code.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getGoods();
            }
        });

        typeAdapter = new GoodTypesAdapter();
        typeAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<GoodTypeActModel.GoodTypeData.GoodType>() {
            @Override
            public void onItemClick(View covertView, int position, GoodTypeActModel.GoodTypeData.GoodType data) {
                if(currentGoodTypeIndex >= 0){
                    typeAdapter.getItem(currentGoodTypeIndex).setChecked(false);
                }
                tv_filter_type_now.setText(data.getName());
                data.setChecked(true);
                typeAdapter.notifyDataSetChanged();
                currentGoodTypeIndex = position;
                hideMenu();
                getGoods();
            }
        });
        rv_good_types.setAdapter(typeAdapter);

        rv_goods.setLayoutManager(new GridLayoutManager(context,3));
        adapter = new CSGoodsAdapter(this);
        rv_goods.setAdapter(adapter);

    }

    private void changeMode(int m){
        switch (m){
            case 0: //商品库存
                hideMenu();
                tv_sr.setTextColor(getResources().getColor(R.color.main_blue));
                view_sr.setVisibility(View.VISIBLE);
                tv_or.setTextColor(getResources().getColor(R.color.six_three));
                view_or.setVisibility(View.INVISIBLE);
                modeNow = 0;
                break;
            case 1: //库存预警
                hideMenu();
                tv_sr.setTextColor(getResources().getColor(R.color.six_three));
                view_sr.setVisibility(View.INVISIBLE);
                tv_or.setTextColor(getResources().getColor(R.color.main_blue));
                view_or.setVisibility(View.VISIBLE);
                modeNow = 1;
                break;
        }
        getGoods();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        if(id == R.id.ll_sr){ //商品库存
            changeMode(0);
        }else if(id == R.id.ll_or){ //库存预警
            changeMode(1);
        }else if(id == R.id.ll_chose_type){ //筛选分类
            showMenu(0,"");
        }else if(id == R.id.ll_ir_switch){ //库存上限
            showMenu(1,"最高库存");
        }else if(id == R.id.ll_ir_switch2){
            showMenu(1,"最低库存");
        }else if(id == R.id.view_mask || id == R.id.tv_cancel_filter_inventory){
            hideMenu();
        }else if(id == R.id.tv_clear_current_ir){ //筛选-库存上限/下限-清空
            if("最高库存".equals(tv_ir_title.getText().toString())){
                tv_biggest_inventory_now.setText("库存上限");
                currentIrMaxIndex = 0;
                getGoods();
            }else if("最低库存".equals(tv_ir_title.getText().toString())){
                tv_smallest.setText("库存下限");
                currentIrMinIndex = 0;
                getGoods();
            }
            hideMenu();
        }else if(id == R.id.tv_confirm_ir){ //筛选-库存上限/下限-确认
            if(wv_ir.getItems() != null && wv_ir.getItems().size() > 0){
                if("最高库存".equals(tv_ir_title.getText().toString())){
                    tv_biggest_inventory_now.setText(wv_ir.getSelectedItem());
                    currentIrMaxIndex = wv_ir.getSelectedPosition();
                    getGoods();
                }else if("最低库存".equals(tv_ir_title.getText().toString())){
                    tv_smallest.setText(wv_ir.getSelectedItem());
                    currentIrMinIndex = wv_ir.getSelectedPosition();
                    getGoods();
                }
            }
            hideMenu();
        }
    }

    private void hideMenu() {
        switch (menu_index_now){
            case 0:
                menu_index_now = -1;
                view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                view_mask.setVisibility(View.GONE);
                ObjectAnimator.ofFloat(rv_good_types, "translationY", 0, -menuHeight).setDuration(200).start();
                tv_filter_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                iv_type.setImageResource(R.drawable.sic_drop_down_selected);
                break;
            case 1:
                menu_index_now = -1;
                view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                view_mask.setVisibility(View.GONE);
                ObjectAnimator.ofFloat(ll_ir, "translationY", 0, -menuHeight).setDuration(200).start();
                if("最高库存".equals(tv_ir_title.getText().toString())){
                    tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_biggest.setImageResource(R.drawable.sic_drop_down_selected);
                }else{
                    tv_smallest.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_smallest.setImageResource(R.drawable.sic_drop_down_selected);
                }
                break;
        }
    }

    private void showMenu(int type,String filter_type){
        switch (type){
            case 0: //全部分类
                if(menu_index_now < 0){
                    view_mask.startAnimation(AnimationUtil.getMaskViewAppearAnim());
                    view_mask.setVisibility(View.VISIBLE);
                    rl_filter_good_type.setVisibility(View.VISIBLE);
                    rv_good_types.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            rv_good_types.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            menuHeight = rv_good_types.getHeight();
                            ObjectAnimator.ofFloat(rv_good_types, "translationY", -menuHeight, 0).setDuration(200).start();
                        }
                    });
                    menu_index_now = 0;
                    tv_filter_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_type.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_biggest.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_smallest.setTextColor(getResources().getColor(R.color.six_six));
                    iv_smallest.setImageResource(R.drawable.sic_drop_down_normal);
                }else if(menu_index_now == 0){
                    menu_index_now = -1;
                    view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                    view_mask.setVisibility(View.GONE);
                    ObjectAnimator.ofFloat(rv_good_types, "translationY", 0, -menuHeight).setDuration(200).start();
                    tv_filter_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_type.setImageResource(R.drawable.sic_drop_down_selected);
                }else{
                    if(menu_index_now == 1){
                        ll_ir.setVisibility(View.GONE);
                        rl_filter_good_type.setVisibility(View.VISIBLE);
                        rv_good_types.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                rv_good_types.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = rv_good_types.getHeight();
                                ObjectAnimator.ofFloat(rv_good_types, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }
                    menu_index_now = 0;
                    tv_filter_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_type.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_biggest.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_smallest.setTextColor(getResources().getColor(R.color.six_six));
                    iv_smallest.setImageResource(R.drawable.sic_drop_down_normal);
                }
                break;
            case 1:
                if(menu_index_now < 0){
                    tv_ir_title.setText(filter_type);
                    view_mask.startAnimation(AnimationUtil.getMaskViewAppearAnim());
                    view_mask.setVisibility(View.VISIBLE);
                    ll_ir.setVisibility(View.VISIBLE);
                    ll_ir.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            ll_ir.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            menuHeight = ll_ir.getHeight();
                            ObjectAnimator.ofFloat(ll_ir, "translationY", -menuHeight, 0).setDuration(200).start();
                        }
                    });
                    menu_index_now = 1;
                    if("最高库存".equals(tv_ir_title.getText().toString())){
                        tv_filter_type_now.setTextColor(getResources().getColor(R.color.six_six));
                        iv_type.setImageResource(R.drawable.sic_drop_down_normal);
                        tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.main_blue));
                        iv_biggest.setImageResource(R.drawable.sic_drop_up_selected);
                        tv_smallest.setTextColor(getResources().getColor(R.color.six_six));
                        iv_smallest.setImageResource(R.drawable.sic_drop_down_normal);
                    }else{
                        tv_filter_type_now.setTextColor(getResources().getColor(R.color.six_six));
                        iv_type.setImageResource(R.drawable.sic_drop_down_normal);
                        tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.six_six));
                        iv_biggest.setImageResource(R.drawable.sic_drop_down_normal);
                        tv_smallest.setTextColor(getResources().getColor(R.color.main_blue));
                        iv_smallest.setImageResource(R.drawable.sic_drop_up_selected);
                    }
                }else if(menu_index_now == 1){
                    if(filter_type.equals(tv_ir_title.getText().toString())){
                        menu_index_now = -1;
                        view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                        view_mask.setVisibility(View.GONE);
                        ObjectAnimator.ofFloat(ll_ir, "translationY", 0, -menuHeight).setDuration(200).start();
                    }else{
                        tv_ir_title.setText(filter_type);
                    }
                    if("最高库存".equals(tv_ir_title.getText().toString())){
                        tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.main_blue));
                        if(view_mask.getVisibility() == View.VISIBLE){
                            iv_biggest.setImageResource(R.drawable.sic_drop_up_selected);
                        }else{
                            iv_biggest.setImageResource(R.drawable.sic_drop_down_selected);
                        }
                        tv_smallest.setTextColor(getResources().getColor(R.color.six_six));
                        iv_smallest.setImageResource(R.drawable.sic_drop_down_normal);
                    }else{
                        tv_smallest.setTextColor(getResources().getColor(R.color.main_blue));
                        if(view_mask.getVisibility() == View.VISIBLE){
                            iv_smallest.setImageResource(R.drawable.sic_drop_up_selected);
                        }else{
                            iv_smallest.setImageResource(R.drawable.sic_drop_down_selected);
                        }
                        tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.six_six));
                        iv_biggest.setImageResource(R.drawable.sic_drop_down_normal);
                    }
                }else{
                    tv_ir_title.setText(filter_type);
                    if(menu_index_now == 0){
                        ll_ir.setVisibility(View.VISIBLE);
                        rl_filter_good_type.setVisibility(View.GONE);
                        ll_ir.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ll_ir.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = ll_ir.getHeight();
                                ObjectAnimator.ofFloat(ll_ir, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }
                    menu_index_now = 1;
                    if("最高库存".equals(tv_ir_title.getText().toString())){
                        tv_filter_type_now.setTextColor(getResources().getColor(R.color.six_six));
                        iv_type.setImageResource(R.drawable.sic_drop_down_normal);
                        tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.main_blue));
                        iv_biggest.setImageResource(R.drawable.sic_drop_up_selected);
                        tv_smallest.setTextColor(getResources().getColor(R.color.six_six));
                        iv_smallest.setImageResource(R.drawable.sic_drop_down_normal);
                    }else{
                        tv_filter_type_now.setTextColor(getResources().getColor(R.color.six_six));
                        iv_type.setImageResource(R.drawable.sic_drop_down_normal);
                        tv_biggest_inventory_now.setTextColor(getResources().getColor(R.color.six_six));
                        iv_biggest.setImageResource(R.drawable.sic_drop_down_normal);
                        tv_smallest.setTextColor(getResources().getColor(R.color.main_blue));
                        iv_smallest.setImageResource(R.drawable.sic_drop_up_selected);
                    }
                }
                break;
        }
    }

}
