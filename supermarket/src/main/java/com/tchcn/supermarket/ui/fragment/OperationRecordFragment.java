package com.tchcn.supermarket.ui.fragment;

import android.animation.ObjectAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.fanwe.library.adapter.http.model.SDResponse;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.model.UserInfoModel;
import com.tchcn.library.utils.LogUtil;
import com.tchcn.supermarket.R;
import com.tchcn.supermarket.adapter.GoodTypesAdapter;
import com.tchcn.supermarket.adapter.GoodsInManageAdapter;
import com.tchcn.supermarket.adapter.MembersFilterAdapter;
import com.tchcn.supermarket.base.BaseRecyclerAdapter;
import com.tchcn.supermarket.base.SBaseFragment;
import com.tchcn.supermarket.common.SCommonInterface;
import com.tchcn.supermarket.model.GoodManageModel;
import com.tchcn.supermarket.model.GoodTypeActModel;
import com.tchcn.supermarket.model.LocationUserListActModel;
import com.tchcn.supermarket.model.StockOperateActModel;
import com.tchcn.supermarket.utils.AnimationUtil;
import com.tchcn.supermarket.utils.ViewBinder;
import com.tchcn.supermarket.view.WheelView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *  库存查询-操作记录
 * Created by humu on 2018/5/7.
 */

public class OperationRecordFragment extends SBaseFragment {

    private static OperationRecordFragment fragment;
    //商品列表
    private RecyclerView rv_goods;
    private GoodsInManageAdapter adapter;
    //入库记录
    private LinearLayout ll_sr;
    private TextView tv_sr;
    private View view_sr;
    //出库记录
    private LinearLayout ll_or;
    private TextView tv_or;
    private View view_or;

    private View view_mask;
    //商品类型下拉框
    private RelativeLayout rl_filter_good_type;
    //操作人下拉框
    private RelativeLayout rl_filter_member;
    private RecyclerView rv_members;
    private TextView tv_operator;
    private ImageView iv_operator;
    //商品类型列表
    private RecyclerView rv_good_types;
    private TextView tv_good_type_now;
    private ImageView iv_good_type;
    //年月日时筛选
    private LinearLayout ll_filter_time;
    private TextView tv_start_time;
    private ImageView iv_start_time;
    private TextView tv_end_time;
    private ImageView iv_end_time;
    private WheelView wv_year;
    private WheelView wv_month;
    private WheelView wv_day;
    private WheelView wv_hour;

    private int menu_index_now = -1; //0全部分类，1全部操作人,2开始时间,3结束时间
    private int currentGoodTypeIndex = 0; //商品类型索引,默认0
    private int menuHeight;
    private MembersFilterAdapter membersFilterAdapter;
    private int currentUserIndex = 0; //当前操作人索引，默认0
    private List<LocationUserListActModel.LocationUserListData.LocationUser> locationUserList;
    private GoodTypesAdapter typeAdapter;
    private List<GoodTypeActModel.GoodTypeData.GoodType> goodTypeList;

    private String startYear;
    private String startMonth;
    private String startDay;
    private String startHour;
    private String endYear;
    private String endMonth;
    private String endDay;
    private String endHour;

    private String is_store = "1"; //1入库记录 2出库记录
    private String deal_id = ""; //分类id
    private String account_id = ""; //操作人id
    private EditText et_keyword;

    public static OperationRecordFragment getInstance(){
        if(fragment == null){
            fragment = new OperationRecordFragment();
        }
        return fragment;
    }

    @Override
    protected int onCreateContentView() {
        return R.layout.sfragment_operation_record;
    }

    @Override
    protected void initView() {

        findViews();
        getGoodData();
        getGoodTypeData();
        getOperatorData();
        getTimeFilterData();
    }

    //筛选-操作人列表
    private void getOperatorData() {
        locationUserList = new ArrayList<>();
        LocationUserListActModel.LocationUserListData.LocationUser allUser = new LocationUserListActModel.LocationUserListData.LocationUser();
        allUser.setId("");
        allUser.setUser_name("全部操作人");
        allUser.setChecked(true);
        locationUserList.add(0,allUser);
        SCommonInterface.getLocationUserList(activity.getLocationId(), new AppRequestCallback<LocationUserListActModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                if(actModel.isOk()){
                    LocationUserListActModel.LocationUserListData data = actModel.getData();
                    if(data != null){
                        List<LocationUserListActModel.LocationUserListData.LocationUser> userList = data.getRes();
                        if(userList != null && userList.size() > 0){
                            locationUserList.addAll(userList);
                        }
                    }
                }
            }

            @Override
            protected void onFinish(SDResponse resp) {
                super.onFinish(resp);
                membersFilterAdapter.setDatas(locationUserList);
            }

        });

    }

    //筛选-商品类型数据获取
    private void getGoodTypeData() {
        goodTypeList = new ArrayList<>();
        GoodTypeActModel.GoodTypeData.GoodType allType = new GoodTypeActModel.GoodTypeData.GoodType();
        allType.setChecked(true);
        allType.setId("");
        allType.setName("全部分类");
        goodTypeList.add(0,allType);
        SCommonInterface.getAllTables(activity.getLocationId(), new AppRequestCallback<GoodTypeActModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                if(actModel.isOk()){
                    GoodTypeActModel.GoodTypeData data = actModel.getData();
                    if(data != null){
                        List<GoodTypeActModel.GoodTypeData.GoodType> types = data.getRes();
                        if(types != null && types.size() > 0){
                            goodTypeList.addAll(types);
                        }
                    }
                }
            }

            @Override
            protected void onFinish(SDResponse resp) {
                super.onFinish(resp);
                typeAdapter.setDatas(goodTypeList);
            }

        });

    }

    //获取商品列表
    private void getGoodData() {
        String keyword = et_keyword.getText().toString();
        SCommonInterface.getStockOperateList(activity.getLocationId(), is_store, deal_id, keyword,
                account_id, startYear, startMonth, startDay, startHour, endYear, endMonth,
                endDay, endHour, new AppRequestCallback<StockOperateActModel>() {
                    @Override
                    protected void onSuccess(SDResponse sdResponse) {
                        if(actModel.isOk()){
                            StockOperateActModel.StockOperateData data = actModel.getData();
                            if(data != null){
                                List<StockOperateActModel.StockOperateData.StockOperateModel> modelList = data.getRes();
                                if(modelList != null && modelList.size() > 0){
                                    adapter.setDatas(modelList);
                                }else{
                                    adapter.setDatas(new ArrayList<StockOperateActModel.StockOperateData.StockOperateModel>());
                                }
                            }else{
                                adapter.setDatas(new ArrayList<StockOperateActModel.StockOperateData.StockOperateModel>());
                            }
                        }else{
                            adapter.setDatas(new ArrayList<StockOperateActModel.StockOperateData.StockOperateModel>());
                        }
                    }

                    @Override
                    protected void onFinish(SDResponse resp) {
                        super.onFinish(resp);
                        adapter.notifyModeChanged(is_store);
                    }
                });
    }

    //筛选-时间数据获取
    private void getTimeFilterData() {
        List<String> testList = new ArrayList<>();
        for(int i = 10;i<20;i++){
            testList.add(i+"");
        }
        wv_year.setItems(getAllYears(),getThisYearPosition());
        wv_year.setOnItemSelectedListener(new WheelView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int selectedIndex, String item) {
                wv_day.setItems(getSpecifiedDays(Integer.parseInt(item),Integer.parseInt(wv_month.getSelectedItem())),0);
            }
        });
        wv_month.setItems(getMonths(),getThisMonthPosition());
        wv_month.setOnItemSelectedListener(new WheelView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int selectedIndex, String item) {
                wv_day.setItems(getSpecifiedDays(Integer.parseInt(wv_year.getSelectedItem()),Integer.parseInt(item)),0);
            }
        });
        wv_day.setItems(getSpecifiedDays(Integer.parseInt(wv_year.getSelectedItem()),Integer.parseInt(wv_month.getSelectedItem())),getDayOfMonthIndex());
        wv_hour.setItems(getHours(),getHourNowIndex());

    }

    private void findViews() {
        ll_sr = find(R.id.ll_sr);
        ll_sr.setOnClickListener(this);
        tv_sr = find(R.id.tv_sr);
        view_sr = find(R.id.view_sr);
        ll_or = find(R.id.ll_or);
        ll_or.setOnClickListener(this);
        tv_or = find(R.id.tv_or);
        view_or = find(R.id.view_or);
        rv_goods = find(R.id.rv_goods);
        rv_goods.setLayoutManager(new GridLayoutManager(context,3));
        view_mask = find(R.id.view_mask);
        view_mask.setOnClickListener(this);
        rl_filter_good_type = find(R.id.rl_filter_good_type);
        tv_good_type_now = find(R.id.tv_good_type_now);
        iv_good_type = find(R.id.iv_good_type);
        rv_good_types = find(R.id.rv_good_types);
        rv_good_types.setLayoutManager(new LinearLayoutManager(context));
        LinearLayout ll_filter_type = find(R.id.ll_filter_type);
        ll_filter_type.setOnClickListener(this);
        rl_filter_member = find(R.id.rl_filter_member);
        rv_members = find(R.id.rv_members);
        tv_operator = find(R.id.tv_operator);
        iv_operator = find(R.id.iv_operator);
        rv_members.setLayoutManager(new LinearLayoutManager(context));
        tv_start_time = find(R.id.tv_start_time);
        iv_start_time = find(R.id.iv_start_time);
        LinearLayout ll_filter_member = find(R.id.ll_filter_member);
        ll_filter_member.setOnClickListener(this);
        LinearLayout ll_start_time = find(R.id.ll_start_time);
        ll_start_time.setOnClickListener(this);
        LinearLayout ll_end_time = find(R.id.ll_end_time);
        ll_end_time.setOnClickListener(this);
        tv_end_time = find(R.id.tv_end_time);
        iv_end_time = find(R.id.iv_end_time);
        wv_year = find(R.id.wv_year);
        wv_year.setIsLoop(false);
        wv_month = find(R.id.wv_month);
        wv_month.setIsLoop(false);
        wv_day = find(R.id.wv_day);
        wv_day.setIsLoop(false);
        wv_hour = find(R.id.wv_hour);
        wv_hour.setIsLoop(false);
        ll_filter_time = find(R.id.ll_filter_time);
        ll_filter_time.setOnClickListener(this);
        TextView tv_clear_time = find(R.id.tv_clear_time);
        tv_clear_time.setOnClickListener(this);
        TextView tv_cancel_filter_time = find(R.id.tv_cancel_filter_time);
        tv_cancel_filter_time.setOnClickListener(this);
        TextView tv_confirm_time = find(R.id.tv_confirm_time);
        tv_confirm_time.setOnClickListener(this);
        et_keyword = find(R.id.et_keyword);
        et_keyword.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                getGoodData();
            }
        });

        membersFilterAdapter = new MembersFilterAdapter();
        membersFilterAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<LocationUserListActModel.LocationUserListData.LocationUser>() {
            @Override
            public void onItemClick(View covertView, int position, LocationUserListActModel.LocationUserListData.LocationUser data) {
                if(currentUserIndex >= 0){
                    membersFilterAdapter.getItem(currentUserIndex).setChecked(false);
                }
                data.setChecked(true);
                account_id = data.getId();
                membersFilterAdapter.notifyDataSetChanged();
                currentUserIndex = position;
                tv_operator.setText(data.getUser_name());
                hideMenu();
                getGoodData();
            }
        });
        rv_members.setAdapter(membersFilterAdapter);

        typeAdapter = new GoodTypesAdapter();
        typeAdapter.setOnItemClickListener(new BaseRecyclerAdapter.OnItemClickListener<GoodTypeActModel.GoodTypeData.GoodType>() {
            @Override
            public void onItemClick(View covertView, int position, GoodTypeActModel.GoodTypeData.GoodType data) {
                if(currentGoodTypeIndex >= 0){
                    typeAdapter.getItem(currentGoodTypeIndex).setChecked(false);
                }
                data.setChecked(true);
                deal_id = data.getId();
                typeAdapter.notifyDataSetChanged();
                currentGoodTypeIndex = position;
                tv_good_type_now.setText(data.getName());
                hideMenu();
                getGoodData();
            }
        });

        rv_good_types.setAdapter(typeAdapter);

        adapter = new GoodsInManageAdapter();
        rv_goods.setAdapter(adapter);

    }

    private void changeMode(int m){
        switch (m){
            case 0: //入库记录
                tv_sr.setTextColor(getResources().getColor(R.color.main_blue));
                view_sr.setVisibility(View.VISIBLE);
                tv_or.setTextColor(getResources().getColor(R.color.six_three));
                view_or.setVisibility(View.INVISIBLE);
                break;
            case 1: //出库记录
                tv_sr.setTextColor(getResources().getColor(R.color.six_three));
                view_sr.setVisibility(View.INVISIBLE);
                tv_or.setTextColor(getResources().getColor(R.color.main_blue));
                view_or.setVisibility(View.VISIBLE);
                break;
        }
        getGoodData();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int id = v.getId();
        if(id == R.id.ll_sr){ //入库记录
            is_store = "1";
            changeMode(0);
        }else if(id == R.id.ll_or){ //出库记录
            is_store = "2";
            changeMode(1);
        }else if(id == R.id.ll_filter_type){ //筛选商品分类
            showMenu(0);
        }else if(id == R.id.ll_filter_member){ //筛选操作人
            showMenu(1);
        }else if(id == R.id.ll_start_time){ //开始时间
            showMenu(2);
        }else if(id == R.id.ll_end_time){ //结束时间
            showMenu(3);
        }else if(id == R.id.view_mask){
            hideMenu();
        }else if(id == R.id.tv_clear_time){
            if(menu_index_now == 2){
                startYear = "";
                startMonth = "";
                startDay = "";
                startHour = "";
                ViewBinder.setTextView(tv_start_time,"开始时间");
                getGoodData();
            }else if(menu_index_now == 3){
                endYear = "";
                endMonth = "";
                endDay = "";
                endHour = "";
                ViewBinder.setTextView(tv_end_time,"结束时间");
                getGoodData();
            }
            hideMenu();
        }else if(id == R.id.tv_cancel_filter_time){
            hideMenu();
        }else if(id == R.id.tv_confirm_time){
            if(menu_index_now == 2){
                startYear = wv_year.getSelectedItem();
                startMonth = wv_month.getSelectedItem();
                startDay = wv_day.getSelectedItem();
                startHour = wv_hour.getSelectedItem();
                ViewBinder.setTextView(tv_start_time,startYear+"-"+startMonth+"-"+startDay+" "+startHour+":00");
            }else if(menu_index_now == 3){
                endYear = wv_year.getSelectedItem();
                endMonth = wv_month.getSelectedItem();
                endDay = wv_day.getSelectedItem();
                endHour = wv_hour.getSelectedItem();
                ViewBinder.setTextView(tv_end_time,endYear+"-"+endMonth+"-"+endDay+" "+endHour+":00");
            }
            hideMenu();
            getGoodData();
        }
    }

    private void hideMenu() {
        switch (menu_index_now){
            case 0:
                menu_index_now = -1;
                view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                view_mask.setVisibility(View.GONE);
                ObjectAnimator.ofFloat(rv_good_types, "translationY", 0, -menuHeight).setDuration(200).start();
                tv_good_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                iv_good_type.setImageResource(R.drawable.sic_drop_down_selected);
                break;
            case 1:
                menu_index_now = -1;
                view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                view_mask.setVisibility(View.GONE);
                ObjectAnimator.ofFloat(rl_filter_member, "translationY", 0, -menuHeight).setDuration(200).start();
                tv_operator.setTextColor(getResources().getColor(R.color.main_blue));
                iv_operator.setImageResource(R.drawable.sic_drop_down_selected);
                break;
            case 2:
                menu_index_now = -1;
                view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                view_mask.setVisibility(View.GONE);
                ObjectAnimator.ofFloat(ll_filter_time, "translationY", 0, -menuHeight).setDuration(200).start();
                tv_start_time.setTextColor(getResources().getColor(R.color.main_blue));
                iv_start_time.setImageResource(R.drawable.sic_drop_down_selected);
                break;
            case 3:
                menu_index_now = -1;
                view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                view_mask.setVisibility(View.GONE);
                ObjectAnimator.ofFloat(ll_filter_time, "translationY", 0, -menuHeight).setDuration(200).start();
                tv_end_time.setTextColor(getResources().getColor(R.color.main_blue));
                iv_end_time.setImageResource(R.drawable.sic_drop_down_selected);
                break;
        }
    }

    private void showMenu(int type){
        switch (type){
            case 0: //分类
                if(menu_index_now < 0){
                    view_mask.startAnimation(AnimationUtil.getMaskViewAppearAnim());
                    view_mask.setVisibility(View.VISIBLE);
                    rl_filter_good_type.setVisibility(View.VISIBLE);
                    rv_good_types.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            rv_good_types.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            menuHeight = rv_good_types.getHeight();
                            ObjectAnimator.ofFloat(rv_good_types, "translationY", -menuHeight, 0).setDuration(200).start();
                        }
                    });
                    menu_index_now = 0;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_good_type.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_operator.setTextColor(getResources().getColor(R.color.six_six));
                    iv_operator.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_start_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_start_time.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_end_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_end_time.setImageResource(R.drawable.sic_drop_down_normal);
                }else if(menu_index_now == 0){
                    menu_index_now = -1;
                    view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                    view_mask.setVisibility(View.GONE);
                    ObjectAnimator.ofFloat(rv_good_types, "translationY", 0, -menuHeight).setDuration(200).start();
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_good_type.setImageResource(R.drawable.sic_drop_down_selected);
                }else{
                    if(menu_index_now == 1){
                        rl_filter_member.setVisibility(View.GONE);
                        rl_filter_good_type.setVisibility(View.VISIBLE);
                        rv_good_types.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                rv_good_types.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = rv_good_types.getHeight();
                                ObjectAnimator.ofFloat(rv_good_types, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }else if(menu_index_now == 2 || menu_index_now == 3){
                        ll_filter_time.setVisibility(View.GONE);
                        rl_filter_good_type.setVisibility(View.VISIBLE);
                        rv_good_types.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                rv_good_types.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = rv_good_types.getHeight();
                                ObjectAnimator.ofFloat(rv_good_types, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }
                    menu_index_now = 0;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_good_type.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_operator.setTextColor(getResources().getColor(R.color.six_six));
                    iv_operator.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_start_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_start_time.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_end_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_end_time.setImageResource(R.drawable.sic_drop_down_normal);
                }
                break;
            case 1: //操作人
                if(menu_index_now < 0){
                    view_mask.startAnimation(AnimationUtil.getMaskViewAppearAnim());
                    view_mask.setVisibility(View.VISIBLE);
                    rl_filter_member.setVisibility(View.VISIBLE);
                    rl_filter_member.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            rl_filter_member.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            menuHeight = rl_filter_member.getHeight();
                            ObjectAnimator.ofFloat(rl_filter_member, "translationY", -menuHeight, 0).setDuration(200).start();
                        }
                    });
                    menu_index_now = 1;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_good_type.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_operator.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_operator.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_start_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_start_time.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_end_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_end_time.setImageResource(R.drawable.sic_drop_down_normal);
                }else if(menu_index_now == 1){
                    menu_index_now = -1;
                    view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                    view_mask.setVisibility(View.GONE);
                    ObjectAnimator.ofFloat(rl_filter_member, "translationY", 0, -menuHeight).setDuration(200).start();
                    tv_operator.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_operator.setImageResource(R.drawable.sic_drop_down_selected);
                }else{
                    if(menu_index_now == 0){
                        rl_filter_member.setVisibility(View.VISIBLE);
                        rl_filter_good_type.setVisibility(View.GONE);
                        rl_filter_member.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                rl_filter_member.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = rl_filter_member.getHeight();
                                ObjectAnimator.ofFloat(rl_filter_member, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }else if(menu_index_now == 2 || menu_index_now == 3){
                        rl_filter_member.setVisibility(View.VISIBLE);
                        ll_filter_time.setVisibility(View.GONE);
                        rl_filter_member.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                rl_filter_member.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = rl_filter_member.getHeight();
                                ObjectAnimator.ofFloat(rl_filter_member, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }
                    menu_index_now = 1;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_good_type.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_operator.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_operator.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_start_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_start_time.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_end_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_end_time.setImageResource(R.drawable.sic_drop_down_normal);
                }
                break;
            case 2:
                if(menu_index_now < 0){
                    view_mask.startAnimation(AnimationUtil.getMaskViewAppearAnim());
                    view_mask.setVisibility(View.VISIBLE);
                    ll_filter_time.setVisibility(View.VISIBLE);
                    ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            menuHeight = ll_filter_time.getHeight();
                            ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                        }
                    });
                    menu_index_now = 2;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_good_type.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_operator.setTextColor(getResources().getColor(R.color.six_six));
                    iv_operator.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_start_time.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_start_time.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_end_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_end_time.setImageResource(R.drawable.sic_drop_down_normal);
                }else if(menu_index_now == 2){
                    menu_index_now = -1;
                    view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                    view_mask.setVisibility(View.GONE);
                    ObjectAnimator.ofFloat(ll_filter_time, "translationY", 0, -menuHeight).setDuration(200).start();
                    tv_start_time.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_start_time.setImageResource(R.drawable.sic_drop_down_selected);
                }else{
                    if(menu_index_now == 0){
                        ll_filter_time.setVisibility(View.VISIBLE);
                        rl_filter_good_type.setVisibility(View.GONE);
                        ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = ll_filter_time.getHeight();
                                ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }else if(menu_index_now == 1){
                        rl_filter_member.setVisibility(View.GONE);
                        ll_filter_time.setVisibility(View.VISIBLE);
                        ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = ll_filter_time.getHeight();
                                ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }else if(menu_index_now == 3){
                        ll_filter_time.setVisibility(View.GONE);
                        ll_filter_time.setVisibility(View.VISIBLE);
                        ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = ll_filter_time.getHeight();
                                ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }
                    menu_index_now = 2;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_good_type.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_operator.setTextColor(getResources().getColor(R.color.six_six));
                    iv_operator.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_start_time.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_start_time.setImageResource(R.drawable.sic_drop_up_selected);
                    tv_end_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_end_time.setImageResource(R.drawable.sic_drop_down_normal);
                }
                break;
            case 3:
                if(menu_index_now < 0){
                    view_mask.startAnimation(AnimationUtil.getMaskViewAppearAnim());
                    view_mask.setVisibility(View.VISIBLE);
                    ll_filter_time.setVisibility(View.VISIBLE);
                    ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                        @Override
                        public void onGlobalLayout() {
                            ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                            menuHeight = ll_filter_time.getHeight();
                            ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                        }
                    });
                    menu_index_now = 3;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_good_type.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_operator.setTextColor(getResources().getColor(R.color.six_six));
                    iv_operator.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_start_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_start_time.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_end_time.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_end_time.setImageResource(R.drawable.sic_drop_up_selected);
                }else if(menu_index_now == 3){
                    menu_index_now = -1;
                    view_mask.startAnimation(AnimationUtil.getMaskViewDisappearAnim());
                    view_mask.setVisibility(View.GONE);
                    ObjectAnimator.ofFloat(ll_filter_time, "translationY", 0, -menuHeight).setDuration(200).start();
                    tv_end_time.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_end_time.setImageResource(R.drawable.sic_drop_down_selected);
                }else{
                    if(menu_index_now == 0){
                        ll_filter_time.setVisibility(View.VISIBLE);
                        rl_filter_good_type.setVisibility(View.GONE);
                        ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = ll_filter_time.getHeight();
                                ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }else if(menu_index_now == 1){
                        rl_filter_member.setVisibility(View.GONE);
                        ll_filter_time.setVisibility(View.VISIBLE);
                        ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = ll_filter_time.getHeight();
                                ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }else if(menu_index_now == 2){
                        ll_filter_time.setVisibility(View.GONE);
                        ll_filter_time.setVisibility(View.VISIBLE);
                        ll_filter_time.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                            @Override
                            public void onGlobalLayout() {
                                ll_filter_time.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                                menuHeight = ll_filter_time.getHeight();
                                ObjectAnimator.ofFloat(ll_filter_time, "translationY", -menuHeight, 0).setDuration(200).start();
                            }
                        });
                    }
                    menu_index_now = 3;
                    tv_good_type_now.setTextColor(getResources().getColor(R.color.six_six));
                    iv_good_type.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_operator.setTextColor(getResources().getColor(R.color.six_six));
                    iv_operator.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_start_time.setTextColor(getResources().getColor(R.color.six_six));
                    iv_start_time.setImageResource(R.drawable.sic_drop_down_normal);
                    tv_end_time.setTextColor(getResources().getColor(R.color.main_blue));
                    iv_end_time.setImageResource(R.drawable.sic_drop_up_selected);
                }
                break;
        }
    }

    /**
     *  获取可选年份
     * */
    private List<String> getAllYears(){
        Calendar calendar = Calendar.getInstance();
        List<String> yearList = new ArrayList<>();
        for(int i = 2018;i<=calendar.get(Calendar.YEAR);i++){
            yearList.add(i+"");
        }
        return yearList;
    }

    /**
     * 月份列表
     * */
    private List<String> getMonths(){
        List<String> monthList = new ArrayList<>();
        for(int i = 1;i<=12;i++){
            monthList.add(i+"");
        }
        return monthList;
    }

    /**
     *  获取今年在列表中的索引
     * */
    private int getThisYearPosition(){
        Calendar a = Calendar.getInstance();
        List<String> yearList = getAllYears();
        for(int i = 0;i<yearList.size();i++){
            String year = yearList.get(i);
            if(a.get(Calendar.YEAR) == Integer.parseInt(year)){
                return i;
            }
        }
        return 0;
    }


    /**
     *  获取当前月在列表中的索引
     * */
    private int getThisMonthPosition(){
        Calendar a = Calendar.getInstance();
        List<String> monthList = getMonths();
        for(int i = 0;i<monthList.size();i++){
            String month = monthList.get(i);
            if(a.get(Calendar.MONTH) + 1 == Integer.parseInt(month)){
                return i;
            }
        }
        return 0;
    }

    /**
     *  获取今天在日期列表中的索引
     * */
    private int getDayOfMonthIndex(){
        Calendar calendar = Calendar.getInstance();
        List<String> daysList = getSpecifiedDays(calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH) + 1);
        int day = calendar.get(Calendar.DAY_OF_MONTH);
        for(int i = 0;i<daysList.size();i++){
            String d = daysList.get(i);
            if(d.equals(day+"")){
                return i;
            }
        }
        return 0;
    }

    /**
     *  获取指定月的天数集合
     * */
    private List<String> getSpecifiedDays(int year,int month){
        List<String> dayList = new ArrayList<>();
        Calendar a = Calendar.getInstance();
        a.set(Calendar.YEAR, year);
        a.set(Calendar.MONTH, month - 1);
        a.set(Calendar.DATE, 1);//把日期设置为当月第一天
        a.roll(Calendar.DATE, -1);//日期回滚一天，也就是最后一天
        int maxDate = a.get(Calendar.DATE);
        for(int i = 1;i<=maxDate;i++){
            dayList.add(i+"");
        }
        return dayList;
    }

    /**
     * 小时列表
     * */
    private List<String> getHours(){
        List<String> hourList = new ArrayList<>();
        for(int i = 0;i<=23;i++){
            hourList.add(i+"");
        }
        return hourList;
    }

    private int getHourNowIndex(){
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR_OF_DAY,8);
        for(int i = 0;i<=23;i++){
            if(i == calendar.get(Calendar.HOUR_OF_DAY)){
                return i;
            }
        }
        return 0;
    }

}
