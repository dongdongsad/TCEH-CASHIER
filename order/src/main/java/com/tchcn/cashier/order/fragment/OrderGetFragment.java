package com.tchcn.cashier.order.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.tchcn.cashier.order.R;
import com.tchcn.cashier.order.adapter.GetOrderAdapter;
import com.tchcn.library.dao.OrderModelDao;
import com.tchcn.library.model.OrderModel;
import com.tchcn.library.fragment.SDBaseFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * 取单界面
 * Created by sad on 2018/5/29.
 */

public class OrderGetFragment extends SDBaseFragment {

    RecyclerView recyclerView;
    GetOrderAdapter getOrderAdapter;
    List<OrderModel> orderModels = new ArrayList<>();

    private static OrderGetFragment fragment;


    public static OrderGetFragment getInstance(){
        if(fragment == null){
            fragment = new OrderGetFragment();
        }
        return fragment;
    }

    public static OrderGetFragment newInstance() {
        fragment = new OrderGetFragment();
        return fragment;
    }

    @Override
    protected int onCreateContentView() {
        return R.layout.frag_order_get;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(final View v) {
        orderModels = OrderModelDao.queryAllHangingOrder();
        recyclerView = (RecyclerView)v.findViewById(R.id.rv);
        getOrderAdapter =new GetOrderAdapter(orderModels);
        recyclerView.setAdapter(getOrderAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(),2));
        getOrderAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                getOrderAdapter.setSelectNum(position);
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.fl_container_left,OrderFragment.newInstance(orderModels.get(position))).commitAllowingStateLoss();
                OrderModelDao.deleteModel(orderModels.get(position));
            }
        });
    }

}
