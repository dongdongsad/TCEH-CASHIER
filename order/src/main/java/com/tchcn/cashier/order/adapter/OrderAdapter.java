package com.tchcn.cashier.order.adapter;

import android.graphics.Paint;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.model.GoodActModel;
import com.tchcn.library.utils.SDNumberUtil;

import java.util.List;

/**
 * 餐桌adapter
 * Created by sad on 2018/5/10.
 */

public class OrderAdapter extends BaseQuickAdapter<GoodActModel,BaseViewHolder> {

    private int selectNum = -1;

    public OrderAdapter(@Nullable List<GoodActModel> data) {
        super(R.layout.item_order_good,data);
    }

    @Override
    protected void convert(final BaseViewHolder helper, final GoodActModel item) {
        helper.setText(R.id.tv_name,item.getName())
                .setText(R.id.tv_num,item.getCarNum()+"")
                .setText(R.id.tv_price,"￥"+ SDNumberUtil.doubleTrans(item.getPrice()))
                .addOnClickListener(R.id.iv_add)
                .addOnClickListener(R.id.iv_sub)
                .addOnClickListener(R.id.tv_discount)
                .addOnClickListener(R.id.btn_free);
        if(selectNum == helper.getAdapterPosition()){
            if(item.getStatus() >0){
                helper.setVisible(R.id.ll_operate,false);
            }else
                helper.setVisible(R.id.ll_operate,true);
            helper.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.menu_light));
        }else {
            helper.setVisible(R.id.ll_operate,false);
            helper.itemView.setBackgroundColor(mContext.getResources().getColor(R.color.white));
        }
        final TextView tvPriceOrigin = helper.getView(R.id.tv_price_origin);
        final TextView tvPrice = helper.getView(R.id.tv_price);
        final TextView tvAct = helper.getView(R.id.tv_act);
        String norms = "";
        TextView tvNorm = helper.getView(R.id.tv_norm);
        if(item.getHasNorm() == 1 && item.getSelectNorms()!=null){
            for (int i = 0; i < item.getSelectNorms().size(); i++) {
                norms+=item.getSelectNorms().get(i).getName()+",";
            }
            tvNorm.setText(norms.substring(0,norms.length()-1));
            tvNorm.setVisibility(View.VISIBLE);
        }else
            tvNorm.setVisibility(View.GONE);
        if(item.getDiscount() >0) {
            double price = item.getPrice() * (item.getDiscount() / 10d);
            tvPriceOrigin.setText("￥" + SDNumberUtil.doubleTrans(item.getPrice()));
            tvPriceOrigin.getPaint().setFlags(Paint.STRIKE_THRU_TEXT_FLAG); //中划线
            tvPrice.setText("￥" + SDNumberUtil.doubleTrans(price));
            tvPriceOrigin.setVisibility(View.VISIBLE);
            tvAct.setText("折");
            tvAct.setVisibility(View.VISIBLE);
        }
        if(item.getFree() == 1){
            helper.setVisible(R.id.ll_operate,false);
            tvAct.setText("赠");
            tvAct.setVisibility(View.VISIBLE);
        }
        if(item.getStatus() > 0 ){
            helper.setVisible(R.id.iv_sub,false)
                    .setVisible(R.id.iv_add,false)
                    .setText(R.id.tv_num,"×"+item.getCarNum());
        }
        if(item.getStatus() == 2){
            helper.setVisible(R.id.tv_return,true);
        }else
            helper.setVisible(R.id.tv_return,false);
    }

    public void setSelectNum(int selectNum) {
        this.selectNum = selectNum;
        notifyDataSetChanged();
    }

    public GoodActModel getNum() {
        if(selectNum>=0)
        return getData().get(selectNum);
        else return null;
    }

    public int getSelectNum() {
        return selectNum;
    }
}
