package com.tchcn.cashier.order.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.model.GoodActModel;

/**
 * Created by sad on 2018/5/18.
 */

public class ReturnGoodDialog extends BaseNiceDialog{

    private GoodActModel actModel;
    TextView tvNum;
    int num;
    ChooseNumDialog.OnDialogClickListener onDialogClickListener = null;

    public static ReturnGoodDialog newInstance(GoodActModel actModel) {
        ReturnGoodDialog dialog = new ReturnGoodDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("good",actModel);
        dialog.setArguments(bundle);
        return dialog;
    }

    public ReturnGoodDialog setConvertListener(ChooseNumDialog.OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_return_good;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        actModel = (GoodActModel) bundle.getSerializable("good");
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        holder.setText(R.id.tv_title,actModel.getName());
        holder.setOnClickListener(R.id.iv_close, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tvNum = holder.getView(R.id.tv_num);
        holder.setText(R.id.tv_num,actModel.getCarNum()+"");
        num = actModel.getCarNum();
        holder.setOnClickListener(R.id.tv_commit, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogClickListener.onItemClick(v,dialog);
            }
        });
        holder.setOnClickListener(R.id.iv_add, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(num < actModel.getCarNum()){
                    tvNum.setText(++num +"");
                }

            }
        });

        holder.setOnClickListener(R.id.iv_sub, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(num > 1 && num <= actModel.getCarNum()){
                    tvNum.setText(--num +"");
                }

            }
        });

    }

    public int getNum() {
        return num;
    }
}
