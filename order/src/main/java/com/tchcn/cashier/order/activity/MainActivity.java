package com.tchcn.cashier.order.activity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tchcn.cashier.order.R;
import com.tchcn.cashier.order.R2;
import com.tchcn.cashier.order.fragment.ChooseGoodFragment;
import com.tchcn.cashier.order.fragment.ChooseTableFragment;
import com.tchcn.cashier.order.fragment.OrderFragment;
import com.tchcn.cashier.order.fragment.OrderLeftFragment;
import com.tchcn.library.dao.OrderModelDao;
import com.tchcn.library.db.DbManagerX;
import com.tchcn.library.model.GoodActModel;
import com.tchcn.library.model.OrderModel;
import com.tchcn.library.model.TableModel;
import com.tchcn.library.activity.SDBaseActivity;

import org.xutils.ex.DbException;

import butterknife.BindView;
import butterknife.ButterKnife;


public class MainActivity extends SDBaseActivity {


    @BindView(R2.id.tv_store_name)
    TextView tvStoreName;
    @BindView(R2.id.tv_user)
    TextView tvUser;
    @BindView(R2.id.ll_table)
    LinearLayout llTable;//桌台
    @BindView(R2.id.ll_order)
    LinearLayout llOrder;//点餐
    @BindView(R2.id.ll_take_away)
    LinearLayout llTakeAway;//外卖
    @BindView(R2.id.ll_list)
    LinearLayout llList;//订单
    @BindView(R2.id.ll_member)
    LinearLayout llMember;//会员
    @BindView(R2.id.ll_shift)
    LinearLayout llShift;//交班
    @BindView(R2.id.ll_settlement)
    LinearLayout llSettlement;//沽清
    @BindView(R2.id.ll_printer)
    LinearLayout llPrinter;//打印
    @BindView(R2.id.ll_more)
    LinearLayout llMore;//更多
    @BindView(R2.id.fl_container_left)
    FrameLayout flContainerLeft;
    @BindView(R2.id.fl_container_right)
    FrameLayout flContainerRight;
    @BindView(R2.id.tv_exit)
    TextView tvExit;

    private LinearLayout currentMenu;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();
        hideBottomUIMenu();
    }

    private void initView() {
        currentMenu = llTable;
        llTable.setSelected(true);
        tvExit.setOnClickListener(this);
        llTable.setOnClickListener(this);
        llOrder.setOnClickListener(this);
        llTakeAway.setOnClickListener(this);
        llMember.setOnClickListener(this);
        llList.setOnClickListener(this);
        llShift.setOnClickListener(this);
        llSettlement.setOnClickListener(this);
        llPrinter.setOnClickListener(this);
        llMore.setOnClickListener(this);

        getSDFragmentManager().beginTransaction()
                .add(R.id.fl_container_left,OrderLeftFragment.getInstance())
                .add(R.id.fl_container_right,ChooseTableFragment.getInstance())
                .commitAllowingStateLoss();

            //tvUser.setText(LocalUserModelDao.queryModel().getReal_name());
    }




    @Override
    public void onClick(View v) {
        super.onClick(v);
        int i = v.getId();
        if (i == R.id.tv_exit) {
            //finish();
            Intent intent=new Intent("com.tchcn.cashier.activity.RegisterActivity");
            startActivity(intent);
        } else if (i == R.id.ll_table) {
            if(checkCurrent(llTable)){
                getSDFragmentManager().beginTransaction().replace(R.id.fl_container_left,OrderLeftFragment.newInstance()).replace(R.id.fl_container_right, ChooseTableFragment.newInstance()).commitAllowingStateLoss();
            }
        } else if (i == R.id.ll_order) {
            if(checkCurrent(llOrder)){
                getSDFragmentManager().beginTransaction().replace(R.id.fl_container_left, OrderFragment.newInstance(null)).replace(R.id.fl_container_right,ChooseGoodFragment.newInstance(2)).commitAllowingStateLoss();
            }
        } else if (i == R.id.ll_take_away) {
            checkCurrent(llTakeAway);
        } else if (i == R.id.ll_list) {
            checkCurrent(llList);
        } else if (i == R.id.ll_member) {
            checkCurrent(llMember);
        } else if (i == R.id.ll_shift) {
            checkCurrent(llShift);
        } else if (i == R.id.ll_settlement) {
            checkCurrent(llSettlement);
        } else if (i == R.id.ll_printer) {
            checkCurrent(llPrinter);
        } else if (i == R.id.ll_more) {
            checkCurrent(llMore);
            OrderModelDao.deleteAllModel();
        }
    }

    //菜单选择
    private boolean checkCurrent(LinearLayout ll) {
        ll.setSelected(true);
        if(currentMenu == ll)
            return false;
        currentMenu.setSelected(false);
        currentMenu = ll;
        return true;
    }


    /**
     * orderfragment 点击购物车更新goodfragment数据
     * @param goodActModel
     * @param isAdd
     */
    public void notifyGoodFragment(GoodActModel goodActModel,boolean isAdd){
        ChooseGoodFragment.getInstance().notifyNum(goodActModel,isAdd);
    }

    /**
     * orderfragment点击清空菜品已选
     */
    public void clearGood() {
        ChooseGoodFragment.getInstance().clearData();
    }

    /**
     * 选择桌台后设置桌台信息
     * @param tableModel
     */
     public void showGoodsFragment(TableModel tableModel) {
        getSupportFragmentManager().beginTransaction().add(R.id.fl_container_right,ChooseGoodFragment.newInstance(1)).commitAllowingStateLoss();
        OrderLeftFragment.getInstance().setTableData(tableModel);
    }


    /**
     * 切换指定fragment
     * @param instance
     */
    public void changeRightFragment(Fragment instance) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container_right,instance).commitAllowingStateLoss();
    }

    /**
     * 切换指定fragment
     * @param instance
     */
    public void changeLeftFragment(Fragment instance) {
        getSupportFragmentManager().beginTransaction().replace(R.id.fl_container_left,instance).commitAllowingStateLoss();
    }


    protected void hideBottomUIMenu() {
        //隐藏虚拟按键，并且全屏
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) { // lower api
            View v = this.getWindow().getDecorView();
            v.setSystemUiVisibility(View.GONE);
        } else if (Build.VERSION.SDK_INT >= 19) {
            //for new api versions.
            View decorView = getWindow().getDecorView();
            int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY | View.SYSTEM_UI_FLAG_FULLSCREEN;
            decorView.setSystemUiVisibility(uiOptions);
        }
    }
}
