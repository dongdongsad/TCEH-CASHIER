package com.tchcn.cashier.order.fragment;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.tchcn.cashier.order.R;
import com.tchcn.cashier.order.view.FlowTagLayout;
import com.tchcn.library.fragment.SDBaseFragment;
import com.tchcn.library.utils.SDViewUtil;


/**
 * 备注
 * Created by sad on 2018/5/10.
 */

public class OrderRemarkFragment extends SDBaseFragment {

    TextView tvCommit;
    EditText etRemarks;
    TextView tvNum;
    FlowTagLayout flowTagLayout;
    private static OrderRemarkFragment fragment;



    public static OrderRemarkFragment getInstance() {
        if (fragment == null) {
            fragment = new OrderRemarkFragment();
        }
        return fragment;
    }


    @Override
    protected int onCreateContentView() {
        return R.layout.frag_order_remark;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(final View v) {
        tvCommit = (TextView) v.findViewById(R.id.tv_commit);
        etRemarks = (EditText) v.findViewById(R.id.et_remarks);
        tvNum = (TextView) v.findViewById(R.id.tv_num);
        flowTagLayout = (FlowTagLayout) v.findViewById(R.id.flowTagLayout);
        tvCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String remark = "";
                String text = etRemarks.getText().toString();
                String remarkName = "";
                String remarksId = "";
                for (int i = 0; i < flowTagLayout.getChildCount(); i++) {
                    TextView textView = (TextView) flowTagLayout.getChildAt(i);
                    if(textView.isSelected() == true) {
                        remarkName += textView.getText().toString() + ",";
                        remarksId += i+",";
                    }
                }
                if(!"".equals(remarkName)) {
                    remarkName = remarkName.substring(0, remarkName.length() - 1);
                    remarksId = remarksId.substring(0, remarksId.length() - 1);
                }
                if(!"".equals(text)&&!"".equals(remarkName)){
                    remark = text + "," + remarkName;
                }else {
                    remark = text + remarkName;
                }
                if(OrderLeftFragment.getInstance().isAdded())
                OrderLeftFragment.getInstance().setRemarks(remark,remarksId);
                if(OrderFragment.getInstance().isAdded())
                OrderFragment.getInstance().setRemarks(remark,remarksId);
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                // 隐藏软键盘
                imm.hideSoftInputFromWindow(etRemarks.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
            }
        });
        etRemarks.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                tvNum.setText(editable.toString().length()+"/50");
            }
        });
        for (int i = 0; i < 4; i++) {
            final TextView textView = makeTextView("备注"+i);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(textView.isSelected() ==true){
                        textView.setSelected(false);
                    }else
                        textView.setSelected(true);
                }
            });
            flowTagLayout.addView(textView);
        }
    }

    private TextView makeTextView(String name) {
        final TextView textView = new TextView(getActivity());
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(SDViewUtil.px2sp(24));
        int padding = SDViewUtil.dp2px(10);
        textView.setPadding(padding, padding / 2, padding, padding / 2);
        textView.setBackgroundResource(R.drawable.selector_norms_bg);
        Resources resource = getActivity().getResources();
        ColorStateList csl = resource.getColorStateList(R.color.norm_text_color);
        if (csl != null) {
            textView.setTextColor(csl);
        }
        textView.setText(name);
        return textView;
    }

}
