package com.tchcn.cashier.order.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tchcn.cashier.order.R;
import com.tchcn.cashier.order.activity.MainActivity;
import com.tchcn.library.dao.OrderModelDao;
import com.tchcn.library.fragment.SDBaseFragment;
import com.tchcn.library.model.OrderModel;

import java.text.DecimalFormat;


/**
 * 付款
 * Created by sad on 2018/5/17.
 */
public class PayFragment extends SDBaseFragment {
    TextView tvAmountReceivable;
    TextView tvAmountActual;
    TextView tvAmountReturn;
    TextView tvCash;
    TextView tvWechat;
    TextView tvAli;
    TextView tvCard;
    TextView tvFifty;
    TextView tvHundred;
    TextView tvNum1;
    TextView tvNum4;
    TextView tvNum7;
    TextView tvNum00;
    TextView tvNum2;
    TextView tvNum5;
    TextView tvNum8;
    TextView tvNum0;
    TextView tvNum3;
    TextView tvNum6;
    TextView tvNum9;
    TextView tvPoint;
    TextView tvClear;
    TextView tvCommit;
    RelativeLayout rlBackspace;


    static PayFragment fragment;
    OrderModel orderModel;
    MainActivity activity;

    private long actualAmount = 0;
    private String pointPart = "";
    private boolean hasPoint = false;

    public static PayFragment newInstance(OrderModel orderModel) {
        fragment = new PayFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("order", orderModel);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int onCreateContentView() {
        return R.layout.frag_pay;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(View view) {
        tvAmountReceivable = (TextView) view.findViewById(R.id.tv_amount_receivable);
        tvAmountActual = (TextView) view.findViewById(R.id.tv_amount_actual);
        tvAmountReturn = (TextView) view.findViewById(R.id.tv_amount_return);
        tvCash = (TextView) view.findViewById(R.id.tv_cash);
        tvWechat = (TextView) view.findViewById(R.id.tv_wechat);
        tvAli = (TextView) view.findViewById(R.id.tv_ali);
        tvCard = (TextView) view.findViewById(R.id.tv_card);
        tvFifty = (TextView) view.findViewById(R.id.tv_fifty);
        tvHundred = (TextView) view.findViewById(R.id.tv_hundred);
        tvNum1 = (TextView) view.findViewById(R.id.tv_num1);
        tvNum4 = (TextView) view.findViewById(R.id.tv_num4);
        tvNum7 = (TextView) view.findViewById(R.id.tv_num7);
        tvNum00 = (TextView) view.findViewById(R.id.tv_num00);
        tvNum2 = (TextView) view.findViewById(R.id.tv_num2);
        tvNum5 = (TextView) view.findViewById(R.id.tv_num5);
        tvNum8 = (TextView) view.findViewById(R.id.tv_num8);
        tvNum0 = (TextView) view.findViewById(R.id.tv_num0);
        tvNum3 = (TextView) view.findViewById(R.id.tv_num3);
        tvNum6 = (TextView) view.findViewById(R.id.tv_num6);
        tvNum9 = (TextView) view.findViewById(R.id.tv_num9);
        tvPoint = (TextView) view.findViewById(R.id.tv_point);
        tvClear = (TextView) view.findViewById(R.id.tv_clear);
        tvCommit = (TextView) view.findViewById(R.id.tv_commit);
        rlBackspace = (RelativeLayout) view.findViewById(R.id.rl_backspace);

        tvCash.setOnClickListener(this);
        tvWechat.setOnClickListener(this);
        tvAli.setOnClickListener(this);
        tvCard.setOnClickListener(this);
        tvFifty.setOnClickListener(this);
        tvHundred.setOnClickListener(this);
        tvNum1.setOnClickListener(this);
        tvNum4.setOnClickListener(this);
        tvNum7.setOnClickListener(this);
        tvNum00 .setOnClickListener(this);
        tvNum2 .setOnClickListener(this);
        tvNum5 .setOnClickListener(this);
        tvNum8 .setOnClickListener(this);
        tvNum0 .setOnClickListener(this);
        tvNum3 .setOnClickListener(this);
        tvNum6 .setOnClickListener(this);
        tvNum9 .setOnClickListener(this);
        tvPoint.setOnClickListener(this);
        tvClear.setOnClickListener(this);
        tvCommit .setOnClickListener(this);
        rlBackspace .setOnClickListener(this);

        if (getArguments() != null) {
            if (getArguments().getSerializable("order") != null)
                orderModel = (OrderModel) getArguments().getSerializable("order");
        }
        tvAmountReceivable.setText("￥ "+ orderModel.getAmountReceivable());

        activity = (MainActivity)getActivity();
    }


    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.tv_cash) {
            tvCash.setSelected(true);
            tvAli.setSelected(false);
            tvCard.setSelected(false);
            tvWechat.setSelected(false);
        } else if (i == R.id.tv_wechat) {
            tvWechat.setSelected(true);
            tvAli.setSelected(false);
            tvCard.setSelected(false);
            tvCash.setSelected(false);
        } else if (i == R.id.tv_ali) {
            tvAli.setSelected(true);
            tvWechat.setSelected(false);
            tvCard.setSelected(false);
            tvCash.setSelected(false);
        } else if (i == R.id.tv_card) {
            tvCard.setSelected(true);
            tvWechat.setSelected(false);
            tvAli.setSelected(false);
            tvCash.setSelected(false);
        } else if (i == R.id.tv_fifty) {
            actualAmount = 0;
            hasPoint = false;
            updateAmount(50);
        } else if (i == R.id.tv_hundred) {
            actualAmount = 0;
            hasPoint = false;
            updateAmount(100);
        } else if (i == R.id.tv_num1) {
            updateAmount(1);
        } else if (i == R.id.tv_num4) {
            updateAmount(4);
        } else if (i == R.id.tv_num7) {
            updateAmount(7);
        } else if (i == R.id.tv_num00) {
            updateAmount(10);
        } else if (i == R.id.tv_num2) {
            updateAmount(2);
        } else if (i == R.id.tv_num5) {
            updateAmount(5);
        } else if (i == R.id.tv_num8) {
            updateAmount(8);
        } else if (i == R.id.tv_num0) {
            updateAmount(0);
        } else if (i == R.id.tv_num3) {
            updateAmount(3);
        } else if (i == R.id.tv_num6) {
            updateAmount(6);
        } else if (i == R.id.tv_num9) {
            updateAmount(9);
        } else if (i == R.id.tv_point) {
            if(!hasPoint){
                hasPoint = true;
            }
        } else if (i == R.id.rl_backspace) {
            if(pointPart.length()>0){
                pointPart = pointPart.substring(0,pointPart.length()-1);
                if(pointPart.length() == 0)
                tvAmountActual.setText("￥ "+actualAmount +"."+(pointPart.length() == 1? (pointPart+"0"):"00"));
            }else if(hasPoint){
                hasPoint = false;
            }else if(!hasPoint){
                actualAmount= actualAmount/10;
                tvAmountActual.setText("￥ "+actualAmount +".00");
            }
            tvAmountReturn.setText("￥ 0.00");
        } else if (i == R.id.tv_clear) {
            actualAmount = 0;
            pointPart = "";
            updateAmount(0);
        } else if (i == R.id.tv_commit) {
            orderModel.setStatus(2);
            OrderModelDao.insertModel(orderModel);
            activity.changeRightFragment(ChooseGoodFragment.newInstance(2));
        }
    }

    private void updateAmount(int amount) {
        double money = 0;
        if(!hasPoint){
            if(amount<10)
                actualAmount = actualAmount*10 +amount;
            else if(amount == 10)
                actualAmount = actualAmount*100;
            else if(amount ==50)
                actualAmount = 50;
            else if(amount ==100)
                actualAmount =100;
            tvAmountActual.setText("￥ "+actualAmount +".00");
            money = actualAmount;
        }else if(pointPart.length() <2){
            if(pointPart.length() == 0){
                pointPart = amount+"";
            }else
                pointPart =  pointPart + amount;
            tvAmountActual.setText("￥ "+actualAmount +"."+(pointPart.length() == 1? (pointPart+"0"):pointPart));

           money= actualAmount + Double.parseDouble((pointPart.length() == 1? (pointPart+"0"):pointPart))/100;
        }

        if(money > orderModel.getAmountReceivable()){
            DecimalFormat df = new DecimalFormat("#.00");
            tvAmountReturn.setText("￥ "+df.format(money - orderModel.getAmountReceivable()));
        }else {
            tvAmountReturn.setText("￥ 0.00");
        }
    }
}
