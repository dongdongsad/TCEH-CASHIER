package com.tchcn.cashier.order.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.othershe.nicedialog.BaseNiceDialog;
import com.tchcn.cashier.order.R;
import com.tchcn.cashier.order.activity.MainActivity;
import com.tchcn.cashier.order.adapter.ChooseGoodAdapter;
import com.tchcn.cashier.order.adapter.RightMenuAdapter;
import com.tchcn.cashier.order.dialog.ChooseNormsDialog;
import com.tchcn.cashier.order.dialog.ChooseNumDialog;
import com.tchcn.library.model.GoodActModel;
import com.tchcn.library.model.NormsModel;
import com.tchcn.library.fragment.SDBaseFragment;
import com.tchcn.library.model.TypeActModel;
import com.tchcn.library.utils.SDViewUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 选择餐桌
 * Created by sad on 2018/5/8.
 */

public class ChooseGoodFragment extends SDBaseFragment {

    EditText etSearch;
    RecyclerView rvGoods;
    RecyclerView rvRightMenu;


    ChooseGoodAdapter chooseGoodAdapter;
    MainActivity mainActivity;

    List<GoodActModel> goodLists = new ArrayList<>();
    List<NormsModel> normModels = new ArrayList<>();

    private static ChooseGoodFragment fragment;
    private int type = 1;

    RightMenuAdapter rightMenuAdapter;
    List<TypeActModel.TypeModel> rightMenus = new ArrayList<>();

    public static ChooseGoodFragment getInstance(){
        if(fragment == null){
            fragment = new ChooseGoodFragment();
        }
        return fragment;
    }

    public static ChooseGoodFragment newInstance(int type){
        fragment = new ChooseGoodFragment();
        Bundle bundle =new Bundle();
        bundle.putInt("type",type);
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    protected int onCreateContentView() {
        return R.layout.frag_choose_good;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(final View v) {
        mainActivity  = (MainActivity) getActivity();
        if(getArguments()!=null){
            if(getArguments().getInt("type")>0)
                type=getArguments().getInt("type");
        }
        etSearch = (EditText) v.findViewById(R.id.et_search);
        rvGoods = (RecyclerView) v.findViewById(R.id.rv_goods);
        rvRightMenu = (RecyclerView) v.findViewById(R.id.rv_type);
        chooseGoodAdapter = new ChooseGoodAdapter(goodLists);
        chooseGoodAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, final int position) {
                final GoodActModel actModel = chooseGoodAdapter.getData().get(position);
                if(type==1&&OrderLeftFragment.getInstance().isConfirmOrder == true)
                    return;
                    //如果有规格
                if (chooseGoodAdapter.getData().get(position).getHasNorm() == 1) {
                    final ChooseNormsDialog normDialog = ChooseNormsDialog.newInstance(actModel);
                    normDialog.setConvertListener(new ChooseNumDialog.OnDialogClickListener() {
                        @Override
                        public void onItemClick(View view, BaseNiceDialog dialog) {
                            if (view.getId() == R.id.tv_commit) {
                                String ids = "";
                                List<NormsModel> normsModels = normDialog.getSelectNorms();
                                for (int i = 0; i < normsModels.size(); i++) {
                                    ids += normsModels.get(i).getId() + ",";
                                }
                                actModel.setSelectNorms(normDialog.getSelectNorms());
                                actModel.setNormsId(ids.substring(0, ids.length() - 1));
                                if(type == 1)
                                    OrderLeftFragment.getInstance().addShopCart(actModel);
                                else
                                    OrderFragment.getInstance().addShopCart(actModel);
                                actModel.setCarNum(actModel.getCarNum() + 1);
                                chooseGoodAdapter.notifyDataSetChanged();
                                dialog.dismiss();
                            }
                        }
                    }).setWidth(SDViewUtil.px2dp(568))
                            .show(getActivity().getSupportFragmentManager());
                } else {
                    actModel.setCarNum(actModel.getCarNum() + 1);
                    chooseGoodAdapter.notifyDataSetChanged();
                    if(type == 1)
                        OrderLeftFragment.getInstance().addShopCart(actModel);
                    else
                        OrderFragment.getInstance().addShopCart(actModel);
                }

            }
        });
        rvGoods.setAdapter(chooseGoodAdapter);
        rvGoods.setLayoutManager(new GridLayoutManager(getActivity(),6));
        rvRightMenu.setLayoutManager(new LinearLayoutManager(getActivity()));
        rightMenuAdapter = new RightMenuAdapter(rightMenus);

        rightMenuAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                rightMenuAdapter.setSelect(position);
                setSelectType(position);
            }
        });
        rightMenuAdapter.bindToRecyclerView(rvRightMenu);
        initData();
    }

    private void initData() {
        normModels.clear();
        for (int i = 0; i < 6; i++) {
            NormsModel norm = new NormsModel();
            norm.setName("规格"+i);
            List<NormsModel> sss= new ArrayList<>();
            for (int j = 0; j < 3; j++) {
                NormsModel smallNorm = new NormsModel();
                smallNorm.setId(i*10+j);
                smallNorm.setName("口味"+j);
                sss.add(smallNorm);
            }
            norm.setNorms(sss);
            normModels.add(norm);
        }
        for (int i = 0; i < 20; i++) {
            GoodActModel good = new GoodActModel();
            good.setId(i);
            good.setName("笑面"+i);
            good.setPrice(20+i);
            good.setTypeId(i%4+1);
            if(i%4 == 0){
                good.setHasNorm(1);
                good.setNormsModel(normModels);
            }
            goodLists.add(good);
        }
        chooseGoodAdapter.notifyDataSetChanged();
        //CommonInterface.getGoodsType()

    }


    public void setSelectType(int selectType) {
        List<GoodActModel> newGoodList = new ArrayList<>();
        if(selectType >0) {
            for (int i = 0; i < goodLists.size(); i++) {
                if (selectType == goodLists.get(i).getTypeId())
                    newGoodList.add(goodLists.get(i));
            }
            chooseGoodAdapter.setNewData(newGoodList);
        }else
            chooseGoodAdapter.setNewData(goodLists);
    }

    public void notifyNum(GoodActModel goodActModel, boolean isAdd) {
        for (int i = 0; i < goodLists.size(); i++) {
            GoodActModel good = goodLists.get(i);
            if(goodActModel.getId() == good.getId()){
                if(isAdd){
                    good.setCarNum(good.getCarNum()+1);
                }else {
                    good.setCarNum(good.getCarNum()-1);
                }
            }
        }
        chooseGoodAdapter.notifyDataSetChanged();
    }

    public void clearData() {
        for (int i = 0; i < goodLists.size(); i++) {
            goodLists.get(i).setCarNum(0);
        }
        chooseGoodAdapter.notifyDataSetChanged();
    }
}
