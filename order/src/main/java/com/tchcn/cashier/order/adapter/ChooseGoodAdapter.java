package com.tchcn.cashier.order.adapter;

import android.support.annotation.Nullable;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.model.GoodActModel;
import com.tchcn.library.utils.SDNumberUtil;
import com.tchcn.library.utils.SDToast;

import java.util.List;

/**
 * 餐桌adapter
 * Created by sad on 2018/5/3.
 */

public class ChooseGoodAdapter extends BaseQuickAdapter<GoodActModel,BaseViewHolder> {
    public ChooseGoodAdapter(@Nullable List<GoodActModel> data) {
        super(R.layout.item_good,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, GoodActModel item) {
        helper.setText(R.id.tv_good,item.getName())
                .setText(R.id.tv_price,"￥"+ SDNumberUtil.doubleTrans(item.getPrice()));
        if (item.getCarNum() > 0) {
            helper.setVisible(R.id.tv_num, true)
                    .setText(R.id.tv_num, item.getCarNum() + "");
        } else
            helper.setVisible(R.id.tv_num, false);
        helper.getView(R.id.tv_good).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SDToast.showToast("hello");
            }
        });
    }

}
