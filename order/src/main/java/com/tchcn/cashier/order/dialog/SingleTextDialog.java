package com.tchcn.cashier.order.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.order.R;

/**
 * Created by sad on 2018/5/18.
 */

public class SingleTextDialog extends BaseNiceDialog{

    private String title;
    ChooseNumDialog.OnDialogClickListener onDialogClickListener = null;

    public static SingleTextDialog newInstance(String title) {
        SingleTextDialog dialog = new SingleTextDialog();
        Bundle bundle = new Bundle();
        bundle.putString("title",title);
        dialog.setArguments(bundle);
        return dialog;
    }

    public SingleTextDialog setConvertListener(ChooseNumDialog.OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_single_text;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        title = bundle.getString("title");
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        if(title != null)
        holder.setText(R.id.tv_title,title);
        holder.setOnClickListener(R.id.tv_cancel, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        holder.setOnClickListener(R.id.tv_commit, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onDialogClickListener.onItemClick(v,dialog);
            }
        });
    }


}
