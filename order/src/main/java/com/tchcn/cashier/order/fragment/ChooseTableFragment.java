package com.tchcn.cashier.order.fragment;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.fanwe.library.adapter.http.model.SDResponse;
import com.othershe.nicedialog.BaseNiceDialog;
import com.tchcn.cashier.order.R;
import com.tchcn.cashier.order.activity.MainActivity;
import com.tchcn.cashier.order.adapter.ChooseTableAdapter;
import com.tchcn.cashier.order.adapter.RightMenuAdapter;
import com.tchcn.cashier.order.dialog.ChooseNumDialog;
import com.tchcn.library.common.CommonInterface;
import com.tchcn.library.dao.LocalUserModelDao;
import com.tchcn.library.http.AppRequestCallback;
import com.tchcn.library.model.TableActModel;
import com.tchcn.library.model.TableModel;
import com.tchcn.library.fragment.SDBaseFragment;
import com.tchcn.library.model.TypeActModel;
import com.tchcn.library.utils.SDViewUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 选择餐桌
 * Created by sad on 2018/5/3.
 */

public class ChooseTableFragment extends SDBaseFragment {
    //全部桌型
    TextView tvTableAll;
    //开台
    LinearLayout llTableOpen;
    //空闲
    LinearLayout llFree;
    //预定
    LinearLayout llReserve;
    //停用
    LinearLayout llStop;
    RecyclerView rvTable;
    RecyclerView rvRightMenu;

    ChooseTableAdapter chooseTableAdapter;

    List<TableModel> tableModelLists = new ArrayList<>();

    private View currentView;

    MainActivity activity;

    RightMenuAdapter rightMenuAdapter;
    List<TypeActModel.TypeModel> rightMenus = new ArrayList<>();

    private static ChooseTableFragment fragment;

    public static ChooseTableFragment getInstance(){
        if(fragment == null){
            fragment = new ChooseTableFragment();
        }
        return fragment;
    }

    public static ChooseTableFragment newInstance(){
            fragment = new ChooseTableFragment();
        return fragment;
    }

    @Override
    protected int onCreateContentView() {
        return R.layout.frag_choose_table;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(final View v) {
        activity  = (MainActivity)getActivity();
        tvTableAll = (TextView)v.findViewById(R.id.tv_table_all);
        llTableOpen = (LinearLayout) v.findViewById(R.id.ll_table_open);
        llFree = (LinearLayout)v.findViewById(R.id.ll_free);
        llReserve = (LinearLayout)v.findViewById(R.id.ll_reserve);
        llStop = (LinearLayout)v.findViewById(R.id.ll_stop);
        rvTable = (RecyclerView) v.findViewById(R.id.rv_table);
        rvRightMenu = (RecyclerView) v.findViewById(R.id.rv_type);
        tvTableAll.setOnClickListener(this);
        llTableOpen.setOnClickListener(this);
        llFree.setOnClickListener(this);
        llReserve.setOnClickListener(this);
        llStop.setOnClickListener(this);
        chooseTableAdapter = new ChooseTableAdapter(tableModelLists);
        chooseTableAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, final int position) {
                if(chooseTableAdapter.getData().get(position).getStatus() == 1) {
                    final ChooseNumDialog numDialog = ChooseNumDialog.newInstance();
                    numDialog.setConvertListener(new ChooseNumDialog.OnDialogClickListener(){
                                @Override
                                public void onItemClick(View view, BaseNiceDialog dialog) {
                                    if(view.getId() == R.id.tv_commit){
                                        if(numDialog.getNum()>0) {
                                            //就餐人数
                                            chooseTableAdapter.getData().get(position).setNum(numDialog.getNum());
                                            //切换菜品
                                            activity.showGoodsFragment(chooseTableAdapter.getData().get(position));
                                            dialog.dismiss();
                                        }
                                    }
                                }
                            })
                            .setWidth(SDViewUtil.px2dp(568))
                            .show(getActivity().getSupportFragmentManager());     //调节灰色背景透明度[0-1]，默认0.5f
                }
            }
        });
        rvTable.setAdapter(chooseTableAdapter);
        rvTable.setLayoutManager(new GridLayoutManager(getActivity(),6));
        initData();
        tvTableAll.setSelected(true);
        currentView = tvTableAll;
    }

    private void initData() {
       /* CommonInterface.getAllTables("", "", 1, LocalUserModelDao.queryModel().getId(), new AppRequestCallback<TableActModel>() {
            @Override
            protected void onSuccess(SDResponse sdResponse) {
                tableModelLists.clear();
                tableModelLists.addAll(actModel.getList());
                chooseTableAdapter.notifyDataSetChanged();
            }
        });*/
        for (int i = 0; i < 5; i++) {
          /*  RightMenuModel rightModel = new RightMenuModel();
            if ( i == 0 ) {
                rightModel.setName("全部");
                rightModel.setId(-1);
            }else {
                rightModel.setName("桌型" + i);
                rightModel.setId(i);
            }
            rightMenus.add(rightModel);*/
        }
        rvRightMenu.setLayoutManager(new LinearLayoutManager(getActivity()));
        rightMenuAdapter = new RightMenuAdapter(rightMenus);
        rightMenuAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                rightMenuAdapter.setSelect(position);
                setSelectType(position);
            }
        });
        rvRightMenu.setAdapter(rightMenuAdapter);
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        int i = v.getId();
        if(i == R.id.tv_table_all){
            currentView.setSelected(false);
            tvTableAll.setSelected(true);
            currentView = tvTableAll;
            setSelectStatus(-1);
        }else if(i == R.id.ll_table_open){
            currentView.setSelected(false);
            currentView = llTableOpen;
            llTableOpen.setSelected(true);
            setSelectStatus(3);
        }else if(i == R.id.ll_free){
            currentView.setSelected(false);
            currentView = llFree;
            llFree.setSelected(true);
            setSelectStatus(1);
        }else if(i == R.id.ll_reserve){
             currentView.setSelected(false);
            currentView = llReserve;
            llReserve.setSelected(true);
            setSelectStatus(4);
        }else if(i == R.id.ll_stop){
            currentView.setSelected(false);
            currentView = llStop;
            llStop.setSelected(true);
            setSelectStatus(2);
        }
    }

    //更新table
    public void  setSelectType(int type){
        List<TableModel> newTableModelList = new ArrayList<>();
        if(type >0) {
            for (int i = 0; i < tableModelLists.size(); i++) {
                if (type == tableModelLists.get(i).getType())
                    newTableModelList.add(tableModelLists.get(i));
            }
            chooseTableAdapter.setNewData(newTableModelList);
        }else
            chooseTableAdapter.setNewData(tableModelLists);
    }

    //更新table
    public void  setSelectStatus(int status){
        List<TableModel> newTableModelList = new ArrayList<>();
        if(status >0) {
            for (int i = 0; i < tableModelLists.size(); i++) {
                if (status == tableModelLists.get(i).getStatus())
                    newTableModelList.add(tableModelLists.get(i));
            }
            chooseTableAdapter.setNewData(newTableModelList);
        }else
            chooseTableAdapter.setNewData(tableModelLists);
    }

}
