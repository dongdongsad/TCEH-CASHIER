package com.tchcn.cashier.order.dialog;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.order.R;

/**
 * Created by sad on 2018/5/5.
 */

public class ChooseDiscountDialog extends BaseNiceDialog{

    TextView tv0;
    TextView tv1;
    TextView tv2;
    TextView tv3;
    TextView tv4;
    TextView tv5;
    TextView tv6;
    TextView tv7;
    TextView tv8;
    TextView tv9;
    TextView tvPoint;
    TextView tvClear;
    TextView tvCommit;
    TextView tvNum;

    ChooseNumDialog.OnDialogClickListener onDialogClickListener = null;
    public static ChooseDiscountDialog newInstance() {
        ChooseDiscountDialog dialog = new ChooseDiscountDialog();
        return dialog;
    }

    public ChooseDiscountDialog setConvertListener(ChooseNumDialog.OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_choose_discount;
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        holder.setOnClickListener(R.id.iv_close, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        tv0 = (TextView) holder.getConvertView().findViewById(R.id.tv_num0);
        tv1 = (TextView) holder.getConvertView().findViewById(R.id.tv_num1);
        tv2 = (TextView) holder.getConvertView().findViewById(R.id.tv_num2);
        tv3 = (TextView) holder.getConvertView().findViewById(R.id.tv_num3);
        tv4 = (TextView) holder.getConvertView().findViewById(R.id.tv_num4);
        tv5 = (TextView) holder.getConvertView().findViewById(R.id.tv_num5);
        tv6 = (TextView) holder.getConvertView().findViewById(R.id.tv_num6);
        tv7 = (TextView) holder.getConvertView().findViewById(R.id.tv_num7);
        tv8 = (TextView) holder.getConvertView().findViewById(R.id.tv_num8);
        tv9 = (TextView) holder.getConvertView().findViewById(R.id.tv_num9);
        tvPoint = (TextView) holder.getConvertView().findViewById(R.id.tv_point);
        tvClear = (TextView) holder.getConvertView().findViewById(R.id.tv_clear);
        tvCommit = (TextView) holder.getConvertView().findViewById(R.id.tv_commit);
        tvNum = (TextView) holder.getConvertView().findViewById(R.id.tv_num);
        RelativeLayout rlBackspce = (RelativeLayout) holder.getConvertView().findViewById(R.id.rl_backspace);
        tv0.setOnClickListener(onClickListener);
        tv1.setOnClickListener(onClickListener);
        tv2.setOnClickListener(onClickListener);
        tv3.setOnClickListener(onClickListener);
        tv4.setOnClickListener(onClickListener);
        tv5.setOnClickListener(onClickListener);
        tv6.setOnClickListener(onClickListener);
        tv7.setOnClickListener(onClickListener);
        tv8.setOnClickListener(onClickListener);
        tv9.setOnClickListener(onClickListener);
        tvPoint.setOnClickListener(onClickListener);
        tvClear.setOnClickListener(onClickListener);
        rlBackspce.setOnClickListener(onClickListener);
        tvCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDialogClickListener.onItemClick(view,dialog);
            }
        });


    }

    View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int i = view.getId();
            if(i == R.id.tv_num0){
                if(checkNum())
                tvNum.append("0");
            }else if(i == R.id.tv_num1){
                if(checkNum())
                tvNum.append("1");
            }else if(i == R.id.tv_num2){
                if(checkNum())
                tvNum.append("2");
            }else if(i == R.id.tv_num3){
                if(checkNum())
                tvNum.append("3");
            }else if(i == R.id.tv_num4){
                if(checkNum())
                tvNum.append("4");
            }else if(i == R.id.tv_num5){
                if(checkNum())
                tvNum.append("5");
            }else if(i == R.id.tv_num6){
                if(checkNum())
                tvNum.append("6");
            }else if(i == R.id.tv_num7){
                if(checkNum())
                tvNum.append("7");
            }else if(i == R.id.tv_num8){
                if(checkNum())
                tvNum.append("8");
            }else if(i == R.id.tv_num9){
                if(checkNum())
                tvNum.append("9");
            }else if(i == R.id.tv_point){
                if(tvNum.getText().toString().length() == 1)
                    tvNum.append(".");
            }else if(i == R.id.rl_backspace){
                String text = tvNum.getText().toString();
                if(text.length()>0)
                tvNum.setText(text.substring(0,text.length()-1));
            }else if(i == R.id.tv_clear){
                tvNum.setText("");
            }

        }
    };

    private boolean checkNum() {
        String numStr = tvNum.getText().toString();
        if(numStr.length() ==1)
            return false;
        else if(numStr.length()  == 3)
            return false;
        return true;
    }

    public double getNum() {
        double num = Double.parseDouble( tvNum.getText().toString() );
        return num;
    }
}
