package com.tchcn.cashier.order.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.model.TypeActModel;

import java.util.List;

/**
 * 最右菜单adapter
 * Created by sad on 2018/5/3.
 */

public class RightMenuAdapter extends BaseQuickAdapter<TypeActModel.TypeModel,BaseViewHolder> {
    private int select = 0;

    public RightMenuAdapter(@Nullable List<TypeActModel.TypeModel> data) {
        super(R.layout.item_right_menu,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TypeActModel.TypeModel item) {
        if(select == helper.getAdapterPosition()){
            helper.itemView.setSelected(true);
        }else {
            helper.itemView.setSelected(false);
        }
        helper.setText(R.id.tv_name,item.getDeal_name());
        if(item.getNum() > 0)
            helper.setVisible(R.id.tv_num,true)
                    .setText(R.id.tv_num,item.getNum()+"");
    }

    public void setSelect(int select) {
        this.select = select;
        notifyDataSetChanged();
    }
}
