package com.tchcn.cashier.order.dialog;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewConvertListener;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.utils.SDToast;

/**
 * Created by sad on 2018/5/5.
 */

public class ChooseNumDialog extends BaseNiceDialog{

    TextView tvNum;

    public interface OnDialogClickListener {
        void onItemClick(View view,BaseNiceDialog dialog);
    }
    private  int num =0;
    OnDialogClickListener onDialogClickListener = null;

    public static ChooseNumDialog newInstance() {
        ChooseNumDialog dialog = new ChooseNumDialog();
        return dialog;
    }

    public ChooseNumDialog setConvertListener(OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_choose_num;
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        holder.setOnClickListener(R.id.iv_close, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        TextView tv0 = (TextView) holder.getConvertView().findViewById(R.id.tv_num0);
        TextView tv1 = (TextView) holder.getConvertView().findViewById(R.id.tv_num1);
        TextView tv2 = (TextView) holder.getConvertView().findViewById(R.id.tv_num2);
        TextView tv3 = (TextView) holder.getConvertView().findViewById(R.id.tv_num3);
        TextView tv4 = (TextView) holder.getConvertView().findViewById(R.id.tv_num4);
        TextView tv5 = (TextView) holder.getConvertView().findViewById(R.id.tv_num5);
        TextView tv6 = (TextView) holder.getConvertView().findViewById(R.id.tv_num6);
        TextView tv7 = (TextView) holder.getConvertView().findViewById(R.id.tv_num7);
        TextView tv8 = (TextView) holder.getConvertView().findViewById(R.id.tv_num8);
        TextView tv9 = (TextView) holder.getConvertView().findViewById(R.id.tv_num9);
        TextView tvClear = (TextView) holder.getConvertView().findViewById(R.id.tv_clear);
        TextView tvCommit = (TextView) holder.getConvertView().findViewById(R.id.tv_commit);
        tvNum= (TextView) holder.getConvertView().findViewById(R.id.tv_num);
        RelativeLayout rlBackspce = (RelativeLayout) holder.getConvertView().findViewById(R.id.rl_backspace);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = view.getId();
                if(i == R.id.tv_num0){
                    tvNum.append("0");
                }else if(i == R.id.tv_num1){
                    tvNum.append("1");
                }else if(i == R.id.tv_num2){
                    tvNum.append("2");
                }else if(i == R.id.tv_num3){
                    tvNum.append("3");
                }else if(i == R.id.tv_num4){
                    tvNum.append("4");
                }else if(i == R.id.tv_num5){
                    tvNum.append("5");
                }else if(i == R.id.tv_num6){
                    tvNum.append("6");
                }else if(i == R.id.tv_num7){
                    tvNum.append("7");
                }else if(i == R.id.tv_num8){
                    tvNum.append("8");
                }else if(i == R.id.tv_num9){
                    tvNum.append("9");
                }else if(i == R.id.rl_backspace){
                    String text = tvNum.getText().toString();
                    if(text.length()>0)
                    tvNum.setText(text.substring(0,text.length()-1));
                }else if(i == R.id.tv_clear){
                    tvNum.setText("");
                }

            }
        };
        tv0.setOnClickListener(onClickListener);
        tv1.setOnClickListener(onClickListener);
        tv2.setOnClickListener(onClickListener);
        tv3.setOnClickListener(onClickListener);
        tv4.setOnClickListener(onClickListener);
        tv5.setOnClickListener(onClickListener);
        tv6.setOnClickListener(onClickListener);
        tv7.setOnClickListener(onClickListener);
        tv8.setOnClickListener(onClickListener);
        tv9.setOnClickListener(onClickListener);
        tvClear.setOnClickListener(onClickListener);
        rlBackspce.setOnClickListener(onClickListener);
        tvCommit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDialogClickListener.onItemClick(view,dialog);
            }
        });


    }

    public int getNum() {
        if(tvNum.getText().toString().equals("")){
            SDToast.showToast("请输入正确");
            return 0;
        }
        return Integer.parseInt(tvNum.getText().toString());
    }
}
