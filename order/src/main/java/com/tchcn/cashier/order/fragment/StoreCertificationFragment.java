package com.tchcn.cashier.order.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.tchcn.cashier.order.R;
import com.tchcn.library.fragment.SDBaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 门店认证
 */
public class StoreCertificationFragment extends SDBaseFragment {


    TextView tvGo;
    LinearLayout llDefault;
    LinearLayout llForm;

    public StoreCertificationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(View v) {
        tvGo = (TextView) v.findViewById(R.id.tv_go);
        llDefault = (LinearLayout) v.findViewById(R.id.ll_default);
        llForm = (LinearLayout) v.findViewById(R.id.ll_form);

    }

    @Override
    protected int onCreateContentView() {
        return R.layout.frag_store_certification;
    }

}
