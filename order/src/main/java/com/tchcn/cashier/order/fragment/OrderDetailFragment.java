package com.tchcn.cashier.order.fragment;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.tchcn.cashier.order.R;
import com.tchcn.library.fragment.SDBaseFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * 订单详情
 * Created by sad on 2018/5/31.
 */
public class OrderDetailFragment extends SDBaseFragment {

    TextView tvAmountCollect;//应收
    LinearLayout llAmount;//金额Layout
    TextView tvOrderCode;//订单编号
    TextView tvType;//订单类型
    TextView tvStatus;//状态
    TextView tvOperateName;//操作人
    TextView tvPlaceOrderTime;//预定时间
    TextView tvSettleAccount;//结账时间
    LinearLayout llBasicInfo;//基本信息
    ImageView ivAmountArrow;//金额箭头
    TextView tvAmountReceivable;//应收金额
    RelativeLayout rlAmountReceivable;//应收金额layout
    TextView tvAmount;//总金额
    TextView tvAmountDiscount;//优惠金额
    TextView tvBusSmallFree;//商家抹零
    TextView tvCash;//现金支付
    TextView tvGiveChange;//找零
    TextView tvAmountCollected;//实收金额
    TextView tvSettleName;//结账人姓名
    RecyclerView rvGood;//菜品列表
    TextView tvRemark;//备注
    LinearLayout llGoodInfo;//菜品layout
    LinearLayout llSettlementInfo;//结账信息layout
    TextView tvRefund;//退款
    Button btnPrint;//打印小票
    LinearLayout llOrderCheckOutBottom;//已结账按钮布局
    TextView btnPrintRefundPart;//打印部分退款
    Button btnPrintPart;//打印部分收款
    LinearLayout llOrderRefundBottom;//部分退款按钮布局

    public OrderDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(View v) {
       tvAmountCollect = (TextView)v.findViewById(R.id.tv_amount_collect);
       tvOrderCode = (TextView)v.findViewById(R.id.tv_order_code);
       tvType = (TextView)v.findViewById(R.id.tv_type);
       tvStatus = (TextView)v.findViewById(R.id.tv_status);
       tvOperateName = (TextView)v.findViewById(R.id.tv_operate_name);
       tvPlaceOrderTime = (TextView)v.findViewById(R.id.tv_place_order_time);
       tvSettleAccount = (TextView)v.findViewById(R.id.tv_settle_account);
       tvAmountReceivable = (TextView)v.findViewById(R.id.tv_amount_receivable);
       tvAmount = (TextView)v.findViewById(R.id.tv_amount);
       tvAmountDiscount = (TextView)v.findViewById(R.id.tv_amount_discount);
       tvBusSmallFree = (TextView)v.findViewById(R.id.tv_bus_small_free);
       tvCash = (TextView)v.findViewById(R.id.tv_cash);
       tvGiveChange = (TextView)v.findViewById(R.id.tv_give_change);
       tvAmountCollected = (TextView)v.findViewById(R.id.tv_amount_collected);
       tvSettleName = (TextView)v.findViewById(R.id.tv_amount_collect);
       tvRemark = (TextView)v.findViewById(R.id.tv_amount_collect);
       btnPrintRefundPart = (TextView)v.findViewById(R.id.tv_amount_collect);
       tvRefund = (TextView)v.findViewById(R.id.tv_amount_collect);
        rvGood = (RecyclerView)v.findViewById(R.id.rv_good);
        ivAmountArrow = (ImageView) v.findViewById(R.id.iv_amount_arrow);
        btnPrint = (Button) v.findViewById(R.id.btn_print);//打印小票
        btnPrintPart = (Button) v.findViewById(R.id.btn_print_part);//打印部分收款
        llOrderRefundBottom = (LinearLayout) v.findViewById(R.id.ll_order_refund_bottom);//部分退款按钮布局
        llOrderCheckOutBottom = (LinearLayout) v.findViewById(R.id.ll_order_check_out_bottom);//已结账按钮布局
        llGoodInfo = (LinearLayout) v.findViewById(R.id.ll_good_info);//菜品layout
        llSettlementInfo = (LinearLayout) v.findViewById(R.id.ll_settlement_info);//结账信息layout
        llBasicInfo = (LinearLayout) v.findViewById(R.id.ll_basic_info);//基本信息
        llAmount = (LinearLayout) v.findViewById(R.id.ll_amount);//金额Layout
        rlAmountReceivable = (RelativeLayout) v.findViewById(R.id.rl_amount_receivable);//应收金额layout
    }

    @Override
    protected int onCreateContentView() {
        return R.layout.frag_order_detail;
    }

}
