package com.tchcn.cashier.order.adapter;

import android.support.annotation.Nullable;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.model.TableModel;

import java.util.List;

/**
 * 餐桌adapter
 * Created by sad on 2018/5/3.
 */

public class ChooseTableAdapter extends BaseQuickAdapter<TableModel,BaseViewHolder> {
    public ChooseTableAdapter(@Nullable List<TableModel> data) {
        super(R.layout.item_table,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, TableModel item) {
        helper.setText(R.id.tv_table_name,item.getTableid());
        if(item.getStatus() == 1){   //空闲
            helper.getView(R.id.tv_table_name).setBackgroundColor(mContext.getResources().getColor(R.color.table_free));
            helper.getView(R.id.tv_state).setVisibility(View.INVISIBLE);
            helper.getView(R.id.ll_time).setVisibility(View.INVISIBLE);
        }else if(item.getStatus() == 2){   //停用
            helper.getView(R.id.tv_table_name).setBackgroundColor(mContext.getResources().getColor(R.color.color_hint));
            helper.getView(R.id.tv_state).setVisibility(View.INVISIBLE);
            helper.getView(R.id.ll_time).setVisibility(View.INVISIBLE);
        }else if(item.getStatus() == 3) {  //开台
            helper .setText(R.id.tv_date,item.getDay())
                    .setText(R.id.tv_time,item.getTime())
                    .getView(R.id.tv_table_name).setBackgroundColor(mContext.getResources().getColor(R.color.table_open));
            TextView tvState = helper.getView(R.id.tv_state);
            tvState.setText("待结账");
            tvState.setTextColor(mContext.getResources().getColor(R.color.menu_text));
            tvState.setBackgroundColor(mContext.getResources().getColor(R.color.table_open_bg));

            TextView tvTime = helper.getView(R.id.tv_time);
            tvTime.setTextColor(mContext.getResources().getColor(R.color.color_hint));
            helper.getView(R.id.tv_state).setVisibility(View.VISIBLE);
            helper.getView(R.id.ll_time).setVisibility(View.VISIBLE);
        }else if(item.getStatus() == 4){  //预定
            helper .setText(R.id.tv_date,item.getDay())
                    .setText(R.id.tv_time,"预定"+item.getTime())
                    .getView(R.id.tv_table_name).setBackgroundColor(mContext.getResources().getColor(R.color.table_reserve));
            TextView tvTime = helper.getView(R.id.tv_time);
            tvTime.setTextColor(mContext.getResources().getColor(R.color.table_reserve));
            helper.getView(R.id.tv_state).setVisibility(View.INVISIBLE);
            helper.getView(R.id.ll_time).setVisibility(View.VISIBLE);
        }else if(item.getStatus() == 5){  //挂单
            helper .setText(R.id.tv_date,item.getDay())
                    .setText(R.id.tv_time,item.getTime())
                    .getView(R.id.tv_table_name).setBackgroundColor(mContext.getResources().getColor(R.color.table_open));
            TextView tvState = helper.getView(R.id.tv_state);
            tvState.setText("待下单");
            tvState.setTextColor(mContext.getResources().getColor(R.color.table_free));
            tvState.setBackgroundColor(mContext.getResources().getColor(R.color.table_free_bg));
            TextView tvTime = helper.getView(R.id.tv_time);
            tvTime.setTextColor(mContext.getResources().getColor(R.color.color_hint));
            helper.getView(R.id.tv_state).setVisibility(View.VISIBLE);
            helper.getView(R.id.ll_time).setVisibility(View.VISIBLE);
        }
    }

}
