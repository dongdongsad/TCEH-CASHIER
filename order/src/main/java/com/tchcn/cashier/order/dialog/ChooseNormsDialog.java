package com.tchcn.cashier.order.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.othershe.nicedialog.BaseNiceDialog;
import com.othershe.nicedialog.ViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.cashier.order.adapter.OrderNormsAdapter;
import com.tchcn.library.model.GoodActModel;
import com.tchcn.library.model.NormsModel;

import java.util.List;

/**
 * 选择规格
 * Created by sad on 2018/5/15.
 */

public class ChooseNormsDialog extends BaseNiceDialog{

    List<NormsModel> models;
    OrderNormsAdapter orderNormsAdapter;
    GoodActModel goodActModel;

    ChooseNumDialog.OnDialogClickListener onDialogClickListener = null;
    public static ChooseNormsDialog newInstance(GoodActModel goodActModel) {
        ChooseNormsDialog dialog = new ChooseNormsDialog();
        Bundle bundle = new Bundle();
        bundle.putSerializable("actModel",  goodActModel);
        dialog.setArguments(bundle);
        return dialog;
    }

    public ChooseNormsDialog setConvertListener(ChooseNumDialog.OnDialogClickListener convertListener) {
        this.onDialogClickListener = convertListener;
        return this;
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle == null) {
            return;
        }
        goodActModel = (GoodActModel)bundle.getSerializable("actModel");
        models = goodActModel.getNormsModel();
    }

    @Override
    public int intLayoutId() {
        return R.layout.dialog_choose_norms;
    }

    @Override
    public void convertView(final ViewHolder holder, final BaseNiceDialog dialog) {
        holder.setOnClickListener(R.id.iv_close, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        holder.setText(R.id.tv_price,"￥" + goodActModel.getPrice());
        RecyclerView recyclerView = holder.getView(R.id.rv_norms);
        orderNormsAdapter = new OrderNormsAdapter(models);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(orderNormsAdapter);
        holder.setOnClickListener(R.id.tv_commit, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDialogClickListener.onItemClick(view,dialog);
            }
        });
    }

    public List<NormsModel> getSelectNorms(){
        return orderNormsAdapter.getSelectNorms();
    }

}
