package com.tchcn.cashier.order.adapter;

import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.support.annotation.Nullable;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.model.NormsModel;
import com.tchcn.cashier.order.view.FlowTagLayout;
import com.tchcn.library.utils.SDViewUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 规格adapter
 * Created by sad on 2018/5/10.
 */

public class OrderNormsAdapter extends BaseQuickAdapter<NormsModel,BaseViewHolder> {
    List<NormsModel> selectNorms = new ArrayList<>();


    public OrderNormsAdapter(@Nullable List<NormsModel> data) {
        super(R.layout.item_norms,data);
        for (int i = 0; i < data.size(); i++) {
            selectNorms.add(data.get(i).getNorms().get(0));
        }
    }

    @Override
    protected void convert(final BaseViewHolder helper, final NormsModel item) {
        helper.setText(R.id.tv_name,item.getName());
        final FlowTagLayout flowTagLayout = helper.getView(R.id.flowTagLayout);
        flowTagLayout.removeAllViews();
        for (int i = 0; i < item.getNorms().size(); i++) {
            final TextView textView = makeTextView(item.getNorms().get(i).getName());
            if(i == 0)
                textView.setSelected(true);
            textView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for (int j = 0; j < flowTagLayout.getChildCount(); j++) {
                        if(flowTagLayout.getChildAt(j) == textView) {
                            textView.setSelected(true);
                            selectNorms.set(helper.getAdapterPosition(),item.getNorms().get(j));
                        }else
                            flowTagLayout.getChildAt(j).setSelected(false);
                    }
                }
            });
            flowTagLayout.addView(textView);
        }


    }

    private TextView makeTextView(String name) {
        final TextView textView = new TextView(mContext);
        textView.setGravity(Gravity.CENTER);
        textView.setTextSize(SDViewUtil.px2sp(24));
        int padding = 10;
        textView.setPadding(padding, padding / 2, padding, padding / 2);
        textView.setBackgroundResource(R.drawable.selector_norms_bg);
        Resources resource = mContext.getResources();
        ColorStateList csl = resource.getColorStateList(R.color.norm_text_color);
        if (csl != null) {
            textView.setTextColor(csl);
        }
        textView.setText(name);
        return textView;
    }

    public List<NormsModel> getSelectNorms() {
        return selectNorms;
    }
}
