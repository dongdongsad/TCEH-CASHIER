package com.tchcn.cashier.order.adapter;

import android.support.annotation.Nullable;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tchcn.cashier.order.R;
import com.tchcn.library.model.OrderModel;

import java.util.List;

/**
 * 取单adapter
 * Created by sad on 2018/5/16.
 */

public class GetOrderAdapter extends BaseQuickAdapter<OrderModel,BaseViewHolder> {
    private int selectNum = -1;


    public GetOrderAdapter(@Nullable List<OrderModel> data) {
        super(R.layout.item_get_order,data);
    }

    @Override
    protected void convert(BaseViewHolder helper, OrderModel item) {
        double totalPrice = 0;
        String goodName = "";
        for (int i = 0; i < item.getGoodList().size(); i++) {
            totalPrice += item.getGoodList().get(i).getPrice();
            goodName += item.getGoodList().get(i).getName()+",";
        }
        helper.setText(R.id.tv_price,"￥"+ totalPrice)
                .setText(R.id.tv_goods,goodName.substring(0,goodName.length()-1))
                .setText(R.id.tv_time,"挂单时间："+item.getPlaceOrderTime())
                .setText(R.id.tv_operate,"挂单人："+"sds");

        if(selectNum == helper.getAdapterPosition()){
            helper.itemView.setSelected(true);
        }else
            helper.itemView.setSelected(false);

    }

    public void setSelectNum(int selectNum) {
        this.selectNum = selectNum;
        this.notifyDataSetChanged();
    }
}
