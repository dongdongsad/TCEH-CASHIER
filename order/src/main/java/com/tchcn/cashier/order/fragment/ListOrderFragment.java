package com.tchcn.cashier.order.fragment;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.tchcn.cashier.order.R;
import com.tchcn.library.fragment.SDBaseFragment;

/**
 * 订单页
 * Created by sad on 2018/5/31.
 */

public class ListOrderFragment extends SDBaseFragment {


    public ListOrderFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        initView(rootView);
        return rootView;
    }

    private void initView(View rootView) {

    }

    @Override
    protected int onCreateContentView() {
        return R.layout.frag_list_order;
    }

}
