package com.tchcn.cashier.order.fragment;

import com.tchcn.cashier.order.R;
import com.tchcn.library.fragment.SDBaseFragment;

/**
 * Created by sad on 2018/11/3.
 */

public class MemberFragment extends SDBaseFragment {
    @Override
    protected int onCreateContentView() {
        return R.layout.frag_member;
    }
}
