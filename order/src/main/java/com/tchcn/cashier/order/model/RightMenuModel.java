package com.tchcn.cashier.order.model;

/**
 * Created by sad on 2018/5/3.
 */

public class RightMenuModel {
    private int id;
    private String name;
    private int num = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int day) {
        this.num = num;
    }

}
